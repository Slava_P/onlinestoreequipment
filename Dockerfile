#образ взятый за основу
FROM openjdk:17
#Записываем в переменную путь до WAR файла
ARG jarFile=target/OnlineStoreEquipment-1.war
#Куда мы перемещаем варник внутри контейнера
WORKDIR /opt/app
#копируем jar внутрь контейнера
COPY ${jarFile} onlineStoreEquipment.war
#открываем порт
EXPOSE 8090
#команда для запуска
ENTRYPOINT ["java", "-jar", "onlineStoreEquipment.war"]