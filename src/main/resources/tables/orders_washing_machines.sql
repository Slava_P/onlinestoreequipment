CREATE TABLE store_equipment.washing_machines (
    order_id INTEGER REFERENCES store_equipment.oreders(id) ON DELETE CASCADE,
    washing_machine_id INTEGER REFERENCES store_equipment.washing_machines(id) ON DELETE CASCADE,
    amount Integer
);