CREATE TABLE store_equipment.accounts(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    middle_name VARCHAR(50),
    birth_date DATE NOT NULL,
    telephone VARCHAR(50),
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    login VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    role varchar(50) default 'USER'
);
