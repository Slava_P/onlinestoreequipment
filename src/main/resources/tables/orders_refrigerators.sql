CREATE TABLE store_equipment.orders_refrigerators (
  order_id INTEGER REFERENCES store_equipment.oreders(id) ON DELETE CASCADE,
  refrigerator_id INTEGER REFERENCES store_equipment.refrigerators(id) ON DELETE CASCADE,
  amount Integer
);