CREATE TABLE store_equipment.orders_irons (
    order_id INTEGER REFERENCES store_equipment.oreders(id) ON DELETE CASCADE,
    iron_id INTEGER REFERENCES store_equipment.irons(id) ON DELETE CASCADE,
    amount Integer
);