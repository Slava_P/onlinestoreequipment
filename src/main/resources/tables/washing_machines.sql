CREATE TYPE store_equipment.motor_type AS ENUM ('INVERTER', 'STANDARD');

CREATE TABLE store_equipment.washing_machines(
    id SERIAL PRIMARY KEY,
    imgPath varchar(250),
    manufacturer_id INTEGER REFERENCES store_equipment.manufacturers(id) ON DELETE CASCADE,
    model VARCHAR(50) NOT NULL,
    power_consumption INTEGER NOT NULL ,
    motor_type store_equipment.motor_type,
    max_spin_speed INTEGER NOT NULL,
    water_consumption REAL,
    max_load INTEGER NOT NULL ,
    height REAL,
    width REAL,
    depth REAL,
    color VARCHAR(50) NOT NULL,
    weight REAL NOT NULL ,
    amount INTEGER NOT NULL,
    cost INTEGER NOT NULL,
    description TEXT,
    product_type store_equipment.product_type,
    product_subtype store_equipment.product_subtype
);

