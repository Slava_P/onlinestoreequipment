CREATE TYPE store_equipment.product_type AS ENUM ('HOME_APPLIANCES', 'KITCHEN_APPLIANCES');
CREATE TYPE store_equipment.product_subtype AS ENUM ('MICROWAVE', 'REFRIGERATOR', 'WASHING_MACHINE', 'VACUUM_CLEANER', 'IRON');

CREATE TABLE store_equipment.irons(
    id SERIAL PRIMARY KEY,
    imgPath varchar(250),
    manufacturer_id INTEGER REFERENCES store_equipment.manufacturers(id) ON DELETE CASCADE,
    model VARCHAR(50) NOT NULL,
    power_consumption INTEGER NOT NULL,
    water_tank_volume INTEGER NOT NULL,
    water_sprayer BOOLEAN,
    power_cord_length REAL,
    turbo_steam_supply BOOLEAN,
    color VARCHAR(50) NOT NULL,
    height REAL,
    width REAL,
    depth REAL,
    weight REAL NOT NULL ,
    amount INTEGER NOT NULL,
    cost INTEGER NOT NULL,
    description TEXT,
    product_type store_equipment.product_type,
    product_subtype store_equipment.product_subtype
);
