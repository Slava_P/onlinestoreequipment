CREATE TABLE store_equipment.microwaves(
    id SERIAL PRIMARY KEY,
    imgPath varchar(250),
    manufacturer_id INTEGER REFERENCES store_equipment.manufacturers(id) ON DELETE CASCADE,
    model VARCHAR(50) NOT NULL,
    power_consumption INTEGER NOT NULL,
    microwave_power INTEGER NOT NULL,
    volume Integer,
    height REAL,
    width REAL,
    depth REAL,
    color VARCHAR(50) NOT NULL,
    weight REAL NOT NULL ,
    amount INTEGER NOT NULL,
    cost INTEGER NOT NULL,
    description TEXT,
    product_type store_equipment.product_type,
    product_subtype store_equipment.product_subtype
);