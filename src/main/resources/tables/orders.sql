CREATE TABLE store_equipment.orders (
     id SERIAL PRIMARY KEY,
     account_id INTEGER REFERENCES store_equipment.accounts(id) ON DELETE CASCADE,
     is_paid BOOLEAN NOT NULL ,
     cost_order INTEGER NOT NULL,
     date_create DATE,
     date_paid DATE,
     address VARCHAR(150) NOT NULL
);