CREATE TABLE store_equipment.manufacturers(
  id SERIAL PRIMARY KEY,
  firm_name VARCHAR(50) NOT NULL ,
  country VARCHAR(50) NOT NULL,
  description TEXT
);
