CREATE TABLE store_equipment.orders_microwaves (
  order_id INTEGER REFERENCES store_equipment.oreders(id) ON DELETE CASCADE,
  microwave_id INTEGER REFERENCES store_equipment.microwaves(id) ON DELETE CASCADE,
  amount Integer
);