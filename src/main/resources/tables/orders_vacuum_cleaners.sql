CREATE TABLE store_equipment.orders_vacuum_cleaners (
    order_id INTEGER REFERENCES store_equipment.oreders(id) ON DELETE CASCADE,
    vacuum_cleaners_id INTEGER REFERENCES store_equipment.vacuum_cleaners(id) ON DELETE CASCADE,
    amount Integer
);