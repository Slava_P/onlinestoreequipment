package com.onlinestoreequipment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineStoreEquipmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineStoreEquipmentApplication.class, args);
    }

}
