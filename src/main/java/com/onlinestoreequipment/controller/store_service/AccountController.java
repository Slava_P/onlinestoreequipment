package com.onlinestoreequipment.controller.store_service;

import com.onlinestoreequipment.dto.store_service.AccountDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.service.store_service.AccountService;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService){
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model){
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.getAccountByLogin(login);
        model.addAttribute("account", new AccountDTO(account));
        return "store_service/account/profile";
    }

    @GetMapping("/signIn")
    public String index(HttpServletRequest httpServletRequest, Model model){
        //        Проверка данных авторизации
        if (SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AbstractAuthenticationToken)){
            return "index";
        }
        return "store_service/account/signInAccount";
    }

    @GetMapping("/update")
    public String update(Model model){
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        Account account = accountService.getAccountByLogin(login);
        model.addAttribute("profile", new AccountDTO(account));
        return "store_service/account/updateAccount";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid AccountDTO accountDTO){
        accountService.updateFromDTO(accountDTO.getId(), accountDTO);
        return "redirect:/account";
    }

    @GetMapping("/registration")
    public String registrationAccount(){
        return "store_service/account/registration";
    }

    @PostMapping("/registration")
    public String createAccount(@ModelAttribute("accountForm") @Valid AccountDTO accountDTO){
        accountService.createFromDTO(accountDTO);
        return "redirect:/account/signIn";
    }

    @GetMapping("/remember-password")
    public String changePassword(){
        return "store_service/account/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String changePassword(@ModelAttribute("changePasswordForm") AccountDTO accountDTO){
        accountDTO = accountService.getAccountByEmail(accountDTO.getEmail());
        accountService.sendChangePasswordEmail(accountDTO.getEmail(), accountDTO.getId());
        return "redirect:/account/signIn";
    }

    @GetMapping("/change-password/{userId}")
    public String changePasswordAfterEmailSent(@PathVariable("userId") Long id, Model model){
        model.addAttribute("userId", id);
        return "store_service/account/changePassword";
    }

    @PostMapping("/change-password/{userId}")
    public String changePasswordAfterEmailSent(@PathVariable("userId") Long id, @ModelAttribute("changePasswordForm") AccountDTO accountDTO){
        accountService.changePassword(id, accountDTO.getPassword());
        return "redirect:/account/signIn";
    }





}
