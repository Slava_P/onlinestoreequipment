package com.onlinestoreequipment.controller.store_service;

import com.onlinestoreequipment.dto.store_service.AccountDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderShowDTO;
import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

@Controller
@RequestMapping("/orders")
public class OrderController {

    private OrderService orderService;
    private AccountService accountService;

    public OrderController(OrderService orderService, AccountService accountService){
        this.orderService = orderService;
        this.accountService = accountService;
    }

    @GetMapping("/account")
    public String index(Model model){
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        model.addAttribute("allOrders", orderService.getAllToShowDTOInAccount(login));
        return "store_service/order/viewAllOrders";
    }

    @GetMapping("/account/basket")
    public String showBasket(Model model){
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            OrderDTO orderDTO = orderService.getLastOrderAccountToShow(login);
            if (!orderDTO.getIsPaid()) {
                model.addAttribute("order", orderDTO);
                model.addAttribute("amount", orderDTO.getProducts().size());
                model.addAttribute("products", orderDTO.getProducts());
                return "store_service/order/basket";
            }
        }
        return "store_service/order/basket_empty";
    }

    @PostMapping("/account/basket/increase-amount")
    public String increaseAmountProduct(@ModelAttribute("productDTO") ProductOrderDTO product, @ModelAttribute("orderId") Long orderId){
        orderService.increaseAmountProduct(product, orderId);
        return "redirect:/orders/account/basket";
    }

    @PostMapping("/account/basket/reduce-amount")
    public String reduceAmountProduct(@ModelAttribute("productDTO") ProductOrderDTO product, @ModelAttribute("orderId") Long orderId){
        if(product.getAmountInOrder() > 1){
            orderService.reduceAmountProduct(product, orderId);
        }
        return "redirect:/orders/account/basket";
    }

    @GetMapping("/account/{orderId}/form")
    public String formOrder(@PathVariable Long orderId, Model model){
        Order order = orderService.getOne(orderId);
        order.setDateCreate(LocalDate.now());
        orderService.update(order);
        Account account = order.getAccount();
        model.addAttribute("account", account);
        model.addAttribute("order", order);
        return "store_service/order/formOrder";
    }

    @PostMapping("/account/form")
    public String formOrder(@ModelAttribute("address") String address, HttpServletRequest request){
        Order order = orderService.getOne(Long.valueOf(request.getParameter("orderId")));
        order.setAddress(address);
        order.setDatePaid(LocalDate.now());
        order.setIsPaid(true);
        orderService.update(order);
        orderService.order_notification(order.getId());

        return "redirect:/orders/account";
    }

    @PostMapping("/account/basket/delete")
    public String deleteProductFromBasket(@ModelAttribute("productDTO") ProductOrderDTO product, @ModelAttribute("orderId") Long orderId){
        orderService.deleteProductFromOrder(product, orderId);

        return "redirect:/orders/account/basket";
    }

    @GetMapping("/allOrders")
    public String getAllOrders(Model model){
        model.addAttribute("allOrders", orderService.getAllToShowDTO());
        return "store_service/order/viewAllOrders";
    }

    @PostMapping("/find-orders")
    public String findOrders(@ModelAttribute("orderId") Long orderId, Model model){
        model.addAttribute("allOrders", new OrderShowDTO(orderService.getOne(orderId)));
        return "store_service/order/viewAllOrders";
    }

    @PostMapping("/get")
    public String getOrder(@ModelAttribute("orderId") Long orderId, Model model){
        Order order = orderService.getOne(orderId);
        model.addAttribute("order", new OrderShowDTO(order));
        model.addAttribute("products", new OrderDTO(order).getProducts());
        model.addAttribute("account", new AccountDTO(order.getAccount()));
        return "store_service/order/viewOrder";
    }
}
