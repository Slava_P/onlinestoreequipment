package com.onlinestoreequipment.controller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.IronDTO;
import com.onlinestoreequipment.dto.product.home_appliances.IronFilterDTO;
import com.onlinestoreequipment.dto.product.home_appliances.IronPageDTO;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderIron;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.home_appliances.IronService;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderIronService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.yaml.snakeyaml.events.Event;

import javax.validation.Valid;
import java.sql.SQLException;

@Controller
@RequestMapping("/irons")
public class IronController {

    private IronService ironService;
    private ManufacturerService manufacturerService;
    private OrderService orderService;
    private OrderIronService orderIronService;
    private AccountService accountService;

    public IronController(IronService ironService, ManufacturerService manufacturerService, OrderService orderService, OrderIronService orderIronService, AccountService accountService){
        this.ironService = ironService;
        this.manufacturerService = manufacturerService;
        this.orderService = orderService;
        this.orderIronService = orderIronService;
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("irons", ironService.getAllToShow());
        model.addAttribute("manufacturers", manufacturerService.getAll());
        model.addAttribute("maxCost", ironService.getMaxCost());
        model.addAttribute("minCost", ironService.getMinCost());
        return "product/home_appliances/iron/viewAllIrons";
    }

    @GetMapping("/get/{id}")
    public String getOne(@PathVariable Long id, Model model){
        IronPageDTO ironPageDTO = new IronPageDTO(ironService.getOne(id));
        model.addAttribute("product", ironPageDTO);
        return "product/home_appliances/iron/viewIron";
    }

    @GetMapping("/add")
    public String create(){
        return "product/home_appliances/iron/addIron";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("ironDTO") @Valid IronDTO ironDTO, @RequestParam MultipartFile file){
        if (file != null && file.getSize() > 0){
            ironService.createFromDTO(ironDTO, file);
        } else {
            ironService.createFromDTO(ironDTO);
        }
        return "redirect:/irons";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("iron", new IronDTO(ironService.getOne(id)));
        return "product/home_appliances/iron/updateIron";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid IronDTO ironDTO,
                         @RequestParam MultipartFile file){
        if(file != null && file.getSize() > 0){
            ironService.updateFromDTO(ironDTO.getId(), ironDTO, file);
        } else {
            ironService.updateFromDTO(ironDTO.getId(), ironDTO);
        }
        return "redirect:/irons";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        ironService.delete(id);
        return "redirect:/irons";
    }

    @PostMapping("/filter")
    public String getAllWithFilter(@ModelAttribute("ironsFilterDTO")@Valid IronFilterDTO ironFilterDTO, Model model) throws SQLException {
        model.addAttribute("ironsWithFilter", ironService.getAllWithFilter(ironFilterDTO));
        return "product/home_appliances/iron/ironsWithFilter";
    }

    @PostMapping("/filter-by-model")
    public String getAllByModel(@ModelAttribute("ironByModel")@Valid IronFilterDTO ironFilterDTO, Model model){
        model.addAttribute("ironsWithFilter", ironService.getAllByModel(ironFilterDTO));
        return "product/home_appliances/iron/ironsWithFilter";
    }

    @PostMapping("/putInBasket")
    public String putInBasket(@ModelAttribute("idProduct") Long id){
        Iron iron = ironService.getOne(id);
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            Order order = orderService.getLastOrderAccount(login);
            if (!order.getIsPaid()) {
                OrderIron orderIron = orderIronService.create(order, iron);
                order.getOrderIrons().add(orderIron);
                orderService.updateFromDTO(order.getId(), new OrderDTO(order));
                return "redirect:/irons";
            }
        }
        Order newOrder = new Order();
        newOrder.setAccount(accountService.getAccountByLogin(login));
        OrderIron orderIron = orderIronService.create(newOrder, iron);
        newOrder.getOrderIrons().add(orderIron);
        orderService.updateFromDTO(newOrder.getId(), new OrderDTO(newOrder));
        return "redirect:irons";
    }
}
