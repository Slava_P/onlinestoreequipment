package com.onlinestoreequipment.controller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerDTO;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerFilterDTO;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerPageDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderVacuumCleaner;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.home_appliances.VacuumCleanerService;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import com.onlinestoreequipment.service.store_service.orders.OrderVacuumCleanerService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.sql.SQLException;

@Controller
@RequestMapping("/vacuum-cleaners")
public class VacuumCleanerController {

    private VacuumCleanerService vacuumCleanerService;
    private ManufacturerService manufacturerService;
    private OrderService orderService;
    private OrderVacuumCleanerService orderVacuumCleanerService;
    private AccountService accountService;

    public VacuumCleanerController(VacuumCleanerService vacuumCleanerService, ManufacturerService manufacturerService, OrderService orderService, OrderVacuumCleanerService orderVacuumCleanerService, AccountService accountService){
        this.vacuumCleanerService = vacuumCleanerService;
        this.manufacturerService = manufacturerService;
        this.orderService = orderService;
        this.orderVacuumCleanerService = orderVacuumCleanerService;
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("vacuumCleaners", vacuumCleanerService.getAllToShow());
        model.addAttribute("manufacturers", manufacturerService.getAll());
        model.addAttribute("maxCost", vacuumCleanerService.getMaxCost());
        model.addAttribute("minCost", vacuumCleanerService.getMinCost());
        return "product/home_appliances/vacuum_cleaner/viewAllVacuumCleaners";
    }

    @GetMapping("/get/{id}")
    public String getOne(@PathVariable Long id, Model model){
        VacuumCleanerPageDTO vacuumCleanerPageDTO = new VacuumCleanerPageDTO(vacuumCleanerService.getOne(id));
        model.addAttribute("product", vacuumCleanerPageDTO);
        return "product/home_appliances/vacuum_cleaner/viewVacuumCleaner";
    }

    @GetMapping("/add")
    public String create(){
        return "product/home_appliances/vacuum_cleaner/addVacuumCleaner";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("vacuumCleanerDTO") @Valid VacuumCleanerDTO vacuumCleanerDTO, @RequestParam MultipartFile file){
        if (file != null && file.getSize() > 0){
            vacuumCleanerService.createFromDTO(vacuumCleanerDTO, file);
        } else {
            vacuumCleanerService.createFromDTO(vacuumCleanerDTO);
        }

        return "redirect:/vacuum-cleaners";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("vacuumCleaner", new VacuumCleanerDTO(vacuumCleanerService.getOne(id)));
        return "product/home_appliances/vacuum_cleaner/updateVacuumCleaner";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid VacuumCleanerDTO vacuumCleanerDTO,
                         @RequestParam MultipartFile file){
        if(file != null && file.getSize() > 0){
            vacuumCleanerService.updateFromDTO(vacuumCleanerDTO.getId(), vacuumCleanerDTO, file);
        } else {
            vacuumCleanerService.updateFromDTO(vacuumCleanerDTO.getId(), vacuumCleanerDTO);
        }
        return "redirect:/vacuum-cleaners";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        vacuumCleanerService.delete(id);
        return "redirect:/vacuum-cleaners";
    }

    @PostMapping("/filter")
    public String getAllWithFilter(@ModelAttribute("vacuumCleanerFilterDTO")@Valid VacuumCleanerFilterDTO vacuumCleanerFilterDTO, Model model) throws SQLException {
        model.addAttribute("vacuumCleanersWithFilter", vacuumCleanerService.getAllWithFilter(vacuumCleanerFilterDTO));
        return "product/home_appliances/vacuum_cleaner/vacuumCleanersWithFilter";
    }

    @PostMapping("/filter-by-model")
    public String getAllByModel(@ModelAttribute("vacuumCleanerByModel")@Valid VacuumCleanerFilterDTO vacuumCleanerFilterDTO, Model model){
        model.addAttribute("vacuumCleanersWithFilter", vacuumCleanerService.getAllByModel(vacuumCleanerFilterDTO));
        return "product/home_appliances/vacuum_cleaner/vacuumCleanersWithFilter";
    }

    @PostMapping("/putInBasket")
    public String putInBasket(@ModelAttribute("idProduct") Long id){
        VacuumCleaner vacuumCleaner = vacuumCleanerService.getOne(id);
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            Order order = orderService.getLastOrderAccount(login);
            if (!order.getIsPaid()) {
                OrderVacuumCleaner orderVacuumCleaner = orderVacuumCleanerService.create(order, vacuumCleaner);
                order.getOrderVacuumCleaners().add(orderVacuumCleaner);
                orderService.updateFromDTO(order.getId(), new OrderDTO(order));
                return "redirect:/vacuum-cleaners";
            }
        }
        Order newOrder = new Order();
        newOrder.setAccount(accountService.getAccountByLogin(login));
        OrderVacuumCleaner orderVacuumCleaner = orderVacuumCleanerService.create(newOrder, vacuumCleaner);
        newOrder.getOrderVacuumCleaners().add(orderVacuumCleaner);
        orderService.updateFromDTO(newOrder.getId(), new OrderDTO(newOrder));
        return "redirect:/vacuum-cleaners";
    }
}
