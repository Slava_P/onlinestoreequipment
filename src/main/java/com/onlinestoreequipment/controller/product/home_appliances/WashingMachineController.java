package com.onlinestoreequipment.controller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineFilterDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachinePageDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderWashingMachine;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.home_appliances.WashingMachineService;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import com.onlinestoreequipment.service.store_service.orders.OrderWashingMachineService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.sql.SQLException;

@Controller
@RequestMapping("/washing-machines")
public class WashingMachineController {

    private WashingMachineService washingMachineService;
    private ManufacturerService manufacturerService;
    private OrderService orderService;
    private OrderWashingMachineService orderWashingMachineService;
    private AccountService accountService;

    public WashingMachineController(WashingMachineService washingMachineService, ManufacturerService manufacturerService, OrderService orderService, OrderWashingMachineService orderWashingMachineService, AccountService accountService){
        this.washingMachineService = washingMachineService;
        this.manufacturerService = manufacturerService;
        this.orderService = orderService;
        this.orderWashingMachineService = orderWashingMachineService;
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model) throws SQLException {
        model.addAttribute("washingMachines", washingMachineService.getAllToShow());
        model.addAttribute("manufacturers", manufacturerService.getAll());
        model.addAttribute("maxCost", washingMachineService.getMaxCost());
        model.addAttribute("minCost", washingMachineService.getMinCost());
        return "product/home_appliances/washing_machine/viewAllWashingMachines";
    }

    @GetMapping("/get/{id}")
    public String getOne(@PathVariable Long id, Model model){
        WashingMachinePageDTO washingMachinePageDTO = new WashingMachinePageDTO(washingMachineService.getOne(id));
        model.addAttribute("product", washingMachinePageDTO);
        return "product/home_appliances/washing_machine/viewWashingMachine";
    }

    @GetMapping("/add")
    public String create(){
        return "product/home_appliances/washing_machine/addWashingMachine";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("washingMachinesDTO") @Valid WashingMachineDTO washingMachineDTO, @RequestParam MultipartFile file){
        washingMachineService.createFromDTO(washingMachineDTO);
        if (file != null && file.getSize() > 0){
            washingMachineService.createFromDTO(washingMachineDTO, file);
        } else {
            washingMachineService.createFromDTO(washingMachineDTO);
        }
        return "redirect:/washing-machines";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("washingMachine", new WashingMachineDTO(washingMachineService.getOne(id)));
        return "product/home_appliances/washing_machine/updateWashingMachine";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid WashingMachineDTO washingMachineDTO,
                         @RequestParam MultipartFile file){
        if(file != null && file.getSize() > 0){
            washingMachineService.updateFromDTO(washingMachineDTO.getId(), washingMachineDTO, file);
        } else {
            washingMachineService.updateFromDTO(washingMachineDTO.getId(), washingMachineDTO);
        }
        return "redirect:/washing-machines";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        washingMachineService.delete(id);
        return "redirect:/washing-machines";
    }

    @PostMapping("/filter")
    public String getAllWithFilter(@ModelAttribute("washingMachineFilterDTO")@Valid WashingMachineFilterDTO washingMachineFilterDTO, Model model) throws SQLException {
        model.addAttribute("washingMachinesWithFilter", washingMachineService.getAllWithFilter(washingMachineFilterDTO));
        return "product/home_appliances/washing_machine/washingMachinesWithFilter";
    }

    @PostMapping("/filter-by-model")
    public String getAllByModel(@ModelAttribute("washingMachineByModel")@Valid WashingMachineFilterDTO washingMachineFilterDTO, Model model){
        model.addAttribute("washingMachinesWithFilter", washingMachineService.getAllByModel(washingMachineFilterDTO));
        return "product/home_appliances/washing_machine/washingMachinesWithFilter";
    }

    @PostMapping("/putInBasket")
    public String putInBasket(@ModelAttribute("idProduct") Long id){
        WashingMachine washingMachine = washingMachineService.getOne(id);
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            Order order = orderService.getLastOrderAccount(login);
            if (!order.getIsPaid()) {
                OrderWashingMachine orderWashingMachine = orderWashingMachineService.create(order, washingMachine);
                order.getOrderWashingMachines().add(orderWashingMachine);
                orderService.updateFromDTO(order.getId(), new OrderDTO(order));
                return "redirect:/washing-machines";
            }
        }
        Order newOrder = new Order();
        newOrder.setAccount(accountService.getAccountByLogin(login));
        OrderWashingMachine orderWashingMachine = orderWashingMachineService.create(newOrder, washingMachine);
        newOrder.getOrderWashingMachines().add(orderWashingMachine);
        orderService.updateFromDTO(newOrder.getId(), new OrderDTO(newOrder));
        return "redirect:/washing-machines";
    }
}
