package com.onlinestoreequipment.controller.product;


import com.onlinestoreequipment.dto.product.ManufacturerDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorFilterDTO;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.home_appliances.IronService;
import com.onlinestoreequipment.service.product.home_appliances.VacuumCleanerService;
import com.onlinestoreequipment.service.product.home_appliances.WashingMachineService;
import com.onlinestoreequipment.service.product.kitchen_appliances.MicrowaveService;
import com.onlinestoreequipment.service.product.kitchen_appliances.RefrigeratorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/manufacturers")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;
    private final RefrigeratorService refrigeratorService;
    private final MicrowaveService microwaveService;
    private final WashingMachineService washingMachineService;
    private final VacuumCleanerService vacuumCleanerService;
    private final IronService ironService;

    public ManufacturerController(ManufacturerService manufacturerService, RefrigeratorService refrigeratorService,
                                  MicrowaveService microwaveService, WashingMachineService washingMachineService,
                                  VacuumCleanerService vacuumCleanerService, IronService ironService){
        this.manufacturerService = manufacturerService;
        this.refrigeratorService = refrigeratorService;
        this.microwaveService = microwaveService;
        this.washingMachineService = washingMachineService;
        this.vacuumCleanerService = vacuumCleanerService;
        this.ironService = ironService;
    }

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("manufacturers", manufacturerService.getAll());
        return "product/manufacturer/viewAllManufacturers";
    }

    @GetMapping("/add")
    public String create(){
        return "product/manufacturer/addManufacturers";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("manufacturerDTO") @Valid ManufacturerDTO manufacturerDTO){
        manufacturerService.createFromDTO(manufacturerDTO);
        return "redirect:/manufacturers";
    }

    @GetMapping("/allProducts/{id}")
    public String allProduct(@PathVariable Long id, Model model){
        model.addAttribute("firmName", manufacturerService.getOne(id).getFirmName());
        model.addAttribute("countRefrigerators", manufacturerService.getCountProduct(id, ProductSubtype.REFRIGERATOR));
        model.addAttribute("countMicrowaves", manufacturerService.getCountProduct(id, ProductSubtype.MICROWAVE));
        model.addAttribute("countWashingMachines", manufacturerService.getCountProduct(id, ProductSubtype.WASHING_MACHINE));
        model.addAttribute("countVacuumCleaners", manufacturerService.getCountProduct(id, ProductSubtype.VACUUM_CLEANER));
        model.addAttribute("countIrons", manufacturerService.getCountProduct(id, ProductSubtype.IRON));
        return "product/manufacturer/allProductCompany";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("manufacturer", new ManufacturerDTO(manufacturerService.getOne(id)));
        return "product/manufacturer/updateManufacturer";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("manufacturerDTO") ManufacturerDTO manufacturerDTO){
        manufacturerService.updateFromDTO(manufacturerDTO.getId(), manufacturerDTO);
        return "redirect:/manufacturers";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        manufacturerService.delete(id);
        return "redirect:/manufacturers";
    }

    @PostMapping("/filter-by-name")
    public String getAllByName(@ModelAttribute("manufacturersByName")@Valid ManufacturerDTO manufacturerDTO, Model model){
        model.addAttribute("manufacturers", manufacturerService.getAllByFirmName(manufacturerDTO));
        return "product/manufacturer/viewAllManufacturers";
    }

    @GetMapping("/{id}/allRefrigerators")
    public String getAllRefrigerators(@PathVariable Long id, Model model){
        Manufacturer manufacturer = manufacturerService.getOne(id);
        model.addAttribute("firmName", manufacturer.getFirmName());
        model.addAttribute("refrigeratorsCompany", manufacturerService.getAllRefrigerators(id));
        model.addAttribute("maxCost", refrigeratorService.getMaxCost(manufacturer));
        model.addAttribute("minCost", refrigeratorService.getMinCost(manufacturer));
        return "product/manufacturer/viewAllRefrigeratorsCompany";
    }

    @GetMapping("/{id}/allMicrowaves")
    public String getAllMicrowaves(@PathVariable Long id, Model model){
        Manufacturer manufacturer = manufacturerService.getOne(id);
        model.addAttribute("firmName", manufacturer.getFirmName());
        model.addAttribute("microwavesCompany", manufacturerService.getAllMicrowaves(id));
        model.addAttribute("maxCost", microwaveService.getMaxCost(manufacturer));
        model.addAttribute("minCost", microwaveService.getMinCost(manufacturer));
        return "product/manufacturer/viewAllMicrowavesCompany";
    }

    @GetMapping("/{id}/allWashingMachines")
    public String getAllWashingMachines(@PathVariable Long id, Model model){
        Manufacturer manufacturer = manufacturerService.getOne(id);
        model.addAttribute("firmName", manufacturerService.getOne(id).getFirmName());
        model.addAttribute("washingMachinesCompany", manufacturerService.getAllWashingMachines(id));
        model.addAttribute("maxCost", washingMachineService.getMaxCost(manufacturer));
        model.addAttribute("minCost", washingMachineService.getMinCost(manufacturer));
        return "product/manufacturer/viewAllWashingMachinesCompany";
    }

    @GetMapping("/{id}/allVacuumCleaners")
    public String getAllVacuumCleaners(@PathVariable Long id, Model model){
        Manufacturer manufacturer = manufacturerService.getOne(id);
        model.addAttribute("firmName", manufacturerService.getOne(id).getFirmName());
        model.addAttribute("vacuumCleanersCompany", manufacturerService.getAllVacuumCleaner(id));
        model.addAttribute("maxCost", vacuumCleanerService.getMaxCost(manufacturer));
        model.addAttribute("minCost", vacuumCleanerService.getMinCost(manufacturer));
        return "product/manufacturer/viewAllVacuumCleanersCompany";
    }

    @GetMapping("/{id}/allIrons")
    public String getAllIrons(@PathVariable Long id, Model model){
        Manufacturer manufacturer = manufacturerService.getOne(id);
        model.addAttribute("firmName", manufacturerService.getOne(id).getFirmName());
        model.addAttribute("ironsCompany", manufacturerService.getAllIrons(id));
        model.addAttribute("maxCost", ironService.getMaxCost(manufacturer));
        model.addAttribute("minCost", ironService.getMinCost(manufacturer));
        return "product/manufacturer/viewAllIronsCompany";
    }





}
