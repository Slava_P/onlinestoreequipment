package com.onlinestoreequipment.controller.product;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("products")
public class ProductController {

    @GetMapping("")
    public String index(Model model){
        return "product/viewProductType";
    }
}
