package com.onlinestoreequipment.controller.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveFilterDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowavePageDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderMicrowaves;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.kitchen_appliances.MicrowaveService;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderMicrowaveService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Controller
@RequestMapping("/microwaves")
public class MicrowaveController {

    private MicrowaveService microwaveService;
    private ManufacturerService manufacturerService;
    private OrderService orderService;
    private OrderMicrowaveService orderMicrowaveService;
    private AccountService accountService;

    public MicrowaveController(MicrowaveService microwaveService, ManufacturerService manufacturerService, OrderService orderService, OrderMicrowaveService orderMicrowaveService, AccountService accountService){
        this.microwaveService = microwaveService;
        this.manufacturerService = manufacturerService;
        this.orderService = orderService;
        this.orderMicrowaveService = orderMicrowaveService;
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("microwaves", microwaveService.getAllToShow());
        model.addAttribute("manufacturers", manufacturerService.getAll());
        model.addAttribute("maxCost", microwaveService.getMaxCost());
        model.addAttribute("minCost", microwaveService.getMinCost());
        return "product/kitchen_appliances/microwave/viewAllMicrowaves";
    }

    @GetMapping("/get/{id}")
    public String getOne(@PathVariable Long id, Model model){
        MicrowavePageDTO microwavePageDTO = new MicrowavePageDTO(microwaveService.getOne(id));
        model.addAttribute("product", microwavePageDTO);
        return "product/kitchen_appliances/microwave/viewMicrowave";
    }

    @GetMapping("/add")
    public String create(){
        return "product/kitchen_appliances/microwave/addMicrowave";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("microwaveDTO") @Valid MicrowaveDTO microwaveDTO, @RequestParam MultipartFile file){
        if (file != null && file.getSize() > 0){
            microwaveService.createFromDTO(microwaveDTO, file);
        } else {
            microwaveService.createFromDTO(microwaveDTO);
        }
        return "redirect:/microwaves";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("microwave", new MicrowaveDTO(microwaveService.getOne(id)));
        return "product/kitchen_appliances/microwave/updateMicrowave";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid MicrowaveDTO microwaveDTO,
                         @RequestParam MultipartFile file){
        if(file != null && file.getSize() > 0){
            microwaveService.updateFromDTO(microwaveDTO.getId(), microwaveDTO, file);
        } else {
            microwaveService.updateFromDTO(microwaveDTO.getId(), microwaveDTO);
        }
        return "redirect:/microwaves";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        microwaveService.delete(id);
        return "redirect:/microwaves";
    }

    @PostMapping("/filter")
    public String getAllWithFilter(@ModelAttribute("microwaveFilterDTO")@Valid MicrowaveFilterDTO microwaveFilterDTO, Model model){
        model.addAttribute("microwavesWithFilter", microwaveService.getAllWithFilter(microwaveFilterDTO));
        return "product/kitchen_appliances/microwave/microwavesWithFilter";
    }

    @PostMapping("/filter-by-model")
    public String getAllByModel(@ModelAttribute("microwaveByModel")@Valid MicrowaveFilterDTO microwaveFilterDTO, Model model){
        model.addAttribute("microwavesWithFilter", microwaveService.getAllByModel(microwaveFilterDTO));
        return "product/kitchen_appliances/microwave/microwavesWithFilter";
    }

    @PostMapping("/putInBasket")
    public String putInBasket(@ModelAttribute("idProduct") Long id){
        Microwave microwave = microwaveService.getOne(id);
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            Order order = orderService.getLastOrderAccount(login);
            if (!order.getIsPaid()) {
                OrderMicrowaves orderMicrowaves = orderMicrowaveService.create(order, microwave);
                order.getOrderMicrowaves().add(orderMicrowaves);
                orderService.updateFromDTO(order.getId(), new OrderDTO(order));
                return "redirect:/microwaves";
            }
        }
        Order newOrder = new Order();
        newOrder.setAccount(accountService.getAccountByLogin(login));
        OrderMicrowaves orderMicrowaves = orderMicrowaveService.create(newOrder, microwave);
        newOrder.getOrderMicrowaves().add(orderMicrowaves);
        orderService.updateFromDTO(newOrder.getId(), new OrderDTO(newOrder));
        return "redirect:/microwaves";
    }
}
