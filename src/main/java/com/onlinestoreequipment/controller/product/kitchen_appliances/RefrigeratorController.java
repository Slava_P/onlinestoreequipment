package com.onlinestoreequipment.controller.product.kitchen_appliances;


import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorFilterDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorPageDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import com.onlinestoreequipment.service.product.ManufacturerService;
import com.onlinestoreequipment.service.product.kitchen_appliances.RefrigeratorService;
import com.onlinestoreequipment.service.store_service.AccountService;
import com.onlinestoreequipment.service.store_service.orders.OrderRefrigeratorService;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Controller
@RequestMapping("/refrigerators")
public class RefrigeratorController {

    private RefrigeratorService refrigeratorService;
    private ManufacturerService manufacturerService;
    private OrderService orderService;
    private OrderRefrigeratorService orderRefrigeratorService;
    private AccountService accountService;

    public RefrigeratorController(RefrigeratorService refrigeratorService, ManufacturerService manufacturerService, OrderService orderService, OrderRefrigeratorService orderRefrigeratorService, AccountService accountService){
        this.refrigeratorService = refrigeratorService;
        this.manufacturerService = manufacturerService;
        this.orderService = orderService;
        this.orderRefrigeratorService = orderRefrigeratorService;
        this.accountService = accountService;
    }

    @GetMapping("")
    public String index(Model model){
        model.addAttribute("refrigerators", refrigeratorService.getAllToShow());
        model.addAttribute("manufacturers", manufacturerService.getAll());
        model.addAttribute("maxCost", refrigeratorService.getMaxCost());
        model.addAttribute("minCost", refrigeratorService.getMinCost());

        return "product/kitchen_appliances/refrigerator/viewAllRefrigerators";
    }

    @GetMapping("/add")
    public String create(){
        return "product/kitchen_appliances/refrigerator/addRefrigerator";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("refrigeratorDTO") @Valid RefrigeratorDTO refrigeratorDTO, @RequestParam MultipartFile file){
        if (file != null && file.getSize() > 0){
            refrigeratorService.createFromDTO(refrigeratorDTO, file);
        } else {
            refrigeratorService.createFromDTO(refrigeratorDTO);
        }

        return "redirect:/refrigerators";
    }

    @GetMapping("/get/{id}")
    public String getOne(@PathVariable Long id, Model model){
        RefrigeratorPageDTO refrigeratorPageDTO = new RefrigeratorPageDTO(refrigeratorService.getOne(id));
        model.addAttribute("product", refrigeratorPageDTO);
        return "product/kitchen_appliances/refrigerator/viewRefrigerator";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("refrigerator", new RefrigeratorDTO(refrigeratorService.getOne(id)));
        return "product/kitchen_appliances/refrigerator/updateRefrigerator";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("accountForm") @Valid RefrigeratorDTO refrigeratorDTO,
                         @RequestParam MultipartFile file){
        if(file != null && file.getSize() > 0){
            refrigeratorService.updateFromDTO(refrigeratorDTO.getId(), refrigeratorDTO, file);
        } else {
            refrigeratorService.updateFromDTO(refrigeratorDTO.getId(), refrigeratorDTO);
        }
        return "redirect:/refrigerators";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        refrigeratorService.delete(id);
        return "redirect:/refrigerators";
    }

    @PostMapping("/filter")
    public String getAllWithFilter(@ModelAttribute("refrigeratorFilterDTO")@Valid RefrigeratorFilterDTO refrigeratorFilterDTO, Model model){
        model.addAttribute("refrigeratorsWithFilter", refrigeratorService.getAllWithFilter(refrigeratorFilterDTO));
        return "product/kitchen_appliances/refrigerator/refrigeratorsWithFilter";
    }

    @PostMapping("/filter-by-model")
    public String getAllByModel(@ModelAttribute("refrigeratorByModel")@Valid RefrigeratorFilterDTO refrigeratorFilterDTO, Model model){
        model.addAttribute("refrigeratorsWithFilter", refrigeratorService.getAllByModel(refrigeratorFilterDTO));
        return "product/kitchen_appliances/refrigerator/refrigeratorsWithFilter";
    }

    @PostMapping("/putInBasket")
    public String putInBasket(@ModelAttribute("idProduct") Long id){
        Refrigerator refrigerator = refrigeratorService.getOne(id);
        SecurityContext context = SecurityContextHolder.getContext();
        String login = context.getAuthentication().getName();
        if (orderService.getAllToDTO(login).size() > 0) {
            Order order = orderService.getLastOrderAccount(login);
            if (!order.getIsPaid()) {
                OrderRefrigerator orderRefrigerator = orderRefrigeratorService.create(order, refrigerator);
                order.getOrderRefrigerators().add(orderRefrigerator);
                orderService.updateFromDTO(order.getId(), new OrderDTO(order));
                return "redirect:/refrigerators";
            }
        }
        Order newOrder = new Order();
        newOrder.setAccount(accountService.getAccountByLogin(login));
        OrderRefrigerator orderRefrigerator = orderRefrigeratorService.create(newOrder, refrigerator);
        newOrder.getOrderRefrigerators().add(orderRefrigerator);
        orderService.updateFromDTO(newOrder.getId(), new OrderDTO(newOrder));
        return "redirect:/refrigerators";
        //TODO Если пользователь не авторизирован, перебрасывать его на страницу с авторизацией по кнопке "В корзину"
    }
}
