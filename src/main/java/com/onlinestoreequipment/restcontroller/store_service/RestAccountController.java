package com.onlinestoreequipment.restcontroller.store_service;

import com.onlinestoreequipment.dto.store_service.AccountDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.service.store_service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/account")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Пользователи"
        ,description = "контроллер для работы с пользователями")
public class RestAccountController {

    private AccountService accountService;

    public RestAccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Operation(description = "Найти пользователя по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(accountService.getOne(id));
    }

    @Operation(description = "Найти всех пользователей")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Account>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(accountService.getAll());
    }

    @Operation(description = "Добавить пользователя")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> add(@RequestBody AccountDTO accountDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(accountService.createFromDTO(accountDTO));
    }

    @Operation(description = "Удаление пользователя из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        accountService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Пользователь удален");
    }

    @Operation(description = "Обновление имеющегося пользователя по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> update(@RequestBody AccountDTO accountDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(accountService.updateFromDTO(id, accountDTO));
    }
}
