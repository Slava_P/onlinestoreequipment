package com.onlinestoreequipment.restcontroller.store_service;

import com.onlinestoreequipment.dto.store_service.AccountDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.service.store_service.orders.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/orders")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Заказы"
        ,description = "контроллер для работы с заказами")
public class RestOrderController {

    private OrderService orderService;

    public RestOrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Operation(description = "Найти заказ по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(orderService.getOne(id));
    }

    @Operation(description = "Добавить заказ")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> add(@RequestBody OrderDTO orderDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.createFromDTO(orderDTO));
    }

    @Operation(description = "Удаление заказа из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        orderService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Заказ удален");
    }

    @Operation(description = "Обновление имеющегося заказа по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> update(@RequestBody OrderDTO orderDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.updateFromDTO(id, orderDTO));
    }
}
