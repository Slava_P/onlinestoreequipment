package com.onlinestoreequipment.restcontroller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerDTO;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.service.product.home_appliances.VacuumCleanerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/vacuum-cleaners")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Пылесосы"
        ,description = "контроллер для работы с пылесосами")
public class RestVacuumCleanerController {

    private VacuumCleanerService vacuumCleanerService;


    public RestVacuumCleanerController(VacuumCleanerService vacuumCleanerService) {
        this.vacuumCleanerService = vacuumCleanerService;
    }

    @Operation(description = "Найти пылесос по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacuumCleaner> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(vacuumCleanerService.getOne(id));
    }

    @Operation(description = "Найти все пылесосы")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VacuumCleaner>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(vacuumCleanerService.getAll());
    }

    @Operation(description = "Добавить пылесос")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacuumCleaner> add(@RequestBody VacuumCleanerDTO vacuumCleanerDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(vacuumCleanerService.createFromDTO(vacuumCleanerDTO));
    }

    @Operation(description = "Удаление пылесоса из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        vacuumCleanerService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Пылесос удален");
    }

    @Operation(description = "Обновление имеющегося пылесоса по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacuumCleaner> update(@RequestBody VacuumCleanerDTO vacuumCleanerDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(vacuumCleanerService.updateFromDTO(id, vacuumCleanerDTO));
    }
}
