package com.onlinestoreequipment.restcontroller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.IronDTO;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.service.product.home_appliances.IronService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/irons")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Утюги"
        ,description = "контроллер для работы с утюгом")
public class RestIronController {

    private IronService ironService;

    public RestIronController(IronService ironService) {
        this.ironService = ironService;
    }

    @Operation(description = "Найти утюг по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iron> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(ironService.getOne(id));
    }

    @Operation(description = "Найти все утюги")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Iron>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(ironService.getAll());
    }

    @Operation(description = "Добавить утюг")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iron> add(@RequestBody IronDTO ironDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ironService.createFromDTO(ironDTO));
    }

    @Operation(description = "Удаление утюга из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        ironService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Утюг удален");
    }

    @Operation(description = "Обновление имеющегося утюга по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iron> update(@RequestBody IronDTO ironDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(ironService.updateFromDTO(id, ironDTO));
    }
}
