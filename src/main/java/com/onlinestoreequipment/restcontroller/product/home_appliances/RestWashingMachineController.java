package com.onlinestoreequipment.restcontroller.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineDTO;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.service.product.home_appliances.WashingMachineService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/washing-machines")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Стиральные машины"
        ,description = "контроллер для работы со стиральными машинами")
public class RestWashingMachineController {

    private WashingMachineService washingMachineService;

    public RestWashingMachineController(WashingMachineService washingMachineService){
        this.washingMachineService = washingMachineService;
    }


    @Operation(description = "Найти стиральную машину по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WashingMachine> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(washingMachineService.getOne(id));
    }

    @Operation(description = "Найти все стиральные машины")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<WashingMachine>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(washingMachineService.getAll());
    }

    @Operation(description = "Добавить стиральную машину")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WashingMachine> add(@RequestBody WashingMachineDTO washingMachineDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(washingMachineService.createFromDTO(washingMachineDTO));
    }

    @Operation(description = "Удаление стиральной машины из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        washingMachineService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Стиральная машина удалена");
    }

    @Operation(description = "Обновление имеющейся стиральных машин по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WashingMachine> update(@RequestBody WashingMachineDTO washingMachineDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(washingMachineService.updateFromDTO(id, washingMachineDTO));
    }
}
