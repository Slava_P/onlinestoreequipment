package com.onlinestoreequipment.restcontroller.product;

import com.onlinestoreequipment.dto.product.ManufacturerDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.service.product.ManufacturerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/manufacturers")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Производители"
        ,description = "контроллер для работы с производителями")
public class RestManufacturerController {

    private ManufacturerService manufacturerService;


    public RestManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @Operation(description = "Найти производителя по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Manufacturer> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(manufacturerService.getOne(id));
    }

    @Operation(description = "Найти всех производителей")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(manufacturerService.getAll());
    }

    @Operation(description = "Добавить производителя")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Manufacturer> add(@RequestBody ManufacturerDTO manufacturerDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(manufacturerService.createFromDTO(manufacturerDTO));
    }

    @Operation(description = "Удаление производителя из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        manufacturerService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Производитель удален");
    }

    @Operation(description = "Обновление имеющегося производителя по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Manufacturer> update(@RequestBody ManufacturerDTO manufacturerDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(manufacturerService.updateFromDTO(id, manufacturerDTO));
    }
}
