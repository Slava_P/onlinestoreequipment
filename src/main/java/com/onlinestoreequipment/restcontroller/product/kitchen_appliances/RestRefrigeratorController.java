package com.onlinestoreequipment.restcontroller.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.service.product.kitchen_appliances.RefrigeratorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;

@RestController
@RequestMapping("/rest/refrigerators")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Холодильники"
        ,description = "контроллер для работы с холодильниками")
public class RestRefrigeratorController {

    private RefrigeratorService refrigeratorService;

    public RestRefrigeratorController(RefrigeratorService refrigeratorService) {
        this.refrigeratorService = refrigeratorService;
    }

    @Operation(description = "Найти холодильник по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Refrigerator> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(refrigeratorService.getOne(id));
    }

    @Operation(description = "Найти все холодильники")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Refrigerator>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(refrigeratorService.getAll());
    }

    @Operation(description = "Добавить холодильник")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Refrigerator> add(@RequestBody RefrigeratorDTO refrigeratorDTO, MultipartFile file){
        System.out.println(1);
        if (file != null && file.getSize() > 0){
            System.out.println(2);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(refrigeratorService.createFromDTO(refrigeratorDTO, file));
        } else {
            System.out.println(3);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(refrigeratorService.createFromDTO(refrigeratorDTO));
        }
    }

    @Operation(description = "Удаление холодильника из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        refrigeratorService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Холодильник удален");
    }

    @Operation(description = "Обновление имеющегося холодильника по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Refrigerator> update(@RequestBody RefrigeratorDTO refrigeratorDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(refrigeratorService.updateFromDTO(id, refrigeratorDTO));
    }


}
