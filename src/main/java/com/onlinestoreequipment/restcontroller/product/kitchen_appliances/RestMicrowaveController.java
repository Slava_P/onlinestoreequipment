package com.onlinestoreequipment.restcontroller.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.service.product.kitchen_appliances.MicrowaveService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/microwaves")
@CrossOrigin(origins = "*", allowedHeaders = "*") //(если надо писать фронт отдельно) чтобы браузер не блокировал запросы
@Tag(name = "Микроволновые печи"
        ,description = "контроллер для работы с микроволновыми печами")
public class RestMicrowaveController {

    private MicrowaveService microwaveService;

    public RestMicrowaveController(MicrowaveService microwaveService) {
        this.microwaveService = microwaveService;
    }

    @Operation(description = "Найти микроволновую печь по id")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Microwave> getOne(@RequestParam("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(microwaveService.getOne(id));
    }

    @Operation(description = "Найти все микроволновые печи")
    @RequestMapping(value = "getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Microwave>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(microwaveService.getAll());
    }

    @Operation(description = "Добавить микроволновую печь")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Microwave> add(@RequestBody MicrowaveDTO microwaveDTO){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(microwaveService.createFromDTO(microwaveDTO));
    }

    @Operation(description = "Удаление микроволновой печи из магазина и заказов покупателей")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "id") Long id){
        microwaveService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Микроволновая печь удалена");
    }

    @Operation(description = "Обновление имеющейся микроволновой печи по id")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Microwave> update(@RequestBody MicrowaveDTO microwaveDTO, @RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.CREATED).body(microwaveService.updateFromDTO(id, microwaveDTO));
    }

}
