package com.onlinestoreequipment.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CostDTO {

    private Integer minCostDTO;
    private Integer maxCostDTO;
}

