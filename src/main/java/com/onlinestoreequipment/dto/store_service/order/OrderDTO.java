package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.store_service.orders.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDTO {

    private Long id;
    private Boolean isPaid;
    private Integer costOrder;
    private LocalDate dateCreate;
    private LocalDate datePaid;
    private Set<ProductOrderDTO> products = new HashSet<>();

    public OrderDTO(Order order){
        this.id = order.getId();
        this.isPaid = order.getIsPaid();
        this.dateCreate = order.getDateCreate();
        this.datePaid = order.getDatePaid();
        this.products = productOrderDTOs(order);
        setCostOrder();
    }

    private Set<ProductOrderDTO> productOrderDTOs(Order order){
        Set<ProductOrderDTO> products = new HashSet<>();

        products.addAll(RefrigeratorOrderDTO.refrigeratorOrderDTOSet(order));
        products.addAll(MicrowaveOrderDTO.microwaveOrderDTOSet(order));
        products.addAll(WashingMachineOrderDTO.washingMachineOrderDTOSet(order));
        products.addAll(VacuumCleanerOrderDTO.vacuumCleanerOrderDTOSet(order));
        products.addAll(IronOrderDTO.ironOrderDTOSet(order));

        return products;
    }

    private void setCostOrder(){
        Integer cost = 0;
        for (ProductOrderDTO productOrderDTO: products){
            cost += productOrderDTO.getAllCost();
        }

        this.costOrder = cost;
    }

}
