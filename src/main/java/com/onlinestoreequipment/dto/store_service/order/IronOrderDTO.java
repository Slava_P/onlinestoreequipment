package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderIron;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class IronOrderDTO extends ProductOrderDTO{

    public IronOrderDTO(Iron iron){
        this.setId(iron.getId());
        this.setModel(iron.getModel());
        this.setManufacturerName(iron.getManufacturer().getFirmName());
        this.setImg(iron.getImgPath());
        this.setProductSubtype(iron.getProductSubtype().getProductSubtype());
        this.setCost(iron.getCost());
    }

    public static Set<IronOrderDTO> ironOrderDTOSet(Order order){
        Set<IronOrderDTO> ironOrderDTOs = new HashSet<>();
        Set<OrderIron> orderIrons = order.getOrderIrons();
        for (OrderIron orderIron: orderIrons){
            IronOrderDTO ironOrderDTO = new IronOrderDTO(orderIron.getIron());
            ironOrderDTO.setAmountInOrder(orderIron.getAmount());
            ironOrderDTO.setAllCost();
            ironOrderDTOs.add(ironOrderDTO);
        }
        return ironOrderDTOs;
    }

    private void setAllCost(){
        this.setAllCost(this.getCost() * this.getAmountInOrder());
    }
}
