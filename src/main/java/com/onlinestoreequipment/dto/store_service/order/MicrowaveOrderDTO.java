package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderMicrowaves;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class MicrowaveOrderDTO extends ProductOrderDTO {

    public MicrowaveOrderDTO(Microwave microwave){
        this.setId(microwave.getId());
        this.setModel(microwave.getModel());
        this.setManufacturerName(microwave.getManufacturer().getFirmName());
        this.setImg(microwave.getImgPath());
        this.setProductSubtype(microwave.getProductSubtype().getProductSubtype());
        this.setCost(microwave.getCost());
    }

    public static Set<MicrowaveOrderDTO> microwaveOrderDTOSet(Order order){
        Set<MicrowaveOrderDTO> microwaveOrderDTOs = new HashSet<>();
        Set<OrderMicrowaves> orderMicrowaves = order.getOrderMicrowaves();
        for (OrderMicrowaves orderMicrowave: orderMicrowaves){
            MicrowaveOrderDTO microwaveOrderDTO = new MicrowaveOrderDTO(orderMicrowave.getMicrowave());
            microwaveOrderDTO.setAmountInOrder(orderMicrowave.getAmount());
            microwaveOrderDTO.setAllCost();
            microwaveOrderDTOs.add(microwaveOrderDTO);
        }
        return microwaveOrderDTOs;
    }

    private void setAllCost(){
        this.setAllCost(this.getCost() * this.getAmountInOrder());
    }
}
