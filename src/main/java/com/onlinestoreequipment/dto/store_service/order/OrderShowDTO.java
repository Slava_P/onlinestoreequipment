package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.store_service.orders.Order;
import lombok.*;

import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderShowDTO {

    private Long id;
    private String isPaid;
    private String dateCreate;
    private String datePaid;
    private Integer costOrder;
    private String address;

    public OrderShowDTO(Order order){
        this.id = order.getId();
        this.address = order.getAddress();
        if(order.getIsPaid()){
            this.isPaid = "Оплачено";
        } else {
            this.isPaid = "Не оплачено";
        }

        this.costOrder = order.getCostOrder();

        if (order.getDateCreate() == null){
            this.dateCreate = "----";
        } else {
            this.dateCreate = order.getDateCreate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }

        if (order.getDatePaid() == null){
            this.datePaid = "----";
        } else {
            this.datePaid = order.getDatePaid().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }
    }
}
