package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class RefrigeratorOrderDTO extends ProductOrderDTO{

    public RefrigeratorOrderDTO(Refrigerator refrigerator){
        this.setId(refrigerator.getId());
        this.setModel(refrigerator.getModel());
        this.setManufacturerName(refrigerator.getManufacturer().getFirmName());
        this.setImg(refrigerator.getImgPath());
        this.setProductSubtype(refrigerator.getProductSubtype().getProductSubtype());
        this.setCost(refrigerator.getCost());
    }

    public static Set<RefrigeratorOrderDTO> refrigeratorOrderDTOSet(Order order){
        Set<RefrigeratorOrderDTO> refrigeratorOrderDTOs = new HashSet<>();
        Set<OrderRefrigerator> orderRefrigerators = order.getOrderRefrigerators();
        for (OrderRefrigerator orderRefrigerator: orderRefrigerators){
            RefrigeratorOrderDTO refrigeratorOrderDTO = new RefrigeratorOrderDTO(orderRefrigerator.getRefrigerator());
            refrigeratorOrderDTO.setAmountInOrder(orderRefrigerator.getAmount());
            refrigeratorOrderDTO.setAllCost();
            refrigeratorOrderDTOs.add(refrigeratorOrderDTO);
        }
        return refrigeratorOrderDTOs;
    }

    private void setAllCost(){
        this.setAllCost(this.getCost() * this.getAmountInOrder());
    }

}
