package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderWashingMachine;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class WashingMachineOrderDTO extends ProductOrderDTO{

    public WashingMachineOrderDTO(WashingMachine washingMachine){
        this.setId(washingMachine.getId());
        this.setModel(washingMachine.getModel());
        this.setManufacturerName(washingMachine.getManufacturer().getFirmName());
        this.setImg(washingMachine.getImgPath());
        this.setProductSubtype(washingMachine.getProductSubtype().getProductSubtype());
        this.setCost(washingMachine.getCost());
    }

    public static Set<WashingMachineOrderDTO> washingMachineOrderDTOSet(Order order){
        Set<WashingMachineOrderDTO> washingMachineOrderDTOs = new HashSet<>();
        Set<OrderWashingMachine> orderWashingMachines = order.getOrderWashingMachines();
        for (OrderWashingMachine orderWashingMachine: orderWashingMachines){
            WashingMachineOrderDTO washingMachineOrderDTO = new WashingMachineOrderDTO(orderWashingMachine.getWashingMachine());
            washingMachineOrderDTO.setAmountInOrder(orderWashingMachine.getAmount());
            washingMachineOrderDTO.setAllCost();
            washingMachineOrderDTOs.add(washingMachineOrderDTO);
        }
        return washingMachineOrderDTOs;
    }

    private void setAllCost(){
        this.setAllCost(this.getCost() * this.getAmountInOrder());
    }
}
