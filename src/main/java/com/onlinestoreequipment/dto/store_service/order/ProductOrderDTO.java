package com.onlinestoreequipment.dto.store_service.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class ProductOrderDTO {

    private Long id;
    private String model;
    private String manufacturerName;
    private String img;
    private Integer amountInOrder;
    private Integer cost;
    private Integer allCost;
    private String productSubtype;

}
