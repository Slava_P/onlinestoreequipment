package com.onlinestoreequipment.dto.store_service.order;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderVacuumCleaner;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class VacuumCleanerOrderDTO extends ProductOrderDTO{

    public VacuumCleanerOrderDTO(VacuumCleaner vacuumCleaner){
        this.setId(vacuumCleaner.getId());
        this.setModel(vacuumCleaner.getModel());
        this.setManufacturerName(vacuumCleaner.getManufacturer().getFirmName());
        this.setImg(vacuumCleaner.getImgPath());
        this.setProductSubtype(vacuumCleaner.getProductSubtype().getProductSubtype());
        this.setCost(vacuumCleaner.getCost());
    }

    public static Set<VacuumCleanerOrderDTO> vacuumCleanerOrderDTOSet(Order order){
        Set<VacuumCleanerOrderDTO> vacuumCleanerOrderDTOs = new HashSet<>();
        Set<OrderVacuumCleaner> orderVacuumCleaners = order.getOrderVacuumCleaners();
        for (OrderVacuumCleaner orderVacuumCleaner: orderVacuumCleaners){
            VacuumCleanerOrderDTO vacuumCleanerOrderDTO = new VacuumCleanerOrderDTO(orderVacuumCleaner.getVacuumCleaner());
            vacuumCleanerOrderDTO.setAmountInOrder(orderVacuumCleaner.getAmount());
            vacuumCleanerOrderDTO.setAllCost();
            vacuumCleanerOrderDTOs.add(vacuumCleanerOrderDTO);
        }
        return vacuumCleanerOrderDTOs;
    }

    private void setAllCost(){
        this.setAllCost(this.getCost() * this.getAmountInOrder());
    }
}
