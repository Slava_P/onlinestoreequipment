package com.onlinestoreequipment.dto.store_service;

import com.onlinestoreequipment.dto.GenericDTO;
import com.onlinestoreequipment.model.store_service.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccountDTO extends GenericDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String telephone;
    private String email;
    private String address;
    private String login;
    private String password;
    private String role;

    public AccountDTO(Account account){
        this.id = account.getId();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.middleName = account.getMiddleName();
        this.birthDate = account.getBirthDate().format(DateTimeFormatter.ISO_DATE);
        this.telephone = account.getTelephone();
        this.email = account.getEmail();
        this.address = account.getAddress();
        this.login = account.getLogin();
        this.password = account.getPassword();
        this.role = account.getRole();
    }
}
