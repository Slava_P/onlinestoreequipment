package com.onlinestoreequipment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Setter
@Getter
public abstract class GenericDTO {

    private String createBy;
    private LocalDateTime createWhen;
}
