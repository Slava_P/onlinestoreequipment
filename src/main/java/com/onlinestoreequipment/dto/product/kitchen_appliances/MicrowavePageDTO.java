package com.onlinestoreequipment.dto.product.kitchen_appliances;

import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MicrowavePageDTO extends MicrowaveDTO{

    private String heightToShow;
    private String widthToShow;
    private String depthToShow;
    private String weightToShow;

    public MicrowavePageDTO(Microwave microwave){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.setId(microwave.getId());
        this.setImgPath(microwave.getImgPath());
        this.setModel(microwave.getModel());
        this.setColor(microwave.getColor());
        this.setAmount(microwave.getAmount());
        this.setCost(microwave.getCost());
        this.setDescription(microwave.getDescription());
        this.setProductType(microwave.getProductType());
        this.setProductSubtype(microwave.getProductSubtype());
        this.setFirmName(microwave.getManufacturer().getFirmName());
        this.setPowerConsumption(microwave.getPowerConsumption());
        this.setMicrowavePower(microwave.getMicrowavePower());
        this.setVolume(microwave.getVolume());
        this.heightToShow= decimalFormat.format(microwave.getHeight());
        this.widthToShow = decimalFormat.format(microwave.getWidth());
        this.depthToShow = decimalFormat.format(microwave.getDepth());
        this.weightToShow = decimalFormat.format(microwave.getWeight());

    }
}
