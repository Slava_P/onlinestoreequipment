package com.onlinestoreequipment.dto.product.kitchen_appliances;

import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class RefrigeratorShowDTO {

    private Long id;
    private String model;
    private Integer cost;
    private String productSubtype;
    private String imgPath;
    private String manufacturerName;
    private Integer refrigeChamberVolume;
    private Integer freezingVolume;
    private String dimensions;

    public RefrigeratorShowDTO(Refrigerator refrigerator){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.id = refrigerator.getId();
        this.model = refrigerator.getModel();
        this.cost = refrigerator.getCost();
        this.productSubtype = refrigerator.getProductSubtype().getProductSubtype();
        this.imgPath = refrigerator.getImgPath();
        this.manufacturerName = refrigerator.getManufacturer().getFirmName();
        this.freezingVolume = refrigerator.getFreezingVolume();
        this.refrigeChamberVolume = refrigerator.getRefrigeChamberVolume();
        this.dimensions = decimalFormat.format(refrigerator.getHeight()) + "x" + decimalFormat.format(refrigerator.getWidth()) + "x" + decimalFormat.format(refrigerator.getDepth());
    }
}
