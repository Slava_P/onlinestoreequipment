package com.onlinestoreequipment.dto.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.ProductDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RefrigeratorDTO extends ProductDTO {

    private Long id;
    private String firmName;
    private Integer powerConsumption;
    private Integer freezingPower;
    private Integer freezingVolume;
    private Integer refrigeChamberVolume;
    private Double height;
    private Double width;
    private Double depth;

    public RefrigeratorDTO(Refrigerator refrigerator){

        this.setCreateBy(refrigerator.getCreateBy());
        this.setCreateWhen(refrigerator.getCreateWhen());
        this.setImgPath(refrigerator.getImgPath());
        this.setModel(refrigerator.getModel());
        this.setColor(refrigerator.getColor());
        this.setWeight(refrigerator.getWeight());
        this.setAmount(refrigerator.getAmount());
        this.setCost(refrigerator.getCost());
        this.setDescription(refrigerator.getDescription());
        this.setProductType(refrigerator.getProductType());
        this.setProductSubtype(refrigerator.getProductSubtype());
        this.id = refrigerator.getId();
        this.firmName = refrigerator.getManufacturer().getFirmName();
        this.powerConsumption = refrigerator.getPowerConsumption();
        this.freezingPower = refrigerator.getFreezingPower();
        this.freezingVolume = refrigerator.getFreezingVolume();
        this.refrigeChamberVolume = refrigerator.getRefrigeChamberVolume();
        this.height = refrigerator.getHeight();
        this.width = refrigerator.getWidth();
        this.depth = refrigerator.getDepth();
    }

}
