package com.onlinestoreequipment.dto.product.kitchen_appliances;


import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class MicrowaveShowDTO {

    private Long id;
    private String model;
    private Integer cost;
    private String productSubtype;
    private String imgPath;
    private String manufacturerName;
    private Integer microwave_power;
    private Integer volume;
    private String dimensions;

    public MicrowaveShowDTO(Microwave microwave){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.id = microwave.getId();
        this.model = microwave.getModel();
        this.cost = microwave.getCost();
        this.productSubtype = microwave.getProductSubtype().getProductSubtype();
        this.imgPath = microwave.getImgPath();
        this.manufacturerName = microwave.getManufacturer().getFirmName();
        this.microwave_power = microwave.getMicrowavePower();
        this.volume = microwave.getVolume();
        this.dimensions = decimalFormat.format(microwave.getHeight()) + "x" + decimalFormat.format(microwave.getWidth()) + "x" + decimalFormat.format(microwave.getDepth());
    }

}
