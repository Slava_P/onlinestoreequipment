package com.onlinestoreequipment.dto.product.kitchen_appliances;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RefrigeratorFilterDTO {

    private String firmName = "";
    private Integer minCostDTO;
    private Integer maxCostDTO;
    private Integer meanHeight;
    private Integer meanWidth;
    private Integer meanDepth;
    private String model;

    public Double getMinHeight(){
        if (meanHeight == 150){
            return 0.0;
        }
        return meanHeight - 10.0;
    }
    public Double getMaxHeight(){
        if (meanHeight == 210){
            return 10000.0;
        }
        return meanHeight + 10.0;
    }
    public Double getMinWidth(){
        if (meanWidth == 40){
            return 0.0;
        }
        return meanWidth - 10.0;
    }
    public Double getMaxWidth(){
        if (meanWidth == 100){
            return 10000.0;
        }
        return meanWidth + 10.0;
    }
    public Double getMinDepth(){
        if (meanDepth == 45){
            return 0.0;
        }
        return meanDepth - 5.0;
    }
    public Double getMaxDepth(){
        if (meanDepth == 75){
            return 10000.0;
        }
        return meanDepth + 5.0;
    }
}
