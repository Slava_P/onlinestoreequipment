package com.onlinestoreequipment.dto.product.kitchen_appliances;

import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RefrigeratorPageDTO extends RefrigeratorDTO{

    private String heightToShow;
    private String widthToShow;
    private String depthToShow;
    private String weightToShow;

    public RefrigeratorPageDTO(Refrigerator refrigerator){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.setCreateBy(refrigerator.getCreateBy());
        this.setCreateWhen(refrigerator.getCreateWhen());
        this.setImgPath(refrigerator.getImgPath());
        this.setModel(refrigerator.getModel());
        this.setColor(refrigerator.getColor());
        this.setAmount(refrigerator.getAmount());
        this.setCost(refrigerator.getCost());
        this.setDescription(refrigerator.getDescription());
        this.setProductType(refrigerator.getProductType());
        this.setProductSubtype(refrigerator.getProductSubtype());
        this.setId(refrigerator.getId());
        this.setFirmName(refrigerator.getManufacturer().getFirmName());
        this.setPowerConsumption(refrigerator.getPowerConsumption());
        this.setFreezingPower(refrigerator.getFreezingPower());
        this.setFreezingVolume(refrigerator.getFreezingVolume());
        this.setRefrigeChamberVolume(refrigerator.getRefrigeChamberVolume());
        this.heightToShow= decimalFormat.format(refrigerator.getHeight());
        this.widthToShow = decimalFormat.format(refrigerator.getWidth());
        this.depthToShow = decimalFormat.format(refrigerator.getDepth());
        this.weightToShow = decimalFormat.format(refrigerator.getWeight());
    }
}
