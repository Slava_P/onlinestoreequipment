package com.onlinestoreequipment.dto.product.kitchen_appliances;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MicrowaveFilterDTO {

    private String firmName = "";
    private Integer minCostDTO;
    private Integer maxCostDTO;
    private Integer meanHeight;
    private Integer meanWidth;
    private Integer meanDepth;
    private String model;
//    private Integer meanVolume;


    public Double getMinHeight(){
        if (meanHeight == 20){
            return 0.0;
        }
        return meanHeight - 5.0;
    }
    public Double getMaxHeight(){
        if (meanHeight == 40){
            return 10000.0;
        }
        return meanHeight + 5.0;
    }
    public Double getMinWidth(){
        if (meanWidth == 25){
            return 0.0;
        }
        return meanWidth - 5.0;
    }
    public Double getMaxWidth(){
        if (meanWidth == 55){
            return 10000.0;
        }
        return meanWidth + 5.0;
    }
    public Double getMinDepth(){
        if (meanDepth == 25){
            return 0.0;
        }
        return meanDepth - 5.0;
    }
    public Double getMaxDepth(){
        if (meanDepth == 45){
            return 10000.0;
        }
        return meanDepth + 5.0;
    }

//    public Integer getMinVolume(){
//        if (meanDepth == 18){
//            return 0;
//        }
//        return meanDepth - 2;
//    }
//
//    public Integer getMaxVolume(){
//        if (meanDepth == 30){
//            return 10000;
//        }
//        return meanDepth + 2;
//    }
}
