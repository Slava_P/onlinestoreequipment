package com.onlinestoreequipment.dto.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.ProductDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MicrowaveDTO extends ProductDTO {

    private Long id;
    private String firmName;
    private Integer powerConsumption;
    private Integer microwavePower;
    private Integer volume;
    private Double height;
    private Double width;
    private Double depth;

    public MicrowaveDTO(Microwave microwave){
        this.setCreateBy(microwave.getCreateBy());
        this.setCreateWhen(microwave.getCreateWhen());
        this.setImgPath(microwave.getImgPath());
        this.setModel(microwave.getModel());
        this.setColor(microwave.getColor());
        this.setWeight(microwave.getWeight());
        this.setAmount(microwave.getAmount());
        this.setCost(microwave.getCost());
        this.setDescription(microwave.getDescription());
        this.setProductType(microwave.getProductType());
        this.setProductSubtype(microwave.getProductSubtype());
        this.id = microwave.getId();
        this.firmName = microwave.getManufacturer().getFirmName();
        this.powerConsumption = microwave.getPowerConsumption();
        this.microwavePower = microwave.getMicrowavePower();
        this.volume = microwave.getVolume();
        this.height = microwave.getHeight();
        this.width = microwave.getWidth();
        this.depth = microwave.getDepth();
    }

}
