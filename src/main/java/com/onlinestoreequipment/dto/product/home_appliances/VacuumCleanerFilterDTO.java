package com.onlinestoreequipment.dto.product.home_appliances;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VacuumCleanerFilterDTO {

    private String firmName = "";
    private Integer minCostDTO;
    private Integer maxCostDTO;
    private Integer meanSuctionPower;
    private Integer meanMaxNoiseLevel;
    private String model;

    public Integer getMinSuctionPower(){
        if (meanSuctionPower == 300){
            return 0;
        }
        return meanSuctionPower - 100;
    }
    public Integer getMaxSuctionPower(){
        if (meanSuctionPower == 900){
            return 100000;
        }
        return meanSuctionPower + 100;
    }

    public Integer getMinMaxNoiseLevel(){
        if (meanMaxNoiseLevel == 55){
            return 0;
        }
        return meanMaxNoiseLevel - 5;
    }
    public Integer getMaxMaxNoiseLevel(){
        if (meanMaxNoiseLevel == 85){
            return 10000;
        }
        return meanMaxNoiseLevel + 5;
    }
}
