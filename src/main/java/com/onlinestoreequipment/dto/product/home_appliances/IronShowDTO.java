package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class IronShowDTO {

    private Long id;
    private String model;
    private Integer cost;
    private String productSubtype;
    private String imgPath;
    private String manufacturerName;
    private Integer waterTankVolume;
    private Integer powerConsumption;
    private String powerCordLength;

    public IronShowDTO(Iron iron){
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        this.id = iron.getId();
        this.model = iron.getModel();
        this.cost = iron.getCost();
        this.productSubtype = iron.getProductSubtype().getProductSubtype();
        this.imgPath = iron.getImgPath();
        this.manufacturerName = iron.getManufacturer().getFirmName();
        this.waterTankVolume = iron.getWaterTankVolume();
        this.powerConsumption = iron.getPowerConsumption();
        this.powerCordLength = decimalFormat.format(iron.getPowerCordLength());
    }
}
