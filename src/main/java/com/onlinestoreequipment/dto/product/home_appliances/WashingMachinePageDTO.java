package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WashingMachinePageDTO extends WashingMachineDTO{

    private String heightToShow;
    private String widthToShow;
    private String depthToShow;
    private String weightToShow;
    private String motorTypeShow;
    private String waterConsumptionShow;

    public WashingMachinePageDTO(WashingMachine washingMachine){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.setId(washingMachine.getId());
        this.setImgPath(washingMachine.getImgPath());
        this.setModel(washingMachine.getModel());
        this.setColor(washingMachine.getColor());
        this.setAmount(washingMachine.getAmount());
        this.setCost(washingMachine.getCost());
        this.setDescription(washingMachine.getDescription());
        this.setProductType(washingMachine.getProductType());
        this.setProductSubtype(washingMachine.getProductSubtype());
        this.setFirmName(washingMachine.getManufacturer().getFirmName());
        this.setPowerConsumption(washingMachine.getPowerConsumption());
        this.setMaxSpinSpeed(washingMachine.getMaxSpinSpeed());
        this.setMaxLoad(washingMachine.getMaxLoad());
        this.motorTypeShow = washingMachine.getMotorType().getType();
        this.waterConsumptionShow = decimalFormat.format(washingMachine.getWaterConsumption());
        this.heightToShow= decimalFormat.format(washingMachine.getHeight());
        this.widthToShow = decimalFormat.format(washingMachine.getWidth());
        this.depthToShow = decimalFormat.format(washingMachine.getDepth());
        this.weightToShow = decimalFormat.format(washingMachine.getWeight());
    }
}
