package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.dto.GenericDTO;
import com.onlinestoreequipment.dto.product.ProductDTO;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VacuumCleanerDTO extends ProductDTO {

    private Long id;
    private String firmName;
    private Integer powerConsumption;
    private Integer suctionPower;
    private Integer maxNoiseLevel;
    private Double powerCordLength;
    private Double height;
    private Double width;
    private Double depth;

    public VacuumCleanerDTO(VacuumCleaner vacuumCleaner){
        this.setCreateBy(vacuumCleaner.getCreateBy());
        this.setCreateWhen(vacuumCleaner.getCreateWhen());
        this.setImgPath(vacuumCleaner.getImgPath());
        this.setModel(vacuumCleaner.getModel());
        this.setColor(vacuumCleaner.getColor());
        this.setWeight(vacuumCleaner.getWeight());
        this.setAmount(vacuumCleaner.getAmount());
        this.setCost(vacuumCleaner.getCost());
        this.setDescription(vacuumCleaner.getDescription());
        this.setProductType(vacuumCleaner.getProductType());
        this.setProductSubtype(vacuumCleaner.getProductSubtype());
        this.id = vacuumCleaner.getId();
        this.firmName = vacuumCleaner.getManufacturer().getFirmName();
        this.powerConsumption = vacuumCleaner.getPowerConsumption();
        this.suctionPower = vacuumCleaner.getSuctionPower();
        this.maxNoiseLevel = vacuumCleaner.getMaxNoiseLevel();
        this.powerCordLength = vacuumCleaner.getPowerCordLength();
        this.height = vacuumCleaner.getHeight();
        this.width = vacuumCleaner.getWidth();
        this.depth = vacuumCleaner.getDepth();
    }

}
