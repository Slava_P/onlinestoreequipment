package com.onlinestoreequipment.dto.product.home_appliances;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class IronFilterDTO {

    private String firmName = "";
    private Integer minCostDTO;
    private Integer maxCostDTO;
    private Integer meanWaterTankVolume;
    private Boolean turboSteamSupply = null;
    private String model;

    public Integer getMinWaterTankVolume(){
        if (meanWaterTankVolume == 250){
            return 0;
        }
        return meanWaterTankVolume - 50;
    }
    public Integer getMaxWaterTankVolume(){
        if (meanWaterTankVolume == 450){
            return 100000;
        }
        return meanWaterTankVolume + 50;
    }
}
