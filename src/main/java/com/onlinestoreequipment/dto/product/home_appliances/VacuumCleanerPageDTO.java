package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VacuumCleanerPageDTO extends VacuumCleanerDTO{

    private String heightToShow;
    private String widthToShow;
    private String depthToShow;
    private String weightToShow;
    private String powerCordLengthShow;

    public VacuumCleanerPageDTO(VacuumCleaner vacuumCleaner){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.setId(vacuumCleaner.getId());
        this.setImgPath(vacuumCleaner.getImgPath());
        this.setModel(vacuumCleaner.getModel());
        this.setColor(vacuumCleaner.getColor());
        this.setAmount(vacuumCleaner.getAmount());
        this.setCost(vacuumCleaner.getCost());
        this.setDescription(vacuumCleaner.getDescription());
        this.setProductType(vacuumCleaner.getProductType());
        this.setProductSubtype(vacuumCleaner.getProductSubtype());
        this.setFirmName(vacuumCleaner.getManufacturer().getFirmName());
        this.setPowerConsumption(vacuumCleaner.getPowerConsumption());
        this.setSuctionPower(vacuumCleaner.getSuctionPower());
        this.setMaxNoiseLevel(vacuumCleaner.getMaxNoiseLevel());
        this.powerCordLengthShow = decimalFormat.format(vacuumCleaner.getPowerCordLength());
        this.heightToShow= decimalFormat.format(vacuumCleaner.getHeight());
        this.widthToShow = decimalFormat.format(vacuumCleaner.getWidth());
        this.depthToShow = decimalFormat.format(vacuumCleaner.getDepth());
        this.weightToShow = decimalFormat.format(vacuumCleaner.getWeight());
    }
}
