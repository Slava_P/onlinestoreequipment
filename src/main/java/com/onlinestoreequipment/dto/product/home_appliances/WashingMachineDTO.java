package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.dto.product.ProductDTO;
import com.onlinestoreequipment.model.constant.MotorType;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WashingMachineDTO extends ProductDTO {

    private Long id;
    private String firmName;
    private Integer powerConsumption;
    private MotorType motorType;
    private Integer maxSpinSpeed;
    private Double waterConsumption;
    private Integer maxLoad;
    private Double height;
    private Double width;
    private Double depth;

    public WashingMachineDTO(WashingMachine washingMachine){
        this.setCreateBy(washingMachine.getCreateBy());
        this.setCreateWhen(washingMachine.getCreateWhen());
        this.setImgPath(washingMachine.getImgPath());
        this.setModel(washingMachine.getModel());
        this.setColor(washingMachine.getColor());
        this.setWeight(washingMachine.getWeight());
        this.setAmount(washingMachine.getAmount());
        this.setCost(washingMachine.getCost());
        this.setDescription(washingMachine.getDescription());
        this.setProductType(washingMachine.getProductType());
        this.setProductSubtype(washingMachine.getProductSubtype());
        this.id = washingMachine.getId();
        this.firmName = washingMachine.getManufacturer().getFirmName();
        this.powerConsumption = washingMachine.getPowerConsumption();
        this.motorType = washingMachine.getMotorType();
        this.maxSpinSpeed = washingMachine.getMaxSpinSpeed();
        this.waterConsumption = washingMachine.getWaterConsumption();
        this.maxLoad = washingMachine.getMaxLoad();
        this.height = washingMachine.getHeight();
        this.width = washingMachine.getWidth();
        this.depth = washingMachine.getDepth();
    }

}
