package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import lombok.*;

import java.text.DecimalFormat;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class WashingMachineShowDTO {

    private Long id;
    private String model;
    private Integer cost;
    private String productSubtype;
    private String imgPath;
    private String manufacturerName;
    private Integer maxLoad;
    private String motorType;
    private String dimensions;

    public WashingMachineShowDTO(WashingMachine washingMachine){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.id = washingMachine.getId();
        this.model = washingMachine.getModel();
        this.cost = washingMachine.getCost();
        this.productSubtype = washingMachine.getProductSubtype().getProductSubtype();
        this.imgPath = washingMachine.getImgPath();
        this.manufacturerName = washingMachine.getManufacturer().getFirmName();
        this.maxLoad = washingMachine.getMaxLoad();
        this.motorType = washingMachine.getMotorType().getType();
        this.dimensions = decimalFormat.format(washingMachine.getHeight()) + "x" + decimalFormat.format(washingMachine.getWidth()) + "x" + decimalFormat.format(washingMachine.getDepth());
    }
}
