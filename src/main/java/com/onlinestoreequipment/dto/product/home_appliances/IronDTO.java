package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.dto.product.ProductDTO;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IronDTO extends ProductDTO {

    private Long id;
    private String firmName;
    private Integer powerConsumption;
    private Integer waterTankVolume;
    private boolean waterSprayer;
    private Double powerCordLength;
    private boolean turboSteamSupply;
    private Double height;
    private Double width;
    private Double depth;

    public IronDTO(Iron iron){
        this.setCreateBy(iron.getCreateBy());
        this.setCreateWhen(iron.getCreateWhen());
        this.setImgPath(iron.getImgPath());
        this.setModel(iron.getModel());
        this.setColor(iron.getColor());
        this.setWeight(iron.getWeight());
        this.setAmount(iron.getAmount());
        this.setCost(iron.getCost());
        this.setDescription(iron.getDescription());
        this.setProductType(iron.getProductType());
        this.setProductSubtype(iron.getProductSubtype());
        this.id = iron.getId();
        this.firmName = iron.getManufacturer().getFirmName();
        this.powerConsumption = iron.getPowerConsumption();
        this.waterTankVolume = iron.getWaterTankVolume();
        this.waterSprayer = iron.isWaterSprayer();
        this.powerCordLength = iron.getPowerCordLength();
        this.turboSteamSupply = iron.isTurboSteamSupply();
        this.height = iron.getHeight();
        this.width = iron.getWidth();
        this.depth = iron.getDepth();
    }
}
