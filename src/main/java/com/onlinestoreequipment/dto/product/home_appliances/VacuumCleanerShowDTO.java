package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class VacuumCleanerShowDTO {
    private Long id;
    private String model;
    private Integer cost;
    private String productSubtype;
    private String imgPath;
    private String manufacturerName;
    private Integer suctionPower;
    private Integer maxNoiseLevel;
    private String powerCordLength;

    public VacuumCleanerShowDTO(VacuumCleaner vacuumCleaner){
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        this.id = vacuumCleaner.getId();
        this.model = vacuumCleaner.getModel();
        this.cost = vacuumCleaner.getCost();
        this.productSubtype = vacuumCleaner.getProductSubtype().getProductSubtype();
        this.imgPath = vacuumCleaner.getImgPath();
        this.manufacturerName = vacuumCleaner.getManufacturer().getFirmName();
        this.suctionPower = vacuumCleaner.getSuctionPower();
        this.maxNoiseLevel = vacuumCleaner.getMaxNoiseLevel();
        this.powerCordLength = decimalFormat.format(vacuumCleaner.getPowerCordLength());
    }
}
