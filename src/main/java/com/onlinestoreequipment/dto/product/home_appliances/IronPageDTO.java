package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DecimalFormat;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IronPageDTO extends IronDTO{

    private String heightToShow;
    private String widthToShow;
    private String depthToShow;
    private String weightToShow;
    private String powerCordLengthShow;
    private String waterSprayerShow;
    private String turboSteamSupplyShow;

    public IronPageDTO(Iron iron){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        this.setId(iron.getId());
        this.setImgPath(iron.getImgPath());
        this.setModel(iron.getModel());
        this.setColor(iron.getColor());
        this.setAmount(iron.getAmount());
        this.setCost(iron.getCost());
        this.setDescription(iron.getDescription());
        this.setProductType(iron.getProductType());
        this.setProductSubtype(iron.getProductSubtype());
        this.setFirmName(iron.getManufacturer().getFirmName());
        this.setPowerConsumption(iron.getPowerConsumption());
        this.setWaterTankVolume(iron.getWaterTankVolume());
        this.powerCordLengthShow = decimalFormat.format(iron.getPowerCordLength());
        this.heightToShow= decimalFormat.format(iron.getHeight());
        this.widthToShow = decimalFormat.format(iron.getWidth());
        this.depthToShow = decimalFormat.format(iron.getDepth());
        this.weightToShow = decimalFormat.format(iron.getWeight());

        if (iron.isTurboSteamSupply()){
            this.turboSteamSupplyShow = "Да";
        } else {
            this.turboSteamSupplyShow = "Нет";
        }

        if (iron.isWaterSprayer()){
            this.waterSprayerShow = "Да";
        } else {
            this.waterSprayerShow = "Нет";
        }
    }
}
