package com.onlinestoreequipment.dto.product.home_appliances;

import com.onlinestoreequipment.model.constant.MotorType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WashingMachineFilterDTO {

    private String firmName = "";
    private Integer minCostDTO;
    private Integer maxCostDTO;
    private Integer meanHeight;
    private Integer meanWidth;
    private Integer meanDepth;
    private MotorType motorType =null;
    private String model;


    public Double getMinHeight(){
        if (meanHeight == 77){
            return 0.0;
        }
        return meanHeight - 3.0;
    }
    public Double getMaxHeight(){
        if (meanHeight == 89){
            return 10000.0;
        }
        return meanHeight + 3.0;
    }
    public Double getMinWidth(){
        if (meanWidth == 51){
            return 0.0;
        }
        return meanWidth - 3.0;
    }
    public Double getMaxWidth(){
        if (meanWidth == 63){
            return 10000.0;
        }
        return meanWidth + 3.0;
    }
    public Double getMinDepth(){
        if (meanDepth == 35){
            return 0.0;
        }
        return meanDepth - 5.0;
    }
    public Double getMaxDepth(){
        if (meanDepth == 65){
            return 10000.0;
        }
        return meanDepth + 5.0;
    }
}
