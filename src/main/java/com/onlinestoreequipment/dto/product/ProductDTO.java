package com.onlinestoreequipment.dto.product;

import com.onlinestoreequipment.dto.GenericDTO;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.constant.ProductType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter

public abstract class ProductDTO extends GenericDTO {

    private String imgPath;

    private String model;

    private String color;

    private Double weight;

    private Integer amount;

    private Integer cost;

    private String description;

    private ProductType productType;

    private ProductSubtype productSubtype;
}
