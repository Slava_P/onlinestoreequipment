package com.onlinestoreequipment.dto.product;

import com.onlinestoreequipment.dto.GenericDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ManufacturerDTO extends GenericDTO {

    private Long id;
    private String firmName;
    private String country;
    private String description;

    public ManufacturerDTO(Manufacturer manufacturer){
        this.id = manufacturer.getId();
        this.firmName = manufacturer.getFirmName();
        this.country = manufacturer.getCountry();
        this.description = manufacturer.getDescription();
    }

}
