package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface IronRepository extends JpaRepository<Iron, Long> {

    Integer countIronByManufacturer(Manufacturer manufacturer);
    List<Iron> findIronByManufacturer(Manufacturer manufacturer);
    List<Iron> findAllByModelLike(String model);
    List<Iron> findAllByManufacturerAndModelLike(Manufacturer manufacturer, String model);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.irons i " +
            "where i.id = :ironId")
    void deleteByIron(Long ironId);

    @Query("select max(cost) from Iron ")
    Integer getMaxCost();
    @Query("select min(cost) from Iron")
    Integer getMinCost();

    @Query("select max(i.cost) from Iron i where " +
            "i.manufacturer = :manufacturer")
    Integer getMaxCostWithManufacturer(Manufacturer manufacturer);

    @Query("select min(i.cost) from Iron i where " +
            "i.manufacturer = :manufacturer")
    Integer getMinCostWithManufacturer(Manufacturer manufacturer);

    @Query("select i from Iron i where i.cost <= :maxCost and i.cost >= :minCost")
    List<Microwave> findIronsWithFilterCost(Integer minCost, Integer maxCost);
}
