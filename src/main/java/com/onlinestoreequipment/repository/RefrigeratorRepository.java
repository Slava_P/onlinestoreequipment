package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RefrigeratorRepository extends JpaRepository<Refrigerator, Long> {

    List<Refrigerator> findRefrigeratorByManufacturer(Manufacturer manufacturer);
    Integer countRefrigeratorByManufacturer(Manufacturer manufacturer);

    List<Refrigerator> findAllByModelLike(String model);
    List<Refrigerator> findAllByManufacturerAndModelLike(Manufacturer manufacturer, String model);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.refrigerators r " +
            "where r.id = :refrigeratorId")
    void deleteByRefrigerator(Long refrigeratorId);


    @Query("select max(cost) from Refrigerator")
    Integer getMaxCost();
    @Query("select min(cost) from Refrigerator")
    Integer getMinCost();

    @Query("select max(r.cost) from Refrigerator r where " +
            "r.manufacturer = :manufacturer")
    Integer getMaxCostWithManufacturer(Manufacturer manufacturer);

    @Query("select min(r.cost) from Refrigerator r where " +
            "r.manufacturer = :manufacturer")
    Integer getMinCostWithManufacturer(Manufacturer manufacturer);

    @Query("select r from Refrigerator r where r.cost <= :maxCost and r.cost >= :minCost")
    List<Refrigerator> findRefrigeratorsWithFilterCost(Integer minCost, Integer maxCost);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.width <= :maxWidth and r.width >= :minWidth and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithFilter(Manufacturer manufacturer, Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                         Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.width <= :maxWidth and r.width >= :minWidth and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterManufacturer(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                        Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.width <= :maxWidth and r.width >= :minWidth and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterManufacturerAndHeight(Integer minCost, Integer maxCost, Double minWidth, Double maxWidth,
                                                                 Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterManufacturerAndWidth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                                Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.width <= :maxWidth and r.width >= :minWidth")
    List<Refrigerator> findAllWithoutFilterManufacturerAndDepth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                                Double minWidth, Double maxWidth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterManufacturerAndHeightAndWidth(Integer minCost, Integer maxCost, Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight")
    List<Refrigerator> findAllWithoutFilterManufacturerAndWidthAndDepth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight);

    @Query("select r from Refrigerator r where " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.width <= :maxWidth and r.width >= :minWidth")
    List<Refrigerator> findAllWithoutFilterManufacturerAndDepthAndHeight(Integer minCost, Integer maxCost,  Double minWidth, Double maxWidth);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.width <= :maxWidth and r.width >= :minWidth and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterHeight(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                  Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterHeightAndWidth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                          Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost")
    List<Refrigerator> findAllWithoutFilterHeightAndWidthAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.width <= :maxWidth and r.width >= :minWidth")
    List<Refrigerator> findAllWithoutFilterHeightAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                          Double minWidth, Double maxWidth);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.depth <= :maxDepth and r.depth >= :minDepth")
    List<Refrigerator> findAllWithoutFilterWidth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                 Double minHeight, Double maxHeight, Double minDepth, Double maxDepth);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight")
    List<Refrigerator> findAllWithoutFilterWidthAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                         Double minHeight, Double maxHeight);

    @Query("select r from Refrigerator r where " +
            "r.manufacturer = :manufacturer and " +
            "r.cost <= :maxCost and r.cost >= :minCost and " +
            "r.height <= :maxHeight and r.height >= :minHeight and " +
            "r.width <= :maxWidth and r.width >= :minWidth")
    List<Refrigerator> findAllWithoutFilterDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                 Double minHeight, Double maxHeight, Double minWidth, Double maxWidth);
}
