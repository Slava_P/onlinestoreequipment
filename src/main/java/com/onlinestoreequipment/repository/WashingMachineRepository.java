package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface WashingMachineRepository extends JpaRepository<WashingMachine, Long> {

    Integer countWashingMachineByManufacturer(Manufacturer manufacturer);
    List<WashingMachine> findWashingMachineByManufacturer(Manufacturer manufacturer);
    List<WashingMachine> findAllByModelLike(String model);
    List<WashingMachine> findAllByManufacturerAndModelLike(Manufacturer manufacturer, String model);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.washing_machines w " +
            "where w.id = :washingMachineId")
    void deleteByWashingMachine(Long washingMachineId);

    @Query("select max(cost) from WashingMachine ")
    Integer getMaxCost();
    @Query("select min(cost) from WashingMachine")
    Integer getMinCost();

    @Query("select max(w.cost) from WashingMachine w where " +
            "w.manufacturer = :manufacturer")
    Integer getMaxCostWithManufacturer(Manufacturer manufacturer);

    @Query("select min(w.cost) from WashingMachine w where " +
            "w.manufacturer = :manufacturer")
    Integer getMinCostWithManufacturer(Manufacturer manufacturer);

    @Query("select m from Microwave m where m.cost <= :maxCost and m.cost >= :minCost")
    List<Microwave> findWashingMachinesWithFilterCost(Integer minCost, Integer maxCost);
}
