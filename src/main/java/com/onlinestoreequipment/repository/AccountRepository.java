package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.store_service.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

   Account findByLogin(String login);

   Account findByEmail(String email);
}
