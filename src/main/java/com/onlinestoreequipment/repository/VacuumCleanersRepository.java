package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface VacuumCleanersRepository extends JpaRepository<VacuumCleaner, Long> {

    Integer countVacuumCleanerByManufacturer(Manufacturer manufacturer);
    List<VacuumCleaner> findVacuumCleanerByManufacturer(Manufacturer manufacturer);
    List<VacuumCleaner> findAllByModelLike(String model);
    List<VacuumCleaner> findAllByManufacturerAndModelLike(Manufacturer manufacturer, String model);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.vacuum_cleaners v " +
            "where v.id = :vacuumCleanerId")
    void deleteByVacuumCleaner(Long vacuumCleanerId);

    @Query("select max(cost) from VacuumCleaner")
    Integer getMaxCost();
    @Query("select min(cost) from VacuumCleaner")
    Integer getMinCost();

    @Query("select max(v.cost) from VacuumCleaner v where " +
            "v.manufacturer = :manufacturer")
    Integer getMaxCostWithManufacturer(Manufacturer manufacturer);

    @Query("select min(v.cost) from VacuumCleaner v where " +
            "v.manufacturer = :manufacturer")
    Integer getMinCostWithManufacturer(Manufacturer manufacturer);

    @Query("select v from VacuumCleaner v where v.cost <= :maxCost and v.cost >= :minCost")
    List<Microwave> findVacuumCleanersWithFilterCost(Integer minCost, Integer maxCost);
}
