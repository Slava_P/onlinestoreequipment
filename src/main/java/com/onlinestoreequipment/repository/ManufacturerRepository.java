package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

    Manufacturer getManufacturerByFirmNameLike(String firmName);

    List<Manufacturer> findAllByFirmNameLike(String firmName);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.manufacturers m " +
            "where m.id = :manufacturerId")
    void deleteByManufacturer(Long manufacturerId);
}
