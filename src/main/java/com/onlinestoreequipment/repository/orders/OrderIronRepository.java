package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderIron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderIronRepository extends JpaRepository<OrderIron, Long> {

    OrderIron findByIronAndOrder(Iron iron, Order order);
    List<OrderIron> findAllByIron(Iron iron);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_irons o " +
            "where o.order_id = :orderId and o.iron_id = :ironId")
    void deleteByIronAndOrder(Long ironId, Long orderId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_irons o " +
            "where o.iron_id = :ironId")
    void deleteByIron(Long ironId);
}
