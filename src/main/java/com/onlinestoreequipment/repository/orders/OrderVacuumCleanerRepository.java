package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderVacuumCleaner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderVacuumCleanerRepository extends JpaRepository<OrderVacuumCleaner, Long> {

    OrderVacuumCleaner findByVacuumCleanerAndOrder(VacuumCleaner vacuumCleaner, Order order);
    List<OrderVacuumCleaner> findAllByVacuumCleaner(VacuumCleaner vacuumCleaner);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_vacuum_cleaners o " +
            "where o.order_id = :orderId and o.vacuum_cleaner_id = :vacuumCleanerId")
    void deleteByVacuumCleanerAndOrder(Long vacuumCleanerId, Long orderId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_vacuum_cleaners o " +
            "where o.vacuum_cleaner_id = :vacuumCleanerId")
    void deleteByVacuumCleaner(Long vacuumCleanerId);
}
