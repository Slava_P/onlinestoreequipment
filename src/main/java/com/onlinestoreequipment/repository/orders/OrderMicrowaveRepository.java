package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderMicrowaves;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderMicrowaveRepository extends JpaRepository<OrderMicrowaves, Long> {

    OrderMicrowaves findByMicrowaveAndOrder(Microwave microwave, Order order);

    List<OrderMicrowaves> findAllByMicrowave(Microwave microwave);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_microwaves o " +
            "where o.order_id = :orderId and o.microwave_id = :microwaveId")
    void deleteByMicrowaveAndOrder(Long microwaveId, Long orderId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_microwaves o " +
            "where o.microwave_id = :microwaveId")
    void deleteByMicrowave(Long microwaveId);
}
