package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.dto.store_service.order.RefrigeratorOrderDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderRefrigeratorRepository extends JpaRepository<OrderRefrigerator, Long> {

    OrderRefrigerator findByRefrigeratorAndOrder(Refrigerator refrigerator, Order order);

    List<OrderRefrigerator> findAllByRefrigerator(Refrigerator refrigerator);


    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_refrigerators o " +
            "where o.order_id = :orderId and o.refrigerator_id = :refrigeratorId")
    void deleteOrderRefrigeratorByRefrigeratorAndOrder(Long refrigeratorId, Long orderId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_refrigerators o " +
            "where o.refrigerator_id = :refrigeratorId")
    void deleteOrderRefrigeratorByRefrigerator(Long refrigeratorId);

}