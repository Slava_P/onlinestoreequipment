package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.model.store_service.orders.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByAccount(Account account);

    @Query(nativeQuery = true,
            value = """ 
                    select * from store_equipment.orders
                    where id = (
                                select max(o.id) from store_equipment.orders o
                                join store_equipment.accounts a on a.id = o.account_id
                                where a.login = :login
                                )
                    """)
    Order findLastByAccountLogin(String login);
}
