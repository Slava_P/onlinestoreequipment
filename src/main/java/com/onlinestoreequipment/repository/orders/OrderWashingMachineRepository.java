package com.onlinestoreequipment.repository.orders;

import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import com.onlinestoreequipment.model.store_service.orders.OrderWashingMachine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderWashingMachineRepository extends JpaRepository<OrderWashingMachine, Long> {

    OrderWashingMachine findByWashingMachineAndOrder(WashingMachine washingMachine, Order order);
    List<OrderWashingMachine> findAllByWashingMachine(WashingMachine washingMachine);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_washing_machines o " +
            "where o.order_id = :orderId and o.washing_machine_id = :washingMachineId")
    void deleteByWashingMachineAndOrder(Long washingMachineId, Long orderId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.orders_washing_machines o " +
            "where o.washing_machine_id = :washingMachineId")
    void deleteByWashingMachine(Long washingMachineId);
}
