package com.onlinestoreequipment.repository;

import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MicrowaveRepository extends JpaRepository<Microwave, Long> {

    Integer countMicrowaveByManufacturer(Manufacturer manufacturer);
    List<Microwave> findMicrowaveByManufacturer(Manufacturer manufacturer);
    List<Microwave> findAllByModelLike(String model);
    List<Microwave> findAllByManufacturerAndModelLike(Manufacturer manufacturer, String model);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from store_equipment.microwaves m " +
            "where m.id = :microwaveId")
    void deleteByMicrowave(Long microwaveId);

    @Query("select max(cost) from Microwave")
    Integer getMaxCost();
    @Query("select min(cost) from Microwave")
    Integer getMinCost();

    @Query("select max(m.cost) from Microwave m where " +
            "m.manufacturer = :manufacturer")
    Integer getMaxCostWithManufacturer(Manufacturer manufacturer);

    @Query("select min(m.cost) from Microwave m where " +
            "m.manufacturer = :manufacturer")
    Integer getMinCostWithManufacturer(Manufacturer manufacturer);

    @Query("select m from Microwave m where m.cost <= :maxCost and m.cost >= :minCost")
    List<Microwave> findMicrowavesWithFilterCost(Integer minCost, Integer maxCost);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.width <= :maxWidth and m.width >= :minWidth and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithFilter(Manufacturer manufacturer, Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                         Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.width <= :maxWidth and m.width >= :minWidth and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterManufacturer(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                        Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.width <= :maxWidth and m.width >= :minWidth and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterManufacturerAndHeight(Integer minCost, Integer maxCost, Double minWidth, Double maxWidth,
                                                                 Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterManufacturerAndWidth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                                Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.width <= :maxWidth and m.width >= :minWidth")
    List<Microwave> findAllWithoutFilterManufacturerAndDepth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight,
                                                                Double minWidth, Double maxWidth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterManufacturerAndHeightAndWidth(Integer minCost, Integer maxCost, Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight")
    List<Microwave> findAllWithoutFilterManufacturerAndWidthAndDepth(Integer minCost, Integer maxCost, Double minHeight, Double maxHeight);

    @Query("select m from Microwave m where " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.width <= :maxWidth and m.width >= :minWidth")
    List<Microwave> findAllWithoutFilterManufacturerAndDepthAndHeight(Integer minCost, Integer maxCost,  Double minWidth, Double maxWidth);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.width <= :maxWidth and m.width >= :minWidth and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterHeight(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                  Double minWidth, Double maxWidth, Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterHeightAndWidth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                          Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost")
    List<Microwave> findAllWithoutFilterHeightAndWidthAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.width <= :maxWidth and m.width >= :minWidth")
    List<Microwave> findAllWithoutFilterHeightAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                          Double minWidth, Double maxWidth);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.depth <= :maxDepth and m.depth >= :minDepth")
    List<Microwave> findAllWithoutFilterWidth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                 Double minHeight, Double maxHeight, Double minDepth, Double maxDepth);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight")
    List<Microwave> findAllWithoutFilterWidthAndDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                         Double minHeight, Double maxHeight);

    @Query("select m from Microwave m where " +
            "m.manufacturer = :manufacturer and " +
            "m.cost <= :maxCost and m.cost >= :minCost and " +
            "m.height <= :maxHeight and m.height >= :minHeight and " +
            "m.width <= :maxWidth and m.width >= :minWidth")
    List<Microwave> findAllWithoutFilterDepth(Manufacturer manufacturer, Integer minCost, Integer maxCost,
                                                 Double minHeight, Double maxHeight, Double minWidth, Double maxWidth);
}
