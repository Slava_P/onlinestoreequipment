package com.onlinestoreequipment.model.product.home_appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.store_service.orders.OrderIron;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "irons", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "irons_seq", allocationSize = 1, schema = "store_equipment")
public class Iron extends HomeAppliances {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_IRONS_MANUFACTURERS"))
    private Manufacturer manufacturer;

    @Column(name = "water_tank_volume", nullable = false)
    private Integer waterTankVolume;

    @Column(name = "water_sprayer")
    private boolean waterSprayer;

    @Column(name = "power_cord_legth")
    private Double powerCordLength;

    @Column(name = "turbo_steam_supply")
    private boolean turboSteamSupply;

    @Column(name = "product_subtype")
    private ProductSubtype productSubtype = ProductSubtype.IRON;

    @OneToMany(mappedBy = "iron")
    @JsonIgnore
    private Set<OrderIron> orderIrons = new HashSet<>();
}
