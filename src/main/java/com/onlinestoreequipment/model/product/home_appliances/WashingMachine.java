package com.onlinestoreequipment.model.product.home_appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.constant.MotorType;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.store_service.orders.OrderWashingMachine;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "washing_machines", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "washing_machines_seq", allocationSize = 1, schema = "store_equipment")
public class WashingMachine extends HomeAppliances {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id",foreignKey = @ForeignKey(name = "FK_WASHING_MACHINES_MANUFACTURERS"))
    private Manufacturer manufacturer;

    @Column(name = "motor_type")
    @Enumerated(EnumType.STRING)
    private MotorType motorType;

    @Column(name = "max_spin_speed", nullable = false)
    private Integer maxSpinSpeed;

    @Column(name = "water_consumption")
    private Double waterConsumption;

    @Column(name = "max_load", nullable = false)
    private Integer maxLoad;

    @Column(name = "product_subtype")
    private ProductSubtype productSubtype = ProductSubtype.WASHING_MACHINE;

    @OneToMany(mappedBy = "washingMachine")
    @JsonIgnore
    private Set<OrderWashingMachine> orderWashingMachine = new HashSet<>();

}
