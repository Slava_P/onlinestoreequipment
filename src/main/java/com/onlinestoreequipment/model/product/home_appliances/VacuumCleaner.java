package com.onlinestoreequipment.model.product.home_appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.store_service.orders.OrderVacuumCleaner;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "vacuum_cleaners", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "vacuum_cleaner_seq", allocationSize = 1, schema = "store_equipment")
public class VacuumCleaner extends HomeAppliances {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_VACUUM_CLEANERS_MANUFACTURERS"))
    private Manufacturer manufacturer;

    @Column(name = "suction_power", nullable = false)
    private Integer suctionPower;

    @Column(name = "max_noise_level")
    private Integer maxNoiseLevel;

    @Column(name = "power_cord_length")
    private Double powerCordLength;

    @Column(name = "product_subtype")
    private ProductSubtype productSubtype = ProductSubtype.VACUUM_CLEANER;

    @OneToMany(mappedBy = "vacuumCleaner")
    @JsonIgnore
    private Set<OrderVacuumCleaner> orderVacuumCleaner = new HashSet<>();
}
