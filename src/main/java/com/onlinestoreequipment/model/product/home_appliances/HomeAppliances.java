package com.onlinestoreequipment.model.product.home_appliances;

import com.onlinestoreequipment.model.constant.ProductType;
import com.onlinestoreequipment.model.product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public abstract class HomeAppliances extends Product {

    @Column(name = "power_consumption", nullable = false)
    private Integer powerConsumption;

    @Column(name = "height")
    private Double height;

    @Column(name = "width")
    private Double width;

    @Column(name = "depth")
    private Double depth;

    @Column(name = "product_type")
    private ProductType productType = ProductType.HOME_APPLIANCES;
}
