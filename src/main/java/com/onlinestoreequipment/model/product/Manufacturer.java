package com.onlinestoreequipment.model.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.onlinestoreequipment.model.GenericModel;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@Table(name = "manufacturers", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "manufacturers_seq", allocationSize = 1, schema = "store_equipment")
public class Manufacturer extends GenericModel {

    @Column(name = "firm_name", nullable = false, unique = true)
    private String firmName;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "description")
    private String description;

//    @OneToMany(mappedBy = "manufacturer")
//    @JsonIgnore
//    private Set<SmartPhone> smartPhones = new HashSet<>();
//
//    @OneToMany(mappedBy = "manufacturer")
//    @JsonIgnore
//    private Set<Tv> tv = new HashSet<>();
//
//    @OneToMany(mappedBy = "manufacturer")
//    @JsonIgnore
//    private Set<Laptop> laptops = new HashSet<>();

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Refrigerator> refrigerators = new HashSet<>();

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<WashingMachine> washingMachines = new HashSet<>();

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private  Set<Microwave> microwaves = new HashSet<>();

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<VacuumCleaner> vacuumCleaners = new HashSet<>();

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Iron> irons =new HashSet<>();

}
