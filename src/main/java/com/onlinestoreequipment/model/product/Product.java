package com.onlinestoreequipment.model.product;

import com.onlinestoreequipment.model.GenericModel;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.constant.ProductType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public abstract class Product extends GenericModel {

    @Column(name = "img_path")
    private String imgPath;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "color", nullable = false)
    private String color;

    @Column(name = "weight", nullable = false)
    private Double weight;

    @Column(name = "amount", nullable = false)
    private Integer amount;

    @Column(name = "cost", nullable = false)
    private Integer cost;

    @Column(name = "description")
    private String description;

    @Column(name = "product_type")
    private ProductType productType;

    @Column(name = "product_subtype")
    private ProductSubtype productSubtype;
}
