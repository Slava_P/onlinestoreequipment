package com.onlinestoreequipment.model.product.kitchen_appliances;

import com.onlinestoreequipment.model.constant.ProductType;
import com.onlinestoreequipment.model.product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public abstract class KitchenAppliances extends Product {

    @Column(name = "power_consumption", nullable = false)
    private Integer powerConsumption;

    @Column(name = "height")
    private Double height;

    @Column(name = "width")
    private Double width;

    @Column(name = "depth")
    private Double depth;

    @Column(name = "product_type")
    private final ProductType productType = ProductType.KITCHEN_APPLIANCES;


}
