package com.onlinestoreequipment.model.product.kitchen_appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.store_service.orders.OrderMicrowaves;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "microwaves", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "microwaves_seq", allocationSize = 1, schema = "store_equipment")
public class Microwave extends KitchenAppliances {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_MICROWAVES_MANUFACTURERS"))
    private Manufacturer manufacturer;

    @Column(name = "microwave_power", nullable = false)
    private Integer microwavePower;

    @Column(name = "volume")
    private Integer volume;

    @Column(name = "product_subtype")
    private ProductSubtype productSubtype = ProductSubtype.MICROWAVE;

    @OneToMany(mappedBy = "microwave")
    @JsonIgnore
    private Set<OrderMicrowaves> orderMicrowaves = new HashSet<>();
}
