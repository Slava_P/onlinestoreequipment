package com.onlinestoreequipment.model.product.kitchen_appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import groovy.lang.ObjectRange;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "refrigerators", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "refrigerators_seq", allocationSize = 1, schema = "store_equipment")
public class Refrigerator extends KitchenAppliances {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_REFRIGERATORS_MANUFACTURERS"))
    private Manufacturer manufacturer;

    @Column(name = "freezing_power")
    private Integer freezingPower;

    @Column(name = "freezing_volume", nullable = false)
    private Integer freezingVolume;

    @Column(name = "refrige_chamber_volume", nullable = false)
    private Integer refrigeChamberVolume;

    @Column(name = "product_subtype")
    private final ProductSubtype productSubtype = ProductSubtype.REFRIGERATOR;

    @OneToMany(mappedBy = "refrigerator")
    @JsonIgnore
    private Set<OrderRefrigerator> orderRefrigerators = new HashSet<>();
}
