package com.onlinestoreequipment.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;


@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public abstract class GenericModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "default_gen")
    private Long id;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "create_when")
    private LocalDateTime createWhen;
}
