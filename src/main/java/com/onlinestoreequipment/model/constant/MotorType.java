package com.onlinestoreequipment.model.constant;

public enum MotorType {
    INVERTER("Инверторный"),
    STANDARD("Стандартный");

    private String type;

    MotorType(String type){
        this.type = type;
    }

    public String getType(){
        return this.type;
    }
}
