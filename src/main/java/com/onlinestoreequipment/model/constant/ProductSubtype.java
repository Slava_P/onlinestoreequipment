package com.onlinestoreequipment.model.constant;

public enum ProductSubtype {
    MICROWAVE("Микроволновая печь"),
    REFRIGERATOR("Холодильник"),
    WASHING_MACHINE("Стиральная машина"),
    VACUUM_CLEANER("Пылесос"),
    IRON("Утюг");

    private final String productSubtype;

    ProductSubtype(String productSubtype){
        this.productSubtype = productSubtype;
    }

    public String getProductSubtype(){
        return this.productSubtype;
    }
}
