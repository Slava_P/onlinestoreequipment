package com.onlinestoreequipment.model.constant;

public enum ProductType {

    HOME_APPLIANCES("Техника для дома"),
    KITCHEN_APPLIANCES("Техника для кухни");

    private final String productType;

    ProductType(String productType){
        this.productType = productType;
    }

    public String getProductType(){
        return this.productType;
    }

}
