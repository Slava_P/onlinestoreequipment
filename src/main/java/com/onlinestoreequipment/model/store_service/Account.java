package com.onlinestoreequipment.model.store_service;

import com.onlinestoreequipment.model.GenericModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "accounts", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "accounts_seq", allocationSize = 1, schema = "store_equipment")
public class Account extends GenericModel {

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role")
    private String role = "USER";

}
