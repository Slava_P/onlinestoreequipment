package com.onlinestoreequipment.model.store_service.orders;

import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "orders_microwaves", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_microwaves_seq", allocationSize = 1, schema = "store_equipment")
public class OrderMicrowaves extends OrderProduct{

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "FK_ORDERS_ORDERMICROWAVES"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "microwave_id", foreignKey = @ForeignKey(name = "FK_REFRIGERATORS_ORDERMICROWAVES"))
    private Microwave microwave;

}
