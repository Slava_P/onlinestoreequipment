package com.onlinestoreequipment.model.store_service.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Setter
@Getter
@MappedSuperclass
public abstract class OrderProduct {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "default_gen")
    private Long id;

    @Column(name = "amount")
    private Integer amount;
}
