package com.onlinestoreequipment.model.store_service.orders;


import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "orders_washing_machines", schema = "store_equipment")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_washing_machines_seq", allocationSize = 1, schema = "store_equipment")
public class OrderWashingMachine extends OrderProduct{

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "FK_ORDERS_ORDERWASHINGMACHINES"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "washing_machine_id", foreignKey = @ForeignKey(name = "FK_WASHINGMACHINES_ORDERWASHINGMACHINES"))
    private WashingMachine washingMachine;

}
