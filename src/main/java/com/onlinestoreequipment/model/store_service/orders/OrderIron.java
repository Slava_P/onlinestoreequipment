package com.onlinestoreequipment.model.store_service.orders;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "orders_irons", schema = "store_equipment")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_irons_seq", allocationSize = 1, schema = "store_equipment")
public class OrderIron extends OrderProduct{

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "FK_ORDERS_ORDERIRONS"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "iron_id", foreignKey = @ForeignKey(name = "FK_IRONS_ORDERIRONS"))
    private Iron iron;
}
