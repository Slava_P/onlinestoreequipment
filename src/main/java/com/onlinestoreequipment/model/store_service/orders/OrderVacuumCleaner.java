package com.onlinestoreequipment.model.store_service.orders;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "orders_vacuum_cleaners", schema = "store_equipment")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_vacuum_cleaners_seq", allocationSize = 1, schema = "store_equipment")
public class OrderVacuumCleaner extends OrderProduct{

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "FK_ORDERS_ORDERVACUUMCLEANERS"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "vacuum_cleaner_id", foreignKey = @ForeignKey(name = "FK_VACUUMCLEANERS_ORDERVACUUMCLEANERS"))
    private VacuumCleaner vacuumCleaner;

}
