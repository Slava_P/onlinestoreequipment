package com.onlinestoreequipment.model.store_service.orders;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onlinestoreequipment.model.GenericModel;
import com.onlinestoreequipment.model.store_service.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "orders", schema = "store_equipment")
@NoArgsConstructor
@Setter
@Getter
@SequenceGenerator(name = "default_gen", sequenceName = "order_seq", allocationSize = 1, schema = "store_equipment")
public class Order extends GenericModel {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", foreignKey = @ForeignKey(name = "FK_ORDERS_CLIENT"))
    private Account account;

    @Column(name = "is_paid", nullable = false)
    private Boolean isPaid = false;

    @Column(name = "cost_order")
    private Integer costOrder;

    @Column(name = "date_create")
    private LocalDate dateCreate;

    @Column(name = "date_paid")
    private LocalDate datePaid;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private Set<OrderRefrigerator> orderRefrigerators = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private Set<OrderMicrowaves> orderMicrowaves = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private Set<OrderWashingMachine> orderWashingMachines = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private Set<OrderVacuumCleaner> orderVacuumCleaners = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private Set<OrderIron> orderIrons =new HashSet<>();
}
