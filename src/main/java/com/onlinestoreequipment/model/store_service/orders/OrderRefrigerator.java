package com.onlinestoreequipment.model.store_service.orders;

import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "orders_refrigerators", schema = "store_equipment")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_refrigerators_seq", allocationSize = 1, schema = "store_equipment")
public class OrderRefrigerator extends OrderProduct{

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "FK_ORDERS_ORDERREFRIGERATORS"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "refrigerator_id", foreignKey = @ForeignKey(name = "FK_REFRIGERATORS_ORDERREFRIGERATORS"))
    private Refrigerator refrigerator;

}
