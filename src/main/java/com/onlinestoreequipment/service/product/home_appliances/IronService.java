package com.onlinestoreequipment.service.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.*;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.repository.IronRepository;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.mapper.IronRowMapper;
import com.onlinestoreequipment.service.store_service.orders.OrderIronService;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class IronService extends GenericService<Iron, IronDTO> {
    private Connection connection;
    private IronRepository ironRepository;
    private ManufacturerRepository manufacturerRepository;
    private OrderIronService orderIronService;
    private static final String UPLOAD_DIRECTORY = "files/irons";
    private NamedParameterJdbcTemplate jdbcTemplate;

    public IronService(IronRepository ironRepository, ManufacturerRepository manufacturerRepository,
                       Connection connection, OrderIronService orderIronService, NamedParameterJdbcTemplate jdbcTemplate){
        this.ironRepository = ironRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.connection = connection;
        this.orderIronService = orderIronService;
        this.jdbcTemplate = jdbcTemplate;
    }
    @Override
    public Iron create(Iron newObject) {
        return ironRepository.save(newObject);
    }

    @Override
    public Iron createFromDTO(IronDTO newObjectDTO) {
        Iron iron = new Iron();
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(newObjectDTO.getFirmName());

        iron.setImgPath(newObjectDTO.getImgPath());
        iron.setModel(newObjectDTO.getModel());
        iron.setColor(newObjectDTO.getColor());
        iron.setWeight(newObjectDTO.getWeight());
        iron.setCost(newObjectDTO.getCost());
        iron.setDescription(newObjectDTO.getDescription());
        iron.setPowerConsumption(newObjectDTO.getPowerConsumption());
        iron.setHeight(newObjectDTO.getHeight());
        iron.setWidth(newObjectDTO.getWidth());
        iron.setDepth(newObjectDTO.getDepth());
        iron.setManufacturer(manufacturer);
        iron.setAmount(newObjectDTO.getAmount());
        iron.setWaterTankVolume(newObjectDTO.getWaterTankVolume());
        iron.setWaterSprayer(newObjectDTO.isWaterSprayer());
        iron.setPowerCordLength(newObjectDTO.getPowerCordLength());
        iron.setTurboSteamSupply(newObjectDTO.isTurboSteamSupply());

        return ironRepository.save(iron);
    }

    public Iron createFromDTO(IronDTO ironDTO, MultipartFile file){
        String fileName = createFile(file);
        ironDTO.setImgPath(fileName);
        return createFromDTO(ironDTO);

    }

    /**
     * Создает файл, и возвращает путь до него.
     *
     * @param file файл
     * @return String путь до созданного файла
     */
    public String createFile(MultipartFile file){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";
        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if(!path.toFile().exists()){
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return resultFileName;
    }

    @Override
    public Iron update(Iron updateObject) {
        return ironRepository.save(updateObject);
    }

    @Override
    public Iron updateFromDTO(Long objectId, IronDTO updateObjectDTO) {
        Iron iron = ironRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Утюга с Id=" + objectId + " не существует в базе"));
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(updateObjectDTO.getFirmName());

        iron.setImgPath(updateObjectDTO.getImgPath());
        iron.setModel(updateObjectDTO.getModel());
        iron.setColor(updateObjectDTO.getColor());
        iron.setWeight(updateObjectDTO.getWeight());
        iron.setCost(updateObjectDTO.getCost());
        iron.setDescription(updateObjectDTO.getDescription());
        iron.setPowerConsumption(updateObjectDTO.getPowerConsumption());
        iron.setHeight(updateObjectDTO.getHeight());
        iron.setWidth(updateObjectDTO.getWidth());
        iron.setDepth(updateObjectDTO.getDepth());
        iron.setManufacturer(manufacturer);
        iron.setAmount(updateObjectDTO.getAmount());
        iron.setWaterTankVolume(updateObjectDTO.getWaterTankVolume());
        iron.setWaterSprayer(updateObjectDTO.isWaterSprayer());
        iron.setPowerCordLength(updateObjectDTO.getPowerCordLength());
        iron.setTurboSteamSupply(updateObjectDTO.isTurboSteamSupply());

        return ironRepository.save(iron);
    }

    public Iron updateFromDTO(Long id, IronDTO ironDTO, MultipartFile file){
        String fileName = createFile(file);
        ironDTO.setImgPath(fileName);
        return updateFromDTO(id, ironDTO);
    }

    /**
     * Удаление товара из магазина и из заказов покупателей.
     *
     * @param objectId id товара
     */
    @Override
    public void delete(Long objectId) {
        Iron iron = getOne(objectId);
        if (orderIronService.getAllWithIron(iron).size() > 0){
            orderIronService.delete(iron);
        }
        ironRepository.deleteByIron(objectId);
    }

    @Override
    public Iron getOne(Long objectId) {
        return ironRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Утюга с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Iron> getAll() {
        return ironRepository.findAll();
    }

    /**
     * Возвращает имеющиеся утюги с набором необходимой информации
     * для показа на странице "viewAllIrons.html".
     *
     * @return List<IronShowDTO> список утюгов с набором необходимой информации
     */
    public List<IronShowDTO> getAllToShow(){
        List<IronShowDTO> ironShowDTOs = new ArrayList<>();
        List<Iron> irons = ironRepository.findAll();
        for (Iron iron: irons){
            ironShowDTOs.add(new IronShowDTO(iron));
        }
        return ironShowDTOs;
    }

    /**
     * Возвращает минимальную стоимость имеющихся утюгов.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(){
        return ironRepository.getMinCost();
    }

    /**
     * Возвращает максимальную стоимость утюгов.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(){
        return ironRepository.getMaxCost();
    }

    /**
     * Возвращает минимальную стоимость имеющихся утюгов заданной фирмы.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(Manufacturer manufacturer){
        return ironRepository.getMinCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает максимальную стоимость имеющихся утюгов заданной фирмы.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(Manufacturer manufacturer){
        return ironRepository.getMaxCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает утюги заданной модели (и заданной компании).
     *
     * @return List<IronShowDTO> отфильтрованных список утюгов
     */
    public List<IronShowDTO> getAllByModel(IronFilterDTO filter){
        String model = "%" + filter.getModel() + "%";
        List<IronShowDTO> ironShowDTOs = new ArrayList<>();
        if (filter.getFirmName().equals("")){
            for (Iron iron: ironRepository.findAllByModelLike(model)){
                ironShowDTOs.add(new IronShowDTO(iron));
            }
        } else {
            Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
            for (Iron iron: ironRepository.findAllByManufacturerAndModelLike(manufacturer, model)){
                ironShowDTOs.add(new IronShowDTO(iron));
            }

        }
        return ironShowDTOs;
    }

    /**
     * Возвращает список отфильтрованных утюгов по:
     * manufacturer, cost, waterTankVolume, turboSteamSupply.
     *
     * @return List<IronShowDTO> список отфильтрованных утюгов
     */
    public List<IronShowDTO> getAllWithFilter(IronFilterDTO filter) throws SQLException {
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
        List<IronShowDTO> ironShowDTOs = new ArrayList<>();
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("maxCost", filter.getMaxCostDTO());
        param.addValue("minCost", filter.getMinCostDTO());

        StringBuilder sqlQuery = new StringBuilder("select * from store_equipment.irons i where " +
                "i.cost <= :maxCost and i.cost >= :minCost");
        if (!filter.getFirmName().equals("")){
            param.addValue("id", manufacturer.getId());
            sqlQuery.append(" and i.manufacturer_id = :id");
        }
        if (filter.getMeanWaterTankVolume() != 0){
            param.addValue("maxWaterTankVolume", filter.getMaxWaterTankVolume());
            param.addValue("minWaterTankVolume", filter.getMinWaterTankVolume());
            sqlQuery.append(" and i.water_tank_volume <= :maxWaterTankVolume and i.water_tank_volume >= :minWaterTankVolume");
        }
        if (filter.getTurboSteamSupply() != null){
            param.addValue("turboSteamSupply", filter.getTurboSteamSupply());
            sqlQuery.append(" and i.turbo_steam_supply = :turboSteamSupply");
        }

        List<Iron> ironsList= jdbcTemplate.query(String.valueOf(sqlQuery), param, new IronRowMapper(manufacturerRepository));

        for (Iron iron: ironsList){
            ironShowDTOs.add(new IronShowDTO(iron));
        }
        return ironShowDTOs;
    }
}
