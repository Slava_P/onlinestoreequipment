package com.onlinestoreequipment.service.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.*;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import com.onlinestoreequipment.repository.VacuumCleanersRepository;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.mapper.VacuumCleanerRowMapper;
import com.onlinestoreequipment.service.mapper.WashingMachineRowMapper;
import com.onlinestoreequipment.service.store_service.orders.OrderVacuumCleanerService;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class VacuumCleanerService extends GenericService<VacuumCleaner, VacuumCleanerDTO> {

    private Connection connection;
    private VacuumCleanersRepository vacuumCleanersRepository;
    private ManufacturerRepository manufacturerRepository;
    private OrderVacuumCleanerService orderVacuumCleanerService;
    private static final String UPLOAD_DIRECTORY = "files/vacuumcleaners";
    private NamedParameterJdbcTemplate jdbcTemplate;

    public VacuumCleanerService(VacuumCleanersRepository vacuumCleanersRepository, ManufacturerRepository manufacturerRepository,
                                Connection connection, OrderVacuumCleanerService orderVacuumCleanerService, NamedParameterJdbcTemplate jdbcTemplate){
        this.vacuumCleanersRepository = vacuumCleanersRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.connection = connection;
        this.orderVacuumCleanerService = orderVacuumCleanerService;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public VacuumCleaner create(VacuumCleaner newObject) {
        return vacuumCleanersRepository.save(newObject);
    }

    @Override
    public VacuumCleaner createFromDTO(VacuumCleanerDTO newObjectDTO) {
        VacuumCleaner vacuumCleaner = new VacuumCleaner();
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(newObjectDTO.getFirmName());

        vacuumCleaner.setImgPath(newObjectDTO.getImgPath());
        vacuumCleaner.setModel(newObjectDTO.getModel());
        vacuumCleaner.setColor(newObjectDTO.getColor());
        vacuumCleaner.setWeight(newObjectDTO.getWeight());
        vacuumCleaner.setCost(newObjectDTO.getCost());
        vacuumCleaner.setDescription(newObjectDTO.getDescription());
        vacuumCleaner.setPowerConsumption(newObjectDTO.getPowerConsumption());
        vacuumCleaner.setHeight(newObjectDTO.getHeight());
        vacuumCleaner.setWidth(newObjectDTO.getWidth());
        vacuumCleaner.setDepth(newObjectDTO.getDepth());
        vacuumCleaner.setManufacturer(manufacturer);
        vacuumCleaner.setAmount(newObjectDTO.getAmount());
        vacuumCleaner.setSuctionPower(newObjectDTO.getSuctionPower());
        vacuumCleaner.setMaxNoiseLevel(newObjectDTO.getMaxNoiseLevel());
        vacuumCleaner.setPowerCordLength(newObjectDTO.getPowerCordLength());

        return vacuumCleanersRepository.save(vacuumCleaner);
    }

    public VacuumCleaner createFromDTO(VacuumCleanerDTO vacuumCleanerDTO, MultipartFile file){
        String fileName = createFile(file);
        vacuumCleanerDTO.setImgPath(fileName);
        return createFromDTO(vacuumCleanerDTO);

    }

    /**
     * Создает файл, и возвращает путь до него.
     *
     * @param file файл
     * @return String путь до созданного файла
     */
    public String createFile(MultipartFile file){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";
        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if(!path.toFile().exists()){
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return resultFileName;
    }

    @Override
    public VacuumCleaner update(VacuumCleaner updateObject) {
        return vacuumCleanersRepository.save(updateObject);
    }

    @Override
    public VacuumCleaner updateFromDTO(Long objectId, VacuumCleanerDTO updateObjectDTO) {
        VacuumCleaner vacuumCleaner = vacuumCleanersRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Пылесоса с Id=" + objectId + " не существует в базе"));
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(updateObjectDTO.getFirmName());

        vacuumCleaner.setImgPath(updateObjectDTO.getImgPath());
        vacuumCleaner.setModel(updateObjectDTO.getModel());
        vacuumCleaner.setColor(updateObjectDTO.getColor());
        vacuumCleaner.setWeight(updateObjectDTO.getWeight());
        vacuumCleaner.setCost(updateObjectDTO.getCost());
        vacuumCleaner.setDescription(updateObjectDTO.getDescription());
        vacuumCleaner.setPowerConsumption(updateObjectDTO.getPowerConsumption());
        vacuumCleaner.setHeight(updateObjectDTO.getHeight());
        vacuumCleaner.setWidth(updateObjectDTO.getWidth());
        vacuumCleaner.setDepth(updateObjectDTO.getDepth());
        vacuumCleaner.setManufacturer(manufacturer);
        vacuumCleaner.setAmount(updateObjectDTO.getAmount());
        vacuumCleaner.setSuctionPower(updateObjectDTO.getSuctionPower());
        vacuumCleaner.setMaxNoiseLevel(updateObjectDTO.getMaxNoiseLevel());
        vacuumCleaner.setPowerCordLength(updateObjectDTO.getPowerCordLength());

        return vacuumCleanersRepository.save(vacuumCleaner);
    }

    public VacuumCleaner updateFromDTO(Long id, VacuumCleanerDTO vacuumCleanerDTO, MultipartFile file){
        String fileName = createFile(file);
        vacuumCleanerDTO.setImgPath(fileName);
        return updateFromDTO(id, vacuumCleanerDTO);
    }

    /**
     * Удаление товара из магазина и из заказов покупателей.
     *
     * @param objectId id товара
     */
    @Override
    public void delete(Long objectId) {
        VacuumCleaner vacuumCleaner = getOne(objectId);
        if (orderVacuumCleanerService.getAllWithVacuumCleaner(vacuumCleaner).size() > 0){
            orderVacuumCleanerService.delete(vacuumCleaner);
        }
        vacuumCleanersRepository.deleteByVacuumCleaner(objectId);

    }

    @Override
    public VacuumCleaner getOne(Long objectId) {
        return vacuumCleanersRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Пылесоса с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<VacuumCleaner> getAll() {
        return vacuumCleanersRepository.findAll();
    }

    /**
     * Возвращает имеющиеся пылесосы с набором необходимой информации
     * для показа на странице "viewAllVacuumCleaners.html".
     *
     * @return List<VacuumCleanerShowDTO> список пылесосов с набором необходимой информации
     */
    public List<VacuumCleanerShowDTO> getAllToShow(){
        List<VacuumCleanerShowDTO> vacuumCleanerShowDTOs = new ArrayList<>();
        List<VacuumCleaner> vacuumCleaners = vacuumCleanersRepository.findAll();
        for (VacuumCleaner vacuumCleaner: vacuumCleaners){
            vacuumCleanerShowDTOs.add(new VacuumCleanerShowDTO(vacuumCleaner));
        }
        return vacuumCleanerShowDTOs;
    }


    /**
     * Возвращает минимальную стоимость имеющихся пылесосов.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(){
        return vacuumCleanersRepository.getMinCost();
    }

    /**
     * Возвращает максимальную стоимость пылесосов.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(){
        return vacuumCleanersRepository.getMaxCost();
    }

    /**
     * Возвращает минимальную стоимость имеющихся пылесосов заданной фирмы.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(Manufacturer manufacturer){
        return vacuumCleanersRepository.getMinCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает максимальную стоимость имеющихся пылесосов заданной фирмы.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(Manufacturer manufacturer){
        return vacuumCleanersRepository.getMaxCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает пылесосы заданной модели (и заданной компании).
     *
     * @return List<VacuumCleanerShowDTO> отфильтрованных список пылесосов
     */
    public List<VacuumCleanerShowDTO> getAllByModel(VacuumCleanerFilterDTO filter){
        String model = "%" + filter.getModel() + "%";
        List<VacuumCleanerShowDTO> vacuumCleanerShowDTOs = new ArrayList<>();
        if (filter.getFirmName().equals("")){
            for (VacuumCleaner vacuumCleaner: vacuumCleanersRepository.findAllByModelLike(model)){
                vacuumCleanerShowDTOs.add(new VacuumCleanerShowDTO(vacuumCleaner));
            }
        } else {
            Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
            for (VacuumCleaner vacuumCleaner: vacuumCleanersRepository.findAllByManufacturerAndModelLike(manufacturer, model)){
                vacuumCleanerShowDTOs.add(new VacuumCleanerShowDTO(vacuumCleaner));
            }

        }
        return vacuumCleanerShowDTOs;
    }

    /**
     * Возвращает список отфильтрованных пылесосов по:
     * manufacturer, cost, suctionPower, maxNoiseLevel.
     *
     * @return List<VacuumCleanerShowDTO> список отфильтрованных пылесосов
     */
    public List<VacuumCleanerShowDTO> getAllWithFilter(VacuumCleanerFilterDTO filter) throws SQLException {
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
        List<VacuumCleanerShowDTO> vacuumCleanerShowDTOs = new ArrayList<>();
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("maxCost", filter.getMaxCostDTO());
        param.addValue("minCost", filter.getMinCostDTO());

        StringBuilder sqlQuery = new StringBuilder("select * from store_equipment.vacuum_cleaners v where " +
                "v.cost <= :maxCost and v.cost >= :minCost");

        if (!filter.getFirmName().equals("")){
            param.addValue("id", manufacturer.getId());
            sqlQuery.append(" and v.manufacturer_id = :id");
        }
        if (filter.getMeanSuctionPower() != 0){
            param.addValue("maxSuctionPower", filter.getMaxSuctionPower());
            param.addValue("minSuctionPower", filter.getMinSuctionPower());
            sqlQuery.append(" and v.suction_power <= :maxSuctionPower and v.suction_power >= :minSuctionPower");
        }
        if (filter.getMeanMaxNoiseLevel() != 0){
            param.addValue("maxMaxNoiseLevel", filter.getMaxMaxNoiseLevel());
            param.addValue("minMaxNoiseLevel", filter.getMinMaxNoiseLevel());
            sqlQuery.append(" and v.max_noise_level <= :maxMaxNoiseLevel and v.max_noise_level >= :minMaxNoiseLevel");
        }

        List<VacuumCleaner> vacuumCleaners = jdbcTemplate.query(String.valueOf(sqlQuery), param, new VacuumCleanerRowMapper(manufacturerRepository));

        for (VacuumCleaner vacuumCleaner: vacuumCleaners){
            vacuumCleanerShowDTOs.add(new VacuumCleanerShowDTO(vacuumCleaner));
        }
        return vacuumCleanerShowDTOs;
    }

}
