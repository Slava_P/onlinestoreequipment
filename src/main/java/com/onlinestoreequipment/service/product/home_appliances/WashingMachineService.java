package com.onlinestoreequipment.service.product.home_appliances;

import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineFilterDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineShowDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import com.onlinestoreequipment.repository.WashingMachineRepository;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.mapper.WashingMachineRowMapper;
import com.onlinestoreequipment.service.store_service.orders.OrderWashingMachineService;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class WashingMachineService extends GenericService<WashingMachine, WashingMachineDTO> {

    private final Connection connection;
    private WashingMachineRepository washingMachineRepository;
    private ManufacturerRepository manufacturerRepository;
    private OrderWashingMachineService orderWashingMachineService;
    private static final String UPLOAD_DIRECTORY = "files/washingmachines";
    private NamedParameterJdbcTemplate jdbcTemplate;

    public WashingMachineService(WashingMachineRepository washingMachineRepository, ManufacturerRepository manufacturerRepository, Connection connection, OrderWashingMachineService orderWashingMachineService, NamedParameterJdbcTemplate jdbcTemplate){
        this.washingMachineRepository = washingMachineRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.connection = connection;
        this.orderWashingMachineService = orderWashingMachineService;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public WashingMachine create(WashingMachine newObject) {
        return washingMachineRepository.save(newObject);
    }

    @Override
    public WashingMachine createFromDTO(WashingMachineDTO newObjectDTO) {
        WashingMachine washingMachine = new WashingMachine();
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(newObjectDTO.getFirmName());

        washingMachine.setImgPath(newObjectDTO.getImgPath());
        washingMachine.setModel(newObjectDTO.getModel());
        washingMachine.setColor(newObjectDTO.getColor());
        washingMachine.setWeight(newObjectDTO.getWeight());
        washingMachine.setCost(newObjectDTO.getCost());
        washingMachine.setDescription(newObjectDTO.getDescription());
        washingMachine.setPowerConsumption(newObjectDTO.getPowerConsumption());
        washingMachine.setHeight(newObjectDTO.getHeight());
        washingMachine.setWidth(newObjectDTO.getWidth());
        washingMachine.setDepth(newObjectDTO.getDepth());
        washingMachine.setManufacturer(manufacturer);
        washingMachine.setAmount(newObjectDTO.getAmount());
        washingMachine.setMotorType(newObjectDTO.getMotorType());
        washingMachine.setMaxSpinSpeed(newObjectDTO.getMaxSpinSpeed());
        washingMachine.setWaterConsumption(newObjectDTO.getWaterConsumption());
        washingMachine.setMaxLoad(newObjectDTO.getMaxLoad());

        return washingMachineRepository.save(washingMachine);
    }

    public WashingMachine createFromDTO(WashingMachineDTO washingMachineDTO, MultipartFile file){
        String fileName = createFile(file);
        washingMachineDTO.setImgPath(fileName);
        return createFromDTO(washingMachineDTO);

    }

    /**
     * Создает файл, и возвращает путь до него.
     *
     * @param file файл
     * @return String путь до созданного файла
     */
    public String createFile(MultipartFile file){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";
        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if(!path.toFile().exists()){
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return resultFileName;
    }

    @Override
    public WashingMachine update(WashingMachine updateObject) {
        return washingMachineRepository.save(updateObject);
    }

    @Override
    public WashingMachine updateFromDTO(Long objectId, WashingMachineDTO updateObjectDTO) {
        WashingMachine washingMachine = washingMachineRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Стиральной машины с Id=" + objectId + " не существует в базе"));
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(updateObjectDTO.getFirmName());

        washingMachine.setImgPath(updateObjectDTO.getImgPath());
        washingMachine.setModel(updateObjectDTO.getModel());
        washingMachine.setColor(updateObjectDTO.getColor());
        washingMachine.setWeight(updateObjectDTO.getWeight());
        washingMachine.setCost(updateObjectDTO.getCost());
        washingMachine.setDescription(updateObjectDTO.getDescription());
        washingMachine.setPowerConsumption(updateObjectDTO.getPowerConsumption());
        washingMachine.setHeight(updateObjectDTO.getHeight());
        washingMachine.setWidth(updateObjectDTO.getWidth());
        washingMachine.setDepth(updateObjectDTO.getDepth());
        washingMachine.setManufacturer(manufacturer);
        washingMachine.setAmount(updateObjectDTO.getAmount());
        washingMachine.setMotorType(updateObjectDTO.getMotorType());
        washingMachine.setMaxSpinSpeed(updateObjectDTO.getMaxSpinSpeed());
        washingMachine.setWaterConsumption(updateObjectDTO.getWaterConsumption());
        washingMachine.setMaxLoad(updateObjectDTO.getMaxLoad());

        return washingMachineRepository.save(washingMachine);
    }

    public WashingMachine updateFromDTO(Long id, WashingMachineDTO washingMachineDTO, MultipartFile file){
        String fileName = createFile(file);
        washingMachineDTO.setImgPath(fileName);
        return updateFromDTO(id, washingMachineDTO);
    }

    /**
     * Удаление товара из магазина и из заказов покупателей.
     *
     * @param objectId id товара
     */
    @Override
    public void delete(Long objectId) {
        WashingMachine washingMachine = getOne(objectId);
        if (orderWashingMachineService.getAllWithWashingMachine(washingMachine).size() > 0){
            orderWashingMachineService.delete(washingMachine);
        }
        washingMachineRepository.deleteByWashingMachine(objectId);

    }


    @Override
    public WashingMachine getOne(Long objectId) {
        return washingMachineRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Стиральной машины с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<WashingMachine> getAll() {
        return washingMachineRepository.findAll();
    }

    /**
     * Возвращает имеющиеся стиральные машины с набором необходимой информации
     * для показа на странице "viewAllWashingMachinesCompany.html".
     *
     * @return List<WashingMachineShowDTO> список стиральных машин с набором необходимой информации
     */
    public List<WashingMachineShowDTO> getAllToShow(){
        List<WashingMachineShowDTO> washingMachineShowDTOs = new ArrayList<>();
        List<WashingMachine> washingMachines = washingMachineRepository.findAll();
        for (WashingMachine washingMachine: washingMachines){
            washingMachineShowDTOs.add(new WashingMachineShowDTO(washingMachine));
        }
        return washingMachineShowDTOs;
    }

    /**
     * Возвращает минимальную стоимость имеющихся стиральных машин.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(){
        return washingMachineRepository.getMinCost();
    }

    /**
     * Возвращает максимальную стоимость стиральных машин.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(){
        return washingMachineRepository.getMaxCost();
    }

    /**
     * Возвращает минимальную стоимость имеющихся стиральных машин заданной фирмы.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(Manufacturer manufacturer){
        return washingMachineRepository.getMinCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает максимальную стоимость имеющихся стиральных машин заданной фирмы.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(Manufacturer manufacturer){
        return washingMachineRepository.getMaxCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает стиральных машин заданной модели (и заданной компании).
     *
     * @return List<WashingMachineShowDTO> отфильтрованных список стиральных машин
     */
    public List<WashingMachineShowDTO> getAllByModel(WashingMachineFilterDTO filter){
        String model = "%" + filter.getModel() + "%";
        List<WashingMachineShowDTO> washingMachineShowDTOs = new ArrayList<>();
        if (filter.getFirmName().equals("")){
            for (WashingMachine washingMachine: washingMachineRepository.findAllByModelLike(model)){
                washingMachineShowDTOs.add(new WashingMachineShowDTO(washingMachine));
            }
        } else {
            Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
            for (WashingMachine washingMachine: washingMachineRepository.findAllByManufacturerAndModelLike(manufacturer, model)){
                washingMachineShowDTOs.add(new WashingMachineShowDTO(washingMachine));
            }

        }
        return washingMachineShowDTOs;
    }

    /**
     * Возвращает список отфильтрованных стиральных машин по:
     * manufacturer, cost, height, width, depth, motorType.
     *
     * @return List<WashingMachineShowDTO> список отфильтрованных стиральных машин
     */
    public List<WashingMachineShowDTO> getAllWithFilter(WashingMachineFilterDTO filter) throws SQLException {
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
        List<WashingMachineShowDTO> washingMachineShowDTOs = new ArrayList<>();
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("maxCost", filter.getMaxCostDTO());
        param.addValue("minCost", filter.getMinCostDTO());
        StringBuilder sqlQuery = new StringBuilder("select * from store_equipment.washing_machines w where " +
                "w.cost <= :maxCost and w.cost >= :minCost");
        if (!filter.getFirmName().equals("")){
            param.addValue("id", manufacturer.getId());
            sqlQuery.append(" and w.manufacturer_id = :id");
        }
        if (filter.getMeanHeight() != 0){
            param.addValue("maxHeight", filter.getMaxHeight());
            param.addValue("minHeight", filter.getMinHeight());
            sqlQuery.append(" and w.height <= :maxHeight and w.height >= :minHeight");
        }
        if (filter.getMeanWidth() != 0){
            param.addValue("maxWidth", filter.getMaxWidth());
            param.addValue("minWidth", filter.getMinWidth());
            sqlQuery.append(" and w.width <= :maxWidth and w.width >= :minWidth");
        }
        if (filter.getMeanDepth() != 0){
            param.addValue("maxDepth", filter.getMaxDepth());
            param.addValue("minDepth", filter.getMinDepth());
            sqlQuery.append(" and w.depth <= :maxDepth and w.depth >= :minDepth");
        }
        if (filter.getMotorType() != null){
            param.addValue("motorType", String.valueOf(filter.getMotorType()));
            sqlQuery.append(" and w.motor_type = :motorType");
        }
        List<WashingMachine> washingMachines = jdbcTemplate.query(String.valueOf(sqlQuery), param, new WashingMachineRowMapper(manufacturerRepository));
        for (WashingMachine washingMachine: washingMachines){
            washingMachineShowDTOs.add(new WashingMachineShowDTO(washingMachine));
        }
        return washingMachineShowDTOs;
    }


}
