package com.onlinestoreequipment.service.product;

import com.onlinestoreequipment.dto.product.ManufacturerDTO;
import com.onlinestoreequipment.dto.product.home_appliances.IronShowDTO;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerShowDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineShowDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveShowDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorFilterDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorShowDTO;
import com.onlinestoreequipment.model.constant.ProductSubtype;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.repository.*;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.product.home_appliances.IronService;
import com.onlinestoreequipment.service.product.home_appliances.VacuumCleanerService;
import com.onlinestoreequipment.service.product.home_appliances.WashingMachineService;
import com.onlinestoreequipment.service.product.kitchen_appliances.MicrowaveService;
import com.onlinestoreequipment.service.product.kitchen_appliances.RefrigeratorService;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManufacturerService extends GenericService<Manufacturer, ManufacturerDTO> {

    private ManufacturerRepository manufacturerRepository;
    private RefrigeratorRepository refrigeratorRepository;
    private MicrowaveRepository microwaveRepository;
    private WashingMachineRepository washingMachineRepository;
    private VacuumCleanersRepository vacuumCleanersRepository;
    private IronRepository ironRepository;
    private RefrigeratorService refrigeratorService;
    private MicrowaveService microwaveService;
    private WashingMachineService washingMachineService;
    private VacuumCleanerService vacuumCleanerService;
    private IronService ironService;

    public ManufacturerService(ManufacturerRepository manufacturerRepository, RefrigeratorRepository refrigeratorRepository,
                               MicrowaveRepository microwaveRepository, WashingMachineRepository washingMachineRepository,
                               VacuumCleanersRepository vacuumCleanersRepository, IronRepository ironRepository, RefrigeratorService refrigeratorService, MicrowaveService microwaveService, WashingMachineService washingMachineService, VacuumCleanerService vacuumCleanerService, IronService ironService){
        this.manufacturerRepository = manufacturerRepository;
        this.refrigeratorRepository = refrigeratorRepository;
        this.microwaveRepository = microwaveRepository;
        this.washingMachineRepository = washingMachineRepository;
        this.vacuumCleanersRepository = vacuumCleanersRepository;
        this.ironRepository = ironRepository;
        this.refrigeratorService = refrigeratorService;
        this.microwaveService = microwaveService;
        this.washingMachineService = washingMachineService;
        this.vacuumCleanerService = vacuumCleanerService;
        this.ironService = ironService;
    }

    @Override
    public Manufacturer create(Manufacturer newObject) {
        return manufacturerRepository.save(newObject);
    }

    @Override
    public Manufacturer createFromDTO(ManufacturerDTO newObjectDTO) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setFirmName(newObjectDTO.getFirmName());
        manufacturer.setCountry(newObjectDTO.getCountry());
        manufacturer.setDescription(newObjectDTO.getDescription());

        return manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer update(Manufacturer updateObject) {
        return manufacturerRepository.save(updateObject);
    }

    @Override
    public Manufacturer updateFromDTO(Long objectId, ManufacturerDTO updateObjectDTO) {
        Manufacturer manufacturer = manufacturerRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + objectId + " не существует в базе"));
        manufacturer.setFirmName(updateObjectDTO.getFirmName());
        manufacturer.setCountry(updateObjectDTO.getCountry());
        manufacturer.setDescription(updateObjectDTO.getDescription());

        return manufacturerRepository.save(manufacturer);
    }

    /**
     * Удаление производителя вместе с его товарами.
     *
     * @param objectId id производителя
     */
    @Override
    public void delete(Long objectId) {
        Manufacturer manufacturer = manufacturerRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + objectId + " не существует в базе"));;
        if (refrigeratorRepository.countRefrigeratorByManufacturer(manufacturer) > 0){
            for (Refrigerator refrigerator: refrigeratorRepository.findRefrigeratorByManufacturer(manufacturer)){
                refrigeratorService.delete(refrigerator.getId());
            }
        }
        if (microwaveRepository.countMicrowaveByManufacturer(manufacturer) > 0){
            for (Microwave microwave: microwaveRepository.findMicrowaveByManufacturer(manufacturer)){
                microwaveService.delete(microwave.getId());
            }
        }
        if (washingMachineRepository.countWashingMachineByManufacturer(manufacturer) > 0){
            for (WashingMachine washingMachine: washingMachineRepository.findWashingMachineByManufacturer(manufacturer)){
                washingMachineService.delete(washingMachine.getId());
            }
        }
        if (vacuumCleanersRepository.countVacuumCleanerByManufacturer(manufacturer) > 0){
            for (VacuumCleaner vacuumCleaner: vacuumCleanersRepository.findVacuumCleanerByManufacturer(manufacturer)){
                vacuumCleanerService.delete(vacuumCleaner.getId());
            }
        }
        if (ironRepository.countIronByManufacturer(manufacturer) > 0){
            for (Iron iron: ironRepository.findIronByManufacturer(manufacturer)){
                ironService.delete(iron.getId());
            }
        }
        manufacturerRepository.deleteByManufacturer(manufacturer.getId());
    }

    @Override
    public Manufacturer getOne(Long objectId) {
        return manufacturerRepository.findById(objectId).orElseThrow(()-> new NotFoundException("Производителя с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Manufacturer> getAll() {
        return manufacturerRepository.findAll();
    }

    /**
     * Возвращает компании по заданному имени.
     *
     * @return List<RefrigeratorShowDTO> отфильтрованных список холодильников
     */
    public List<Manufacturer> getAllByFirmName(ManufacturerDTO filter){
        String firmName = "%" + filter.getFirmName() + "%";
        return manufacturerRepository.findAllByFirmNameLike(firmName);
    }

    /**
     * Получение всех холодильников данной фирмы.
     *
     * @param manufacturerId id производителя
     * @return List<RefrigeratorShowDTO> список холодильников
     */
    public List<RefrigeratorShowDTO> getAllRefrigerators(Long manufacturerId){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        List<RefrigeratorShowDTO> refrigeratorDTOs = new ArrayList<>();
        for (Refrigerator refrigerator: refrigeratorRepository.findRefrigeratorByManufacturer(manufacturer)){
            refrigeratorDTOs.add(new RefrigeratorShowDTO(refrigerator));
        }
        return refrigeratorDTOs;
    }

    /**
     * Получение всех микроволновых печей данной фирмы.
     *
     * @param manufacturerId id производителя
     * @return List<MicrowaveShowDTO> список микроволновых печей
     */
    public List<MicrowaveShowDTO> getAllMicrowaves(Long manufacturerId){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        List<MicrowaveShowDTO> microwaveShowDTOs = new ArrayList<>();
        for (Microwave microwave: microwaveRepository.findMicrowaveByManufacturer(manufacturer)){
            microwaveShowDTOs.add(new MicrowaveShowDTO(microwave));
        }
        return microwaveShowDTOs;
    }

    /**
     * Получение всех стиральных машин данной фирмы.
     *
     * @param manufacturerId id производителя
     * @return List<WashingMachineShowDTO> список стиральных машин
     */
    public List<WashingMachineShowDTO> getAllWashingMachines(Long manufacturerId){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        List<WashingMachineShowDTO> washingMachineShowDTOs = new ArrayList<>();
        for (WashingMachine washingMachine: washingMachineRepository.findWashingMachineByManufacturer(manufacturer)){
            washingMachineShowDTOs.add(new WashingMachineShowDTO(washingMachine));
        }
        return washingMachineShowDTOs;
    }

    /**
     * Получение всех пылесосов данной фирмы.
     *
     * @param manufacturerId id производителя
     * @return List<VacuumCleanerShowDTO> список пылесосов машин
     */
    public List<VacuumCleanerShowDTO> getAllVacuumCleaner(Long manufacturerId){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        List<VacuumCleanerShowDTO> vacuumCleanerShowDTOs = new ArrayList<>();
        for (VacuumCleaner vacuumCleaner: vacuumCleanersRepository.findVacuumCleanerByManufacturer(manufacturer)){
            vacuumCleanerShowDTOs.add(new VacuumCleanerShowDTO(vacuumCleaner));
        }
        return vacuumCleanerShowDTOs;
    }

    /**
     * Получение всех утюгов данной фирмы.
     *
     * @param manufacturerId id производителя
     * @return List<IronShowDTO> список утюгов машин
     */
    public List<IronShowDTO> getAllIrons(Long manufacturerId){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        List<IronShowDTO> ironShowDTOs = new ArrayList<>();
        for (Iron iron: ironRepository.findIronByManufacturer(manufacturer)){
            ironShowDTOs.add(new IronShowDTO(iron));
        }
        return ironShowDTOs;
    }

    /**
     * Получение значения количества моделей продукта
     * определенного типа данной фирмы.
     *
     * @param manufacturerId,subtype id производителя, тип продукта
     * @return Integer значения количества моделей
     */
    public Integer getCountProduct(Long manufacturerId, ProductSubtype subtype){
        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
        Integer countProduct = 0;
        switch (subtype){
            case REFRIGERATOR:
                countProduct = refrigeratorRepository.countRefrigeratorByManufacturer(manufacturer);
                break;
            case MICROWAVE:
                countProduct = microwaveRepository.countMicrowaveByManufacturer(manufacturer);
                break;
            case WASHING_MACHINE:
                countProduct = washingMachineRepository.countWashingMachineByManufacturer(manufacturer);
                break;
            case VACUUM_CLEANER:
                countProduct = vacuumCleanersRepository.countVacuumCleanerByManufacturer(manufacturer);
                break;
            case IRON:
                countProduct = ironRepository.countIronByManufacturer(manufacturer);
                break;
        }
        return countProduct;
    }

//    /**
//     * Создание нового телевизора при связывании телевизора и производителя
//     *
//     * @param newTv новый объект телевизора
//     * @return
//     */
//    public Manufacturer createTv(Tv newTv){
//
//        return
//    }

//    /**
//     * Добавление связи между производителем и телевизором в базе
//     *
//     * @param manufacturerId,tvId Id производителя и телевизора
//     * @return Manufacturer
//     */
//    public Manufacturer addTv(Long manufacturerId, Long tvId){
//        Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
//                .orElseThrow(()-> new NotFoundException("Производителя с Id=" + manufacturerId + " не существует в базе"));
//        Tv tv = tvRepository.findById(tvId)
//                .orElseThrow(()-> new NotFoundException("Телевизора с Id=" + tvId + " не существует в базе"));
//        tv.setManufacturer(manufacturer);
//        manufacturer.getTv().add(tv);
//        return manufacturerRepository.save(manufacturer);
//    }

}
