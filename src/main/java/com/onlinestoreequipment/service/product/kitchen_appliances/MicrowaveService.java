package com.onlinestoreequipment.service.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.kitchen_appliances.*;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import com.onlinestoreequipment.repository.MicrowaveRepository;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.store_service.orders.OrderMicrowaveService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class MicrowaveService extends GenericService<Microwave, MicrowaveDTO> {

    private MicrowaveRepository microwaveRepository;
    private ManufacturerRepository manufacturerRepository;
    private OrderMicrowaveService orderMicrowaveService;
    private static final String UPLOAD_DIRECTORY = "files/microwaves";

    public MicrowaveService(MicrowaveRepository microwaveRepository, ManufacturerRepository manufacturerRepository, OrderMicrowaveService orderMicrowaveService){
        this.microwaveRepository = microwaveRepository;
        this.manufacturerRepository= manufacturerRepository;
        this.orderMicrowaveService = orderMicrowaveService;
    }

    @Override
    public Microwave create(Microwave newObject) {
        return microwaveRepository.save(newObject);
    }

    @Override
    public Microwave createFromDTO(MicrowaveDTO newObjectDTO) {
        Microwave microwave = new Microwave();
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(newObjectDTO.getFirmName());

        microwave.setImgPath(newObjectDTO.getImgPath());
        microwave.setModel(newObjectDTO.getModel());
        microwave.setColor(newObjectDTO.getColor());
        microwave.setWeight(newObjectDTO.getWeight());
        microwave.setCost(newObjectDTO.getCost());
        microwave.setDescription(newObjectDTO.getDescription());
        microwave.setPowerConsumption(newObjectDTO.getPowerConsumption());
        microwave.setHeight(newObjectDTO.getHeight());
        microwave.setWidth(newObjectDTO.getWidth());
        microwave.setDepth(newObjectDTO.getDepth());
        microwave.setManufacturer(manufacturer);
        microwave.setAmount(newObjectDTO.getAmount());
        microwave.setVolume(newObjectDTO.getVolume());
        microwave.setMicrowavePower(newObjectDTO.getMicrowavePower());

        return microwaveRepository.save(microwave);
    }

    public Microwave createFromDTO(MicrowaveDTO microwaveDTO, MultipartFile file){
        String fileName = createFile(file);
        microwaveDTO.setImgPath(fileName);
        return createFromDTO(microwaveDTO);

    }

    /**
     * Создает файл, и возвращает путь до него.
     *
     * @param file файл
     * @return String путь до созданного файла
     */
    public String createFile(MultipartFile file){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";
        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if(!path.toFile().exists()){
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return resultFileName;
    }

    @Override
    public Microwave update(Microwave updateObject) {
        return microwaveRepository.save(updateObject);
    }

    @Override
    public Microwave updateFromDTO(Long objectId, MicrowaveDTO updateObjectDTO) {
        Microwave microwave = microwaveRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Микроволновой печи с Id=" + objectId + " не существует в базе"));
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(updateObjectDTO.getFirmName());

        microwave.setImgPath(updateObjectDTO.getImgPath());
        microwave.setModel(updateObjectDTO.getModel());
        microwave.setColor(updateObjectDTO.getColor());
        microwave.setWeight(updateObjectDTO.getWeight());
        microwave.setCost(updateObjectDTO.getCost());
        microwave.setDescription(updateObjectDTO.getDescription());
        microwave.setPowerConsumption(updateObjectDTO.getPowerConsumption());
        microwave.setHeight(updateObjectDTO.getHeight());
        microwave.setWidth(updateObjectDTO.getWidth());
        microwave.setDepth(updateObjectDTO.getDepth());
        microwave.setManufacturer(manufacturer);
        microwave.setAmount(updateObjectDTO.getAmount());
        microwave.setVolume(updateObjectDTO.getVolume());
        microwave.setMicrowavePower(updateObjectDTO.getMicrowavePower());

        return microwaveRepository.save(microwave);
    }

    public Microwave updateFromDTO(Long id, MicrowaveDTO microwaveDTO, MultipartFile file){
        String fileName = createFile(file);
        microwaveDTO.setImgPath(fileName);
        return updateFromDTO(id, microwaveDTO);
    }

    /**
     * Удаление товара из магазина и из заказов покупателей.
     *
     * @param objectId id товара
     */
    @Override
    public void delete(Long objectId) {
        Microwave microwave = getOne(objectId);
        if (orderMicrowaveService.getAllWithMicrowave(microwave).size() > 0){
            orderMicrowaveService.delete(microwave);
        }
        microwaveRepository.deleteByMicrowave(objectId);

    }


    @Override
    public Microwave getOne(Long objectId) {
        return microwaveRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Микроволновой печи с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Microwave> getAll() {
        return microwaveRepository.findAll();
    }

    /**
     * Возвращает имеющиеся микроволновые печи с набором необходимой информации
     * для показа на странице "viewAllRefrigerators.html".
     *
     * @return List<MicrowaveShowDTO> список микроволновых печей с набором необходимой информации
     */
    public List<MicrowaveShowDTO> getAllToShow(){
        List<MicrowaveShowDTO> microwaveShowDTOs = new ArrayList<>();
        List<Microwave> microwaves = microwaveRepository.findAll();
        for (Microwave microwave: microwaves){
            microwaveShowDTOs.add(new MicrowaveShowDTO(microwave));
        }
        return microwaveShowDTOs;
    }

    /**
     * Возвращает минимальную стоимость имеющихся микроволновых печей.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(){
        return microwaveRepository.getMinCost();
    }

    /**
     * Возвращает максимальную стоимость микроволновых печей.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(){
        return microwaveRepository.getMaxCost();
    }

    /**
     * Возвращает минимальную стоимость имеющихся микроволновых печей заданной фирмы.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(Manufacturer manufacturer){
        return microwaveRepository.getMinCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает максимальную стоимость имеющихся микроволновых печей заданной фирмы.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(Manufacturer manufacturer){
        return microwaveRepository.getMaxCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает микроволновые печи заданной модели (и заданной компании).
     *
     * @return List<MicrowaveShowDTO> отфильтрованных список микроволновых печей
     */
    public List<MicrowaveShowDTO> getAllByModel(MicrowaveFilterDTO filter){
        String model = "%" + filter.getModel() + "%";
        List<MicrowaveShowDTO> microwaveShowDTOs = new ArrayList<>();
        if (filter.getFirmName().equals("")){
            for (Microwave microwave: microwaveRepository.findAllByModelLike(model)){
                microwaveShowDTOs.add(new MicrowaveShowDTO(microwave));
            }
        } else {
            Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
            for (Microwave microwave: microwaveRepository.findAllByManufacturerAndModelLike(manufacturer, model)){
                microwaveShowDTOs.add(new MicrowaveShowDTO(microwave));
            }

        }
        return microwaveShowDTOs;
    }

    /**
     * Возвращает список отфильтрованных микроволновых печей по:
     * manufacturer, cost, height, width, depth.
     *
     * @return List<MicrowaveShowDTO> список отфильтрованных микроволновых печей
     */
    public List<MicrowaveShowDTO> getAllWithFilter(MicrowaveFilterDTO filter){
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
        List<MicrowaveShowDTO> microwaveShowDTOs = new ArrayList<>();
        List<Microwave> microwaves = new ArrayList<>();
        if (filter.getFirmName().equals("") && filter.getMeanHeight() == 0 && filter.getMeanWidth() == 0 && filter.getMeanDepth() == 0){
            microwaves = microwaveRepository.findMicrowavesWithFilterCost(filter.getMinCostDTO(), filter.getMaxCostDTO());
        } else if (filter.getFirmName().equals("")){
            if (filter.getMeanHeight() == 0){
                if(filter.getMeanWidth() == 0){
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndHeightAndWidth(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinDepth(), filter.getMaxDepth());
                } else {
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndHeight(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinWidth(),
                            filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanWidth() == 0){
                if (filter.getMeanDepth() == 0){
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndWidthAndDepth(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinHeight(), filter.getMaxHeight());
                } else {
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndWidth(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                            filter.getMaxHeight(), filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanDepth() == 0){
                if (filter.getMeanHeight() == 0){
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndDepthAndHeight(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinWidth(), filter.getMaxWidth());
                }else {
                    microwaves = microwaveRepository.findAllWithoutFilterManufacturerAndDepth(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                            filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth());
                }
            } else {
                microwaves = microwaveRepository.findAllWithoutFilterManufacturer(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                        filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
            }
        } else if (filter.getMeanHeight() == 0) {
            if (filter.getMeanWidth() == 0){
                if (filter.getMeanDepth() == 0){
                    microwaves = microwaveRepository.findAllWithoutFilterHeightAndWidthAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO());
                }else {
                    microwaves = microwaveRepository.findAllWithoutFilterHeightAndWidth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanDepth() == 0){
                microwaves = microwaveRepository.findAllWithoutFilterHeightAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinWidth(), filter.getMaxWidth());
            } else {
                microwaves = microwaveRepository.findAllWithoutFilterHeight(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinWidth() ,filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
            }
        }else if(filter.getMeanWidth() == 0){
            if(filter.getMeanDepth() == 0){
                microwaves = microwaveRepository.findAllWithoutFilterWidthAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinHeight(), filter.getMaxHeight());
            }else {
                microwaves = microwaveRepository.findAllWithoutFilterWidth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinHeight(), filter.getMaxHeight(), filter.getMinDepth(), filter.getMaxDepth());
            }
        } else if (filter.getMeanDepth() == 0) {
            microwaves = microwaveRepository.findAllWithoutFilterDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                    filter.getMinHeight(), filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth());
        }else {
            microwaves = microwaveRepository.findAllWithFilter(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                    filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
        }
        for (Microwave microwave: microwaves){
            microwaveShowDTOs.add(new MicrowaveShowDTO(microwave));
        }
        return microwaveShowDTOs;
    }
}
