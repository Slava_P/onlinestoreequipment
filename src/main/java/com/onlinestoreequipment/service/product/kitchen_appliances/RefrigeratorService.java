package com.onlinestoreequipment.service.product.kitchen_appliances;

import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorFilterDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorShowDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import com.onlinestoreequipment.repository.RefrigeratorRepository;
import com.onlinestoreequipment.service.GenericService;
import com.onlinestoreequipment.service.store_service.orders.OrderRefrigeratorService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class RefrigeratorService extends GenericService<Refrigerator, RefrigeratorDTO> {

    private RefrigeratorRepository refrigeratorRepository;
    private ManufacturerRepository manufacturerRepository;
    private OrderRefrigeratorService orderRefrigeratorService;
    private static final String UPLOAD_DIRECTORY = "files/refrigerators";

    public RefrigeratorService(RefrigeratorRepository refrigeratorRepository, ManufacturerRepository manufacturerRepository, OrderRefrigeratorService orderRefrigeratorService){
        this.refrigeratorRepository = refrigeratorRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.orderRefrigeratorService = orderRefrigeratorService;
    }

    @Override
    public Refrigerator create(Refrigerator newObject) {
        return refrigeratorRepository.save(newObject);
    }

    @Override
    public Refrigerator createFromDTO(RefrigeratorDTO newObjectDTO) {
        Refrigerator refrigerator = new Refrigerator();
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(newObjectDTO.getFirmName());

        refrigerator.setImgPath(newObjectDTO.getImgPath());
        refrigerator.setModel(newObjectDTO.getModel());
        refrigerator.setColor(newObjectDTO.getColor());
        refrigerator.setWeight(newObjectDTO.getWeight());
        refrigerator.setCost(newObjectDTO.getCost());
        refrigerator.setDescription(newObjectDTO.getDescription());
        refrigerator.setPowerConsumption(newObjectDTO.getPowerConsumption());
        refrigerator.setHeight(newObjectDTO.getHeight());
        refrigerator.setWidth(newObjectDTO.getWidth());
        refrigerator.setDepth(newObjectDTO.getDepth());
        refrigerator.setManufacturer(manufacturer);
        refrigerator.setFreezingPower(newObjectDTO.getFreezingPower());
        refrigerator.setAmount(newObjectDTO.getAmount());
        refrigerator.setFreezingVolume(newObjectDTO.getFreezingVolume());
        refrigerator.setRefrigeChamberVolume(newObjectDTO.getRefrigeChamberVolume());

        return refrigeratorRepository.save(refrigerator);
    }

    public Refrigerator createFromDTO(RefrigeratorDTO refrigeratorDTO, MultipartFile file){
        String fileName = createFile(file);
        refrigeratorDTO.setImgPath(fileName);
        return createFromDTO(refrigeratorDTO);

    }

    /**
     * Создает файл, и возвращает путь до него.
     *
     * @param file файл
     * @return String путь до созданного файла
     */
    public String createFile(MultipartFile file){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";
        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if(!path.toFile().exists()){
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return resultFileName;
    }

    @Override
    public Refrigerator update(Refrigerator updateObject) {
        return refrigeratorRepository.save(updateObject);
    }

    @Override
    public Refrigerator updateFromDTO(Long objectId, RefrigeratorDTO updateObjectDTO) {
        Refrigerator refrigerator = refrigeratorRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Холодильника с Id=" + objectId + " не существует в базе"));
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(updateObjectDTO.getFirmName());

        refrigerator.setImgPath(updateObjectDTO.getImgPath());
        refrigerator.setModel(updateObjectDTO.getModel());
        refrigerator.setColor(updateObjectDTO.getColor());
        refrigerator.setWeight(updateObjectDTO.getWeight());
        refrigerator.setCost(updateObjectDTO.getCost());
        refrigerator.setDescription(updateObjectDTO.getDescription());
        refrigerator.setPowerConsumption(updateObjectDTO.getPowerConsumption());
        refrigerator.setHeight(updateObjectDTO.getHeight());
        refrigerator.setWidth(updateObjectDTO.getWidth());
        refrigerator.setDepth(updateObjectDTO.getDepth());
        refrigerator.setManufacturer(manufacturer);
        refrigerator.setFreezingPower(updateObjectDTO.getFreezingPower());
        refrigerator.setAmount(updateObjectDTO.getAmount());
        refrigerator.setFreezingVolume(updateObjectDTO.getFreezingVolume());
        refrigerator.setRefrigeChamberVolume(updateObjectDTO.getRefrigeChamberVolume());

        return refrigeratorRepository.save(refrigerator);
    }

    public Refrigerator updateFromDTO(Long id, RefrigeratorDTO refrigeratorDTO, MultipartFile file){
        String fileName = createFile(file);
        refrigeratorDTO.setImgPath(fileName);
        return updateFromDTO(id, refrigeratorDTO);
    }

    /**
     * Удаление товара из магазина и из заказов покупателей.
     *
     * @param objectId id товара
     */
    @Override
    public void delete(Long objectId) {
        Refrigerator refrigerator = getOne(objectId);
        if (orderRefrigeratorService.getAllWithRefrigerator(refrigerator).size() > 0){
            orderRefrigeratorService.delete(refrigerator);
        }
        refrigeratorRepository.deleteByRefrigerator(objectId);

    }

    @Override
    public Refrigerator getOne(Long objectId) {
        return refrigeratorRepository.findById(objectId)
                .orElseThrow(()-> new NotFoundException("Холодильника с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Refrigerator> getAll() {
        return refrigeratorRepository.findAll();
    }

    /**
     * Возвращает имеющиеся холодильники с набором необходимой информации
     * для показа на странице "viewAllMicrowavesCompany.html".
     *
     * @return List<RefrigeratorShowDTO> список холодильников с набором необходимой информации
     */
    public List<RefrigeratorShowDTO> getAllToShow(){
        List<RefrigeratorShowDTO> refrigeratorShowDTOs = new ArrayList<>();
        List<Refrigerator> refrigerators = refrigeratorRepository.findAll();
        for (Refrigerator refrigerator: refrigerators){
            refrigeratorShowDTOs.add(new RefrigeratorShowDTO(refrigerator));
        }
        return refrigeratorShowDTOs;
    }

    /**
     * Возвращает минимальную стоимость имеющихся холодильников.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(){
        return refrigeratorRepository.getMinCost();
    }

    /**
     * Возвращает максимальную стоимость имеющихся холодильников.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(){
        return refrigeratorRepository.getMaxCost();
    }

    /**
     * Возвращает минимальную стоимость имеющихся холодильников заданной фирмы.
     *
     * @return Integer минимальная стоимость
     */
    public Integer getMinCost(Manufacturer manufacturer){
        return refrigeratorRepository.getMinCostWithManufacturer(manufacturer);
    }

    /**
     * Возвращает максимальную стоимость имеющихся холодильников заданной фирмы.
     *
     * @return Integer максимальная стоимость
     */
    public Integer getMaxCost(Manufacturer manufacturer){
        return refrigeratorRepository.getMaxCostWithManufacturer(manufacturer);
    }


    /**
     * Возвращает холодильники заданной модели (и заданной компании).
     *
     * @return List<RefrigeratorShowDTO> отфильтрованных список холодильников
     */
    public List<RefrigeratorShowDTO> getAllByModel(RefrigeratorFilterDTO filter){
        String model = "%" + filter.getModel() + "%";
        List<RefrigeratorShowDTO> refrigeratorShowDTOs = new ArrayList<>();
        if (filter.getFirmName().equals("")){
            for (Refrigerator refrigerator: refrigeratorRepository.findAllByModelLike(model)){
                refrigeratorShowDTOs.add(new RefrigeratorShowDTO(refrigerator));
            }
        } else {
            Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
            for (Refrigerator refrigerator: refrigeratorRepository.findAllByManufacturerAndModelLike(manufacturer, model)){
                refrigeratorShowDTOs.add(new RefrigeratorShowDTO(refrigerator));
            }

        }
        return refrigeratorShowDTOs;
    }

    /**
     * Возвращает список отфильтрованных холодильников по:
     * manufacturer, cost, height, width, depth.
     *
     * @return List<RefrigeratorShowDTO> список отфильтрованных холодильников
     */
    public List<RefrigeratorShowDTO> getAllWithFilter(RefrigeratorFilterDTO filter){
        Manufacturer manufacturer = manufacturerRepository.getManufacturerByFirmNameLike(filter.getFirmName());
        List<RefrigeratorShowDTO> refrigeratorShowDTOs = new ArrayList<>();
        List<Refrigerator> refrigerators = new ArrayList<>();
        if (filter.getFirmName().equals("") && filter.getMeanHeight() == 0 && filter.getMeanWidth() == 0 && filter.getMeanDepth() == 0){
            refrigerators = refrigeratorRepository.findRefrigeratorsWithFilterCost(filter.getMinCostDTO(), filter.getMaxCostDTO());
        } else if (filter.getFirmName().equals("")){
            if (filter.getMeanHeight() == 0){
                if(filter.getMeanWidth() == 0){
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndHeightAndWidth(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinDepth(), filter.getMaxDepth());
                } else {
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndHeight(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinWidth(),
                            filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanWidth() == 0){
                if (filter.getMeanDepth() == 0){
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndWidthAndDepth(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinHeight(), filter.getMaxHeight());
                } else {
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndWidth(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                            filter.getMaxHeight(), filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanDepth() == 0){
                if (filter.getMeanHeight() == 0){
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndDepthAndHeight(filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinWidth(), filter.getMaxWidth());
                }else {
                    refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturerAndDepth(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                            filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth());
                }
            } else {
                refrigerators = refrigeratorRepository.findAllWithoutFilterManufacturer(filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                        filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
            }
        } else if (filter.getMeanHeight() == 0) {
            if (filter.getMeanWidth() == 0){
                if (filter.getMeanDepth() == 0){
                    refrigerators = refrigeratorRepository.findAllWithoutFilterHeightAndWidthAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO());
                }else {
                    refrigerators = refrigeratorRepository.findAllWithoutFilterHeightAndWidth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                            filter.getMinDepth(), filter.getMaxDepth());
                }
            } else if(filter.getMeanDepth() == 0){
                refrigerators = refrigeratorRepository.findAllWithoutFilterHeightAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinWidth(), filter.getMaxWidth());
            } else {
              refrigerators = refrigeratorRepository.findAllWithoutFilterHeight(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                      filter.getMinWidth() ,filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
            }
        }else if(filter.getMeanWidth() == 0){
            if(filter.getMeanDepth() == 0){
                refrigerators = refrigeratorRepository.findAllWithoutFilterWidthAndDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinHeight(), filter.getMaxHeight());
            }else {
                refrigerators = refrigeratorRepository.findAllWithoutFilterWidth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                        filter.getMinHeight(), filter.getMaxHeight(), filter.getMinDepth(), filter.getMaxDepth());
            }
        } else if (filter.getMeanDepth() == 0) {
            refrigerators = refrigeratorRepository.findAllWithoutFilterDepth(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(),
                    filter.getMinHeight(), filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth());
        }else {
            refrigerators = refrigeratorRepository.findAllWithFilter(manufacturer, filter.getMinCostDTO(), filter.getMaxCostDTO(), filter.getMinHeight(),
                    filter.getMaxHeight(), filter.getMinWidth(), filter.getMaxWidth(), filter.getMinDepth(), filter.getMaxDepth());
        }
        for (Refrigerator refrigerator: refrigerators){
            refrigeratorShowDTOs.add(new RefrigeratorShowDTO(refrigerator));
        }
        return refrigeratorShowDTOs;
    }
}
