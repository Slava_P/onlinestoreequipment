package com.onlinestoreequipment.service;

import java.util.List;

public abstract class GenericService <T, N> {

    public abstract T create(T newObject);

    public  abstract T createFromDTO(N newObjectDTO);

    public abstract T update(T updateObject);

    public  abstract T updateFromDTO(Long objectId, N updateObjectDTO);

    public abstract void delete(Long objectId);

    public abstract T getOne(Long objectId);

    public abstract List<T> getAll();

}
