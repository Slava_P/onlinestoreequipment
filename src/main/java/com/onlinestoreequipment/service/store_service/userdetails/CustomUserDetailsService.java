package com.onlinestoreequipment.service.store_service.userdetails;

import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Value("${spring.security.user.name}")
    private String adminUsername;
    @Value("${spring.security.user.password}")
    private String adminPassword;
    @Value("${spring.security.user.roles}")
    private String adminRole;
    private AccountRepository accountRepository;

    public CustomUserDetailsService(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.equals(adminUsername)){
            return org.springframework.security.core.userdetails.User
                    .builder()
                    .username(adminUsername)
                    .password(adminPassword)
                    .roles(adminRole)
                    .build();
        } else {
            Account account = accountRepository.findByLogin(username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            return new CustomUserDetails(account.getId().intValue(), username, account.getPassword(), authorities);
        }
    }
}
