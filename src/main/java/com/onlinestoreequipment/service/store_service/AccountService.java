package com.onlinestoreequipment.service.store_service;

import com.onlinestoreequipment.dto.store_service.AccountDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.repository.AccountRepository;
import com.onlinestoreequipment.service.GenericService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;
import java.util.List;

@Service
public class AccountService extends GenericService<Account, AccountDTO> {

    private AccountRepository accountRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private JavaMailSender javaMailSender;
    private static final String CHANGE_PASSWORD_URL = "http://localhost:8090/api/account/change-password/";

    public AccountService(AccountRepository accountRepository, BCryptPasswordEncoder bCryptPasswordEncoder, JavaMailSender javaMailSender){
        this.accountRepository = accountRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public Account create(Account newObject) {
        newObject.setPassword(bCryptPasswordEncoder.encode(newObject.getPassword()));
        return accountRepository.save(newObject);
    }

    @Override
    public Account createFromDTO(AccountDTO newObjectDTO) {
        Account account = new Account();

        account.setFirstName(newObjectDTO.getFirstName());
        account.setLastName(newObjectDTO.getLastName());
        account.setMiddleName(newObjectDTO.getMiddleName());
        account.setTelephone(newObjectDTO.getTelephone());
        account.setBirthDate(LocalDate.parse(newObjectDTO.getBirthDate()));
        account.setEmail(newObjectDTO.getEmail());
        account.setAddress(newObjectDTO.getAddress());
        account.setLogin(newObjectDTO.getLogin());
        account.setPassword(bCryptPasswordEncoder.encode(newObjectDTO.getPassword()));
        return accountRepository.save(account);
    }

    @Override
    public Account update(Account updateObject) {
        return accountRepository.save(updateObject);
    }

    @Override
    public Account updateFromDTO(Long objectId, AccountDTO updateObjectDTO) {
        Account account = accountRepository.findById(objectId).orElseThrow(() -> new NotFoundException("Пользователя с Id=" + objectId + " не существует в базе"));

        account.setFirstName(updateObjectDTO.getFirstName());
        account.setLastName(updateObjectDTO.getLastName());
        account.setMiddleName(updateObjectDTO.getMiddleName());
        account.setTelephone(updateObjectDTO.getTelephone());
        account.setBirthDate(LocalDate.parse(updateObjectDTO.getBirthDate()));
        account.setEmail(updateObjectDTO.getEmail());
        account.setAddress(updateObjectDTO.getAddress());
        //account.setLogin(updateObjectDTO.getLogin());
        return accountRepository.save(account);

    }

    @Override
    public void delete(Long objectId) {
        accountRepository.delete(accountRepository.findById(objectId).orElseThrow(() -> new NotFoundException("Пользователя с Id=" + objectId + " не существует в базе")));
    }

    @Override
    public Account getOne(Long objectId) {
        return accountRepository.findById(objectId).orElseThrow(() -> new NotFoundException("Пользователя с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    public Account getAccountByLogin(String login){
        return accountRepository.findByLogin(login);
    }

    /**
     * Нахождение существующего аккаунта по Email.
     *
     * @param email email пользователя
     * @return AccountDTO аккаунт
     */
    public AccountDTO getAccountByEmail(String email){
        return new AccountDTO(accountRepository.findByEmail(email));
    }

    /**
     * Отправка письма с восстановлением пароля.
     *
     * @param email,id email и id пользователя
     */
    public void sendChangePasswordEmail(String email, Long userId){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        // Содержание письма
        mailMessage.setSubject("Восстановление пароля в онлайн магазине \"TeHStore\"");
        mailMessage.setText("Добрый день! Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля. " +
                "Для восстановления пароля, перейдите по ссылке: '" + CHANGE_PASSWORD_URL + userId + "'");
        javaMailSender.send(mailMessage);
    }

    /**
     * Смена пароля у существующего пользователя.
     *
     * @param id,password id и новый пароль пользователя
     */
    public void changePassword(Long id, String password){
        Account account = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("Пользователя с Id=" + id + " не существует в базе"));
        account.setPassword(bCryptPasswordEncoder.encode(password));
        accountRepository.save(account);


    }

}
