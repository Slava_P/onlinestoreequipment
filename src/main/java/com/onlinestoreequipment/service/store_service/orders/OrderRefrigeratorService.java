package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import com.onlinestoreequipment.repository.RefrigeratorRepository;
import com.onlinestoreequipment.repository.orders.OrderRefrigeratorRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import com.onlinestoreequipment.service.product.kitchen_appliances.RefrigeratorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OrderRefrigeratorService {

    private OrderRefrigeratorRepository orderRefrigeratorRepository;
    private RefrigeratorRepository refrigeratorRepository;
    private OrderRepository orderRepository;

    public OrderRefrigeratorService(OrderRefrigeratorRepository orderRefrigeratorRepository, RefrigeratorRepository refrigeratorRepository, OrderRepository orderRepository){
        this.orderRefrigeratorRepository = orderRefrigeratorRepository;
        this.refrigeratorRepository = refrigeratorRepository;
        this.orderRepository = orderRepository;
    }

    public OrderRefrigerator create(Order order, Refrigerator refrigerator){
        OrderRefrigerator orderRefrigerator = new OrderRefrigerator();
        orderRefrigerator.setOrder(order);
        orderRefrigerator.setRefrigerator(refrigerator);
        orderRefrigerator.setAmount(1);

        return orderRefrigeratorRepository.save(orderRefrigerator);
    }

    public void delete(ProductOrderDTO product, Long orderId){
        orderRefrigeratorRepository.deleteOrderRefrigeratorByRefrigeratorAndOrder(product.getId(), orderId);
    }

    public void delete(Refrigerator refrigerator){
        orderRefrigeratorRepository.deleteOrderRefrigeratorByRefrigerator(refrigerator.getId());
    }

    public List<OrderRefrigerator> getAllWithRefrigerator(Refrigerator refrigerator){
        return orderRefrigeratorRepository.findAllByRefrigerator(refrigerator);
    }

    /**
     * Увеличивает количество товара (amount) в таблице "orders_refrigerators" .
     *
     * @param product,order товар и заказ
     * @return OrderRefrigerator строку из таблицы "orders_refrigerators" с увеличенным "amount"
     */
    public OrderRefrigerator increaseAmount(ProductOrderDTO product, Order order){

        OrderRefrigerator orderRefrigerator = orderRefrigeratorRepository.findByRefrigeratorAndOrder(
                refrigeratorRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Холодильника с Id=" + product.getId() + " не существует в базе")),
                order);

        orderRefrigerator.setAmount(product.getAmountInOrder() + 1);
        return orderRefrigeratorRepository.save(orderRefrigerator);
    }

    /**
     * Уменьшает количество товара (amount) в таблице "orders_refrigerators" .
     *
     * @param product,order товар и заказ
     * @return OrderRefrigerator строку из таблицы "orders_refrigerators" с уменьшенным "amount"
     */
    public OrderRefrigerator reduceAmount(ProductOrderDTO product, Order order){
        OrderRefrigerator orderRefrigerator = orderRefrigeratorRepository.findByRefrigeratorAndOrder(
                refrigeratorRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Холодильника с Id=" + product.getId() + " не существует в базе")),
                order);

        orderRefrigerator.setAmount(product.getAmountInOrder() - 1);
        return orderRefrigeratorRepository.save(orderRefrigerator);
    }

}
