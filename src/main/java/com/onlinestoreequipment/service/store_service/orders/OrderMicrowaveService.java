package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderMicrowaves;
import com.onlinestoreequipment.model.store_service.orders.OrderRefrigerator;
import com.onlinestoreequipment.repository.MicrowaveRepository;
import com.onlinestoreequipment.repository.orders.OrderMicrowaveRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import com.onlinestoreequipment.service.product.kitchen_appliances.MicrowaveService;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OrderMicrowaveService {

    private OrderMicrowaveRepository orderMicrowaveRepository;
    private OrderRepository orderRepository;
    private MicrowaveRepository microwaveRepository;

    public OrderMicrowaveService(OrderMicrowaveRepository orderMicrowaveRepository, OrderRepository orderRepository, MicrowaveRepository microwaveRepository){

        this.orderMicrowaveRepository = orderMicrowaveRepository;
        this.orderRepository = orderRepository;
        this.microwaveRepository = microwaveRepository;
    }

    public OrderMicrowaves create(Order order, Microwave microwave){
        OrderMicrowaves orderMicrowaves = new OrderMicrowaves();
        orderMicrowaves.setOrder(order);
        orderMicrowaves.setMicrowave(microwave);
        orderMicrowaves.setAmount(1);

        return orderMicrowaveRepository.save(orderMicrowaves);
    }

    public void delete(ProductOrderDTO product, Long orderId){
        orderMicrowaveRepository.deleteByMicrowaveAndOrder(product.getId(), orderId);
    }

    public void delete(Microwave microwave){
        orderMicrowaveRepository.deleteByMicrowave(microwave.getId());
    }

    public List<OrderMicrowaves> getAllWithMicrowave(Microwave microwave){
        return orderMicrowaveRepository.findAllByMicrowave(microwave);
    }

    /**
     * Увеличивает количество товара (amount) в таблице "orders_microwaves" .
     *
     * @param product,order товар и заказ
     * @return OrderMicrowaves строку из таблицы "orders_microwaves" с увеличенным "amount"
     */
    public OrderMicrowaves increaseAmount(ProductOrderDTO product, Order order){

        OrderMicrowaves orderMicrowaves = orderMicrowaveRepository.findByMicrowaveAndOrder(
                microwaveRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Микроволновой печи с Id=" + product.getId() + " не существует в базе")),
                order);

        orderMicrowaves.setAmount(product.getAmountInOrder() + 1);
        return orderMicrowaveRepository.save(orderMicrowaves);
    }


    /**
     * Уменьшает количество товара (amount) в таблице "orders_microwaves" .
     *
     * @param product,order товар и заказ
     * @return OrderMicrowaves строку из таблицы "orders_microwaves" с уменьшенным "amount"
     */
    public OrderMicrowaves reduceAmount(ProductOrderDTO product, Order order){
        OrderMicrowaves orderMicrowaves = orderMicrowaveRepository.findByMicrowaveAndOrder(
                microwaveRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Микроволновой печи с Id=" + product.getId() + " не существует в базе")),
                order);

        orderMicrowaves.setAmount(product.getAmountInOrder() - 1);
        return orderMicrowaveRepository.save(orderMicrowaves);
    }

}
