package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderVacuumCleaner;
import com.onlinestoreequipment.repository.VacuumCleanersRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import com.onlinestoreequipment.repository.orders.OrderVacuumCleanerRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OrderVacuumCleanerService {

    private OrderVacuumCleanerRepository orderVacuumCleanerRepository;
    private OrderRepository orderRepository;
    private VacuumCleanersRepository vacuumCleanersRepository;

    public OrderVacuumCleanerService(OrderVacuumCleanerRepository orderVacuumCleanerRepository, OrderRepository orderRepository, VacuumCleanersRepository vacuumCleanersRepository) {
        this.orderVacuumCleanerRepository = orderVacuumCleanerRepository;
        this.orderRepository = orderRepository;
        this.vacuumCleanersRepository = vacuumCleanersRepository;
    }

    public OrderVacuumCleaner create(Order order, VacuumCleaner vacuumCleaner){
        OrderVacuumCleaner orderVacuumCleaner = new OrderVacuumCleaner();
        orderVacuumCleaner.setOrder(order);
        orderVacuumCleaner.setVacuumCleaner(vacuumCleaner);
        orderVacuumCleaner.setAmount(1);

        return orderVacuumCleanerRepository.save(orderVacuumCleaner);
    }

    public void delete(ProductOrderDTO product, Long orderId){
        orderVacuumCleanerRepository.deleteByVacuumCleanerAndOrder(product.getId(), orderId);
    }

    public void delete(VacuumCleaner vacuumCleaner){
        orderVacuumCleanerRepository.deleteByVacuumCleaner(vacuumCleaner.getId());
    }

    public List<OrderVacuumCleaner> getAllWithVacuumCleaner(VacuumCleaner vacuumCleaner){
        return orderVacuumCleanerRepository.findAllByVacuumCleaner(vacuumCleaner);
    }


    /**
     * Увеличивает количество товара (amount) в таблице "orders_vacuum_cleaners" .
     *
     * @param product,order товар и заказ
     * @return OrderVacuumCleaner строку из таблицы "orders_vacuum_cleaners" с увеличенным "amount"
     */
    public OrderVacuumCleaner increaseAmount(ProductOrderDTO product, Order order){

        OrderVacuumCleaner orderVacuumCleaner = orderVacuumCleanerRepository.findByVacuumCleanerAndOrder(
                vacuumCleanersRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Пылесоса с Id=" + product.getId() + " не существует в базе")),
                order);

        orderVacuumCleaner.setAmount(product.getAmountInOrder() + 1);
        return orderVacuumCleanerRepository.save(orderVacuumCleaner);
    }

    /**
     * Уменьшает количество товара (amount) в таблице "orders_vacuum_cleaners" .
     *
     * @param product,order товар и заказ
     * @return OrderVacuumCleaner строку из таблицы "orders_vacuum_cleaners" с уменьшенным "amount"
     */
    public OrderVacuumCleaner reduceAmount(ProductOrderDTO product, Order order){
        OrderVacuumCleaner orderVacuumCleaner = orderVacuumCleanerRepository.findByVacuumCleanerAndOrder(
                vacuumCleanersRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Пылесоса с Id=" + product.getId() + " не существует в базе")),
                order);

        orderVacuumCleaner.setAmount(product.getAmountInOrder() - 1);
        return orderVacuumCleanerRepository.save(orderVacuumCleaner);
    }
}
