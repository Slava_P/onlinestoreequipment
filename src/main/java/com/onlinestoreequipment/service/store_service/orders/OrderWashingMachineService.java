package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderWashingMachine;
import com.onlinestoreequipment.repository.WashingMachineRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import com.onlinestoreequipment.repository.orders.OrderWashingMachineRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OrderWashingMachineService {

    private OrderWashingMachineRepository orderWashingMachineRepository;
    private OrderRepository orderRepository;
    private WashingMachineRepository washingMachineRepository;

    public OrderWashingMachineService(OrderWashingMachineRepository orderWashingMachineRepository, OrderRepository orderRepository, WashingMachineRepository washingMachineRepository) {
        this.orderWashingMachineRepository = orderWashingMachineRepository;
        this.orderRepository = orderRepository;
        this.washingMachineRepository = washingMachineRepository;
    }

    public OrderWashingMachine create(Order order, WashingMachine washingMachine){
        OrderWashingMachine orderWashingMachine = new OrderWashingMachine();
        orderWashingMachine.setOrder(order);
        orderWashingMachine.setWashingMachine(washingMachine);
        orderWashingMachine.setAmount(1);

        return orderWashingMachineRepository.save(orderWashingMachine);
    }

    public void delete(ProductOrderDTO product, Long orderId){
        orderWashingMachineRepository.deleteByWashingMachineAndOrder(product.getId(), orderId);
    }

    public void delete(WashingMachine washingMachine){
        orderWashingMachineRepository.deleteByWashingMachine(washingMachine.getId());
    }

    public List<OrderWashingMachine> getAllWithWashingMachine(WashingMachine washingMachine){
        return orderWashingMachineRepository.findAllByWashingMachine(washingMachine);
    }

    /**
     * Увеличивает количество товара (amount) в таблице "orders_washing_machines" .
     *
     * @param product,order товар и заказ
     * @return OrderWashingMachine строку из таблицы "orders_washing_machines" с увеличенным "amount"
     */
    public OrderWashingMachine increaseAmount(ProductOrderDTO product, Order order){

        OrderWashingMachine orderWashingMachine = orderWashingMachineRepository.findByWashingMachineAndOrder(
                washingMachineRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Стиральной машины с Id=" + product.getId() + " не существует в базе")),
                order);

        orderWashingMachine.setAmount(product.getAmountInOrder() + 1);
        return orderWashingMachineRepository.save(orderWashingMachine);
    }

    /**
     * Уменьшают количество товара (amount) в таблице "orders_washing_machines" .
     *
     * @param product,order товар и заказ
     * @return OrderWashingMachine строку из таблицы "orders_washing_machines" с уменьшенным "amount"
     */
    public OrderWashingMachine reduceAmount(ProductOrderDTO product, Order order){
        OrderWashingMachine orderWashingMachine = orderWashingMachineRepository.findByWashingMachineAndOrder(
                washingMachineRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Стиральной машины с Id=" + product.getId() + " не существует в базе")),
                order);

        orderWashingMachine.setAmount(product.getAmountInOrder() - 1);
        return orderWashingMachineRepository.save(orderWashingMachine);
    }
}
