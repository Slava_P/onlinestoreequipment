package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.OrderDTO;
import com.onlinestoreequipment.dto.store_service.order.OrderShowDTO;
import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.store_service.Account;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.repository.AccountRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import com.onlinestoreequipment.service.GenericService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {

    private OrderRepository orderRepository;
    private AccountRepository accountRepository;
    private OrderRefrigeratorService orderRefrigeratorService;
    private OrderMicrowaveService orderMicrowaveService;
    private OrderWashingMachineService orderWashingMachineService;
    private OrderVacuumCleanerService orderVacuumCleanerService;
    private OrderIronService orderIronService;
    private JavaMailSender javaMailSender;
    @Value("${spring.mail.admin}")
    private String adminEmail;

    public OrderService(OrderRepository orderRepository, AccountRepository accountRepository, OrderRefrigeratorService orderRefrigeratorService, OrderMicrowaveService orderMicrowaveService, OrderWashingMachineService orderWashingMachineService, OrderVacuumCleanerService orderVacuumCleanerService, OrderIronService orderIronService, JavaMailSender javaMailSender){
        this.orderRepository = orderRepository;
        this.accountRepository = accountRepository;
        this.orderRefrigeratorService = orderRefrigeratorService;
        this.orderMicrowaveService = orderMicrowaveService;
        this.orderWashingMachineService = orderWashingMachineService;
        this.orderVacuumCleanerService = orderVacuumCleanerService;
        this.orderIronService = orderIronService;
        this.javaMailSender = javaMailSender;
    }


    @Override
    public Order create(Order newObject) {
        return orderRepository.save(newObject);
    }

    @Override
    public Order createFromDTO(OrderDTO newObjectDTO) {
        Order order = new Order();

        order.setIsPaid(newObjectDTO.getIsPaid());
        order.setCostOrder(newObjectDTO.getCostOrder());
        order.setDateCreate(newObjectDTO.getDateCreate());
        order.setDatePaid(newObjectDTO.getDatePaid());

        return orderRepository.save(order);
    }

    @Override
    public Order update(Order updateObject) {
        return orderRepository.save(updateObject);
    }

    @Override
    public Order updateFromDTO(Long objectId, OrderDTO updateObjectDTO) {
        Order order = orderRepository.findById(objectId).orElseThrow(()-> new NotFoundException("Заказа с Id=" + objectId + " не существует в базе"));
        order.setIsPaid(updateObjectDTO.getIsPaid());
        order.setCostOrder(updateObjectDTO.getCostOrder());
        order.setDateCreate(updateObjectDTO.getDateCreate());
        order.setDatePaid(updateObjectDTO.getDatePaid());
        return orderRepository.save(order);
    }

    @Override
    public void delete(Long objectId) {
        orderRepository.delete(orderRepository.findById(objectId).orElseThrow(()-> new NotFoundException("Заказа с Id=" + objectId + " не существует в базе")));
    }

    /**
     * Удаление определенного товара из корзины покупателя.
     *
     * @param product,orderId товар, Id заказа
     */
    public void deleteProductFromOrder(ProductOrderDTO product, Long orderId){
        switch (product.getProductSubtype()){
            case "Холодильник":
                orderRefrigeratorService.delete(product, orderId);
                break;
            case "Микроволновая печь":
                orderMicrowaveService.delete(product, orderId);
                break;
            case "Стиральная машина":
                orderWashingMachineService.delete(product, orderId);
                break;
            case "Пылесос":
                orderVacuumCleanerService.delete(product, orderId);
                break;
            case "Утюг":
                orderIronService.delete(product, orderId);
                break;
        }
    }

    @Override
    public Order getOne(Long objectId) {
        return orderRepository.findById(objectId).orElseThrow(()-> new NotFoundException("Заказа с Id=" + objectId + " не существует в базе"));
    }

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public List<OrderShowDTO> getAllToShowDTO(){
        List<OrderShowDTO> orderShowDTOs = new ArrayList<>();
        for (Order order: orderRepository.findAll()){
            orderShowDTOs.add(new OrderShowDTO(order));
        }
        return orderShowDTOs;
    }
    /**
     * Возвращает имеющиеся заказы клиента с набором необходимой информации
     * для показа на странице "viewAllOrders.html".
     *
     * @param login логин пользователя
     * @return List<OrderShowDTO> список заказов клиента с набором необходимой информации
     */
    public List<OrderShowDTO> getAllToShowDTOInAccount(String login){
        Account account = accountRepository.findByLogin(login);
        List<OrderShowDTO> orderShowDTOs = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByAccount(account);
        for (Order order: orders){
            orderShowDTOs.add(new OrderShowDTO(order));
        }
        return orderShowDTOs;
    }

    /**
     * Возвращает имеющиеся заказы клиента с набором необходимой информации.
     *
     * @param login логин пользователя
     * @return List<OrderDTO> список заказов клиента с набором необходимой информации
     */
    public List<OrderDTO> getAllToDTO(String login){
        Account account = accountRepository.findByLogin(login);
        List<OrderDTO> orderDTOS = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByAccount(account);
        for (Order order: orders){
            orderDTOS.add(new OrderDTO(order));
        }
        return orderDTOS;
    }

    /**
     * Поиск последнего заказа определенного пользователя для показа.
     *
     * @param login логин пользователя
     * @return OrderShowDTO последний заказ пользователя
     */
    public OrderDTO getLastOrderAccountToShow(String login){
        return new OrderDTO(orderRepository.findLastByAccountLogin(login));
    }

    /**
     * Поиск последнего заказа определенного пользователя.
     *
     * @param login логин пользователя
     * @return Order последний заказ пользователя
     */
    public Order getLastOrderAccount(String login){
        return orderRepository.findLastByAccountLogin(login);
    }

    /**
     * Увеличение количества товара в корзине.
     *
     * @param product,orderId ДТО товара и ID заказа
     * @return Order обновленный заказ пользователя
     */
    public Order increaseAmountProduct(ProductOrderDTO product, Long orderId){
        Order order = getOne(orderId);
        switch (product.getProductSubtype()){
            case "Холодильник":
                orderRefrigeratorService.increaseAmount(product, order);
                break;
            case "Микроволновая печь":
                orderMicrowaveService.increaseAmount(product, order);
                break;
            case "Стиральная машина":
                orderWashingMachineService.increaseAmount(product, order);
                break;
            case "Пылесос":
                orderVacuumCleanerService.increaseAmount(product, order);
                break;
            case "Утюг":
                orderIronService.increaseAmount(product, order);
                break;
        }
        return updateFromDTO(orderId, new OrderDTO(order));
    }

    /**
     * Уменьшение количества товара в корзине.
     *
     * @param product,orderId ДТО товара и ID заказа
     * @return Order обновленный заказ пользователя
     */
    public Order reduceAmountProduct(ProductOrderDTO product, Long orderId){
        Order order = getOne(orderId);
        switch (product.getProductSubtype()){
            case "Холодильник":
                orderRefrigeratorService.reduceAmount(product, order);
                break;
            case "Микроволновая печь":
                orderMicrowaveService.reduceAmount(product, order);
                break;
            case "Стиральная машина":
                orderWashingMachineService.reduceAmount(product, order);
                break;
            case "Пылесос":
                orderVacuumCleanerService.reduceAmount(product, order);
                break;
            case "Утюг":
                orderIronService.reduceAmount(product, order);
                break;
        }
        return updateFromDTO(orderId, new OrderDTO(order));
    }

    /**
     * Отправка письма администратору с уведомлением об оформленном заказе.
     */
    public void order_notification (Long orderId){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(adminEmail);
        mailMessage.setSubject("Оплата заказа в онлайн магазине \"TeHStore\"");
        mailMessage.setText("Уведомляем Вас о том, что в онлайн магазине \"TeHStore\" был сформирован и оплачен заказ №" + orderId +
                ". Пожалуйста перейдите к сборке и доставке заказа покупателю!");
        javaMailSender.send(mailMessage);
    }


}
