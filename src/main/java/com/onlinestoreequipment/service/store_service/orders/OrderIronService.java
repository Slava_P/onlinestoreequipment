package com.onlinestoreequipment.service.store_service.orders;

import com.onlinestoreequipment.dto.store_service.order.ProductOrderDTO;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.store_service.orders.Order;
import com.onlinestoreequipment.model.store_service.orders.OrderIron;
import com.onlinestoreequipment.repository.IronRepository;
import com.onlinestoreequipment.repository.orders.OrderIronRepository;
import com.onlinestoreequipment.repository.orders.OrderRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class OrderIronService {

    private OrderIronRepository orderIronRepository;
    private OrderRepository orderRepository;
    private IronRepository ironRepository;

    public OrderIronService(OrderIronRepository orderIronRepository, OrderRepository orderRepository, IronRepository ironRepository) {
        this.orderIronRepository = orderIronRepository;
        this.orderRepository = orderRepository;
        this.ironRepository = ironRepository;
    }

    public OrderIron create(Order order, Iron iron){
        OrderIron orderIron = new OrderIron();
        orderIron.setOrder(order);
        orderIron.setIron(iron);
        orderIron.setAmount(1);

        return orderIronRepository.save(orderIron);
    }

    public void delete(ProductOrderDTO product, Long orderId){
        orderIronRepository.deleteByIronAndOrder(product.getId(), orderId);
    }

    public void delete(Iron iron){
        orderIronRepository.deleteByIron(iron.getId());
    }

    public List<OrderIron> getAllWithIron(Iron iron){
        return orderIronRepository.findAllByIron(iron);
    }


    /**
     * Увеличивает количество товара (amount) в таблице "orders_irons" .
     *
     * @param product,order товар и заказ
     * @return OrderVacuumCleaner строку из таблицы "orders_irons" с увеличенным "amount"
     */
    public OrderIron increaseAmount(ProductOrderDTO product, Order order){

        OrderIron orderIron = orderIronRepository.findByIronAndOrder(
                ironRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Утюга с Id=" + product.getId() + " не существует в базе")),
                order);

        orderIron.setAmount(product.getAmountInOrder() + 1);
        return orderIronRepository.save(orderIron);
    }

    /**
     * Уменьшает количество товара (amount) в таблице "orders_irons" .
     *
     * @param product,order товар и заказ
     * @return OrderVacuumCleaner строку из таблицы "orders_irons" с уменьшенным "amount"
     */
    public OrderIron reduceAmount(ProductOrderDTO product, Order order){
        OrderIron orderIron = orderIronRepository.findByIronAndOrder(
                ironRepository.findById(product.getId()).orElseThrow(()-> new NotFoundException("Утюга с Id=" + product.getId() + " не существует в базе")),
                order);

        orderIron.setAmount(product.getAmountInOrder() - 1);
        return orderIronRepository.save(orderIron);
    }
}
