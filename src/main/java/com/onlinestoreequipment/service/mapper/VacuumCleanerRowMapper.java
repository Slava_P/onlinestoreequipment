package com.onlinestoreequipment.service.mapper;

import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import org.springframework.jdbc.core.RowMapper;
import org.webjars.NotFoundException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VacuumCleanerRowMapper implements RowMapper<VacuumCleaner> {

    private ManufacturerRepository manufacturerRepository;

    public VacuumCleanerRowMapper(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public VacuumCleaner mapRow(ResultSet rs, int rowNum) throws SQLException {
        VacuumCleaner vacuumCleaner = new VacuumCleaner();
        vacuumCleaner.setId(rs.getLong("id"));
        vacuumCleaner.setManufacturer(manufacturerRepository.findById(rs.getLong("manufacturer_id")).orElseThrow(()-> new NotFoundException("Пылесоса не существует в базе")));
        vacuumCleaner.setImgPath(rs.getString("img_path"));
        vacuumCleaner.setModel(rs.getString("model"));
        vacuumCleaner.setColor(rs.getString("color"));
        vacuumCleaner.setWeight(rs.getDouble("weight"));
        vacuumCleaner.setCost(rs.getInt("cost"));
        vacuumCleaner.setDescription(rs.getString("description"));
        vacuumCleaner.setPowerConsumption(rs.getInt("power_consumption"));
        vacuumCleaner.setHeight(rs.getDouble("height"));
        vacuumCleaner.setWidth(rs.getDouble("width"));
        vacuumCleaner.setDepth(rs.getDouble("depth"));
        vacuumCleaner.setAmount(rs.getInt("amount"));
        vacuumCleaner.setSuctionPower(rs.getInt("suction_power"));
        vacuumCleaner.setMaxNoiseLevel(rs.getInt("max_noise_level"));
        vacuumCleaner.setPowerCordLength(rs.getDouble("power_cord_length"));

        return vacuumCleaner;
    }
}
