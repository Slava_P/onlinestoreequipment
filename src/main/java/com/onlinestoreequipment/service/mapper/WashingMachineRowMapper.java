package com.onlinestoreequipment.service.mapper;

import com.onlinestoreequipment.model.constant.MotorType;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import org.springframework.jdbc.core.RowMapper;
import org.webjars.NotFoundException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WashingMachineRowMapper implements RowMapper<WashingMachine> {

    private ManufacturerRepository manufacturerRepository;

    public WashingMachineRowMapper(ManufacturerRepository manufacturerRepository){
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public WashingMachine mapRow(ResultSet rs, int rowNum) throws SQLException {
        WashingMachine washingMachine = new WashingMachine();
        washingMachine.setId(rs.getLong("id"));
        washingMachine.setManufacturer(manufacturerRepository.findById(rs.getLong("manufacturer_id")).orElseThrow(()-> new NotFoundException("Стиральной машины не существует в базе")));
        washingMachine.setImgPath(rs.getString("img_path"));
        washingMachine.setModel(rs.getString("model"));
        washingMachine.setColor(rs.getString("color"));
        washingMachine.setWeight(rs.getDouble("weight"));
        washingMachine.setCost(rs.getInt("cost"));
        washingMachine.setDescription(rs.getString("description"));
        washingMachine.setPowerConsumption(rs.getInt("power_consumption"));
        washingMachine.setHeight(rs.getDouble("height"));
        washingMachine.setWidth(rs.getDouble("width"));
        washingMachine.setDepth(rs.getDouble("depth"));
        washingMachine.setAmount(rs.getInt("amount"));
        washingMachine.setMotorType(MotorType.valueOf(rs.getString("motor_type")));
        washingMachine.setMaxSpinSpeed(rs.getInt("max_spin_speed"));
        washingMachine.setWaterConsumption(rs.getDouble("water_consumption"));
        washingMachine.setMaxLoad(rs.getInt("max_load"));

        return washingMachine;
    }
}
