package com.onlinestoreequipment.service.mapper;

import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.repository.IronRepository;
import com.onlinestoreequipment.repository.ManufacturerRepository;
import org.springframework.jdbc.core.RowMapper;
import org.webjars.NotFoundException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IronRowMapper implements RowMapper<Iron> {

    private ManufacturerRepository manufacturerRepository;

    public IronRowMapper(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }


    @Override
    public Iron mapRow(ResultSet rs, int rowNum) throws SQLException {
        Iron iron = new Iron();
        iron.setId(rs.getLong("id"));
        iron.setManufacturer(manufacturerRepository.findById(rs.getLong("manufacturer_id")).orElseThrow(()-> new NotFoundException("Утюга не существует в базе")));
        iron.setImgPath(rs.getString("img_path"));
        iron.setModel(rs.getString("model"));
        iron.setColor(rs.getString("color"));
        iron.setWeight(rs.getDouble("weight"));
        iron.setCost(rs.getInt("cost"));
        iron.setDescription(rs.getString("description"));
        iron.setPowerConsumption(rs.getInt("power_consumption"));
        iron.setHeight(rs.getDouble("height"));
        iron.setWidth(rs.getDouble("width"));
        iron.setDepth(rs.getDouble("depth"));
        iron.setAmount(rs.getInt("amount"));
        iron.setWaterTankVolume(rs.getInt("water_tank_volume"));
        iron.setWaterSprayer(rs.getBoolean("water_sprayer"));
        iron.setPowerCordLength(rs.getDouble("power_cord_legth"));
        iron.setTurboSteamSupply(rs.getBoolean("turbo_steam_supply"));

        return iron;
    }
}
