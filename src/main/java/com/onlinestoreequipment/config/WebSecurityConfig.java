package com.onlinestoreequipment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    private UserDetailsService userDetailsService;
    private AuthenticationManagerBuilder authenticationManagerBuilder;

    public WebSecurityConfig(UserDetailsService userDetailsService, AuthenticationManagerBuilder authenticationManagerBuilder){
        this.userDetailsService = userDetailsService;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    //Создаем бин нашего энкриптора паролей
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //Позволить передавать % и /
    @Bean
    public HttpFirewall allowPercent(){
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowUrlEncodedPercent(true);
        return firewall;
    }

    //Настройка где будет запрашиваться авторизация, куда перебрасывать после и тд
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.csrf()
                .disable()
                //Доступ к страницам и папкам
                .authorizeRequests()
                // доступ к странице только с определенной ролью
                .antMatchers("/orders/account/**", "/account/update").hasRole("USER")
                .and()
                .authorizeRequests()
                .antMatchers("/manufacturers/add/**","/manufacturers/update/**", "/refrigerators/add/**", "/refrigerators/update/**", "/microwaves/add/**", "/microwaves/update/**",
                        "/washing-machines/add/**", "/washing-machines/update/**", "/vacuum-cleaners/add/**", "/vacuum-cleaners/update/**", "/irons/add/**", "/irons/update/**", "/orders/allOrders/**", "/orders/find-orders/**").hasRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/orders/get/**").hasAnyRole("ADMIN", "USER")
                .anyRequest().permitAll()
                .and()
                //Логика работы при входе в аккаунт
                .formLogin()
                //страница входа
                .loginPage("/account/signIn")
                // куда будет перенаправлено после входа
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                //Логика работы при выходе из аккаунта
                .logout()
                .permitAll()
                // куда будет перенаправлено после выхода
                .logoutSuccessUrl("/");

        return httpSecurity.build();
    }

    protected void configureGlobal() throws Exception{
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }





}
