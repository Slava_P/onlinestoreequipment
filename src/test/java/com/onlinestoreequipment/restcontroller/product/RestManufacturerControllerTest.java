package com.onlinestoreequipment.restcontroller.product;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.ManufacturerDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestManufacturerControllerTest {

    @Autowired
    public MockMvc mvc;

    @Test
    public void getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                                .get("/rest/manufacturers/getAll")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<Manufacturer> manufacturers = objectMapper.readValue(result, new TypeReference<List<Manufacturer>>() {});
        System.out.println(manufacturers.size());
    }

    @Test
    public void createManufacturer() throws Exception{
        ManufacturerDTO manufacturerDTO = new ManufacturerDTO();
        manufacturerDTO.setFirmName("Тестовая фирма");
        manufacturerDTO.setCountry("Волшебная страна");
        manufacturerDTO.setDescription("Тестовое описание");

        String response = mvc.perform(
                post("/rest/manufacturers/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(manufacturerDTO))
                        .accept(MediaType.APPLICATION_JSON)
                    )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Manufacturer manufacturer =objectMapper.readValue(response, Manufacturer.class);
        System.out.println(manufacturer.getId());
    }

    @Test
    public void updateManufacturer() throws Exception {
        ManufacturerDTO manufacturerDTO = new ManufacturerDTO();
        manufacturerDTO.setFirmName("Тестовая фирма");
        manufacturerDTO.setCountry("Измененная страна");
        manufacturerDTO.setDescription("Тестовое измененное описание");

        String response = mvc.perform(
                        put("/rest/manufacturers/update")
                                .param("id", "7")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(manufacturerDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Manufacturer manufacturer =objectMapper.readValue(response, Manufacturer.class);
        System.out.println(manufacturer);
    }

    @Test
    public void deleteManufacturer() throws Exception{
        String response = mvc.perform(
                delete("/rest/manufacturers/delete")
                        .param("id", "14")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                    )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
