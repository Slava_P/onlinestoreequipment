package com.onlinestoreequipment.restcontroller.product.home_appliance;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerDTO;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineDTO;
import com.onlinestoreequipment.model.constant.MotorType;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestVacuumCleanerControllerTest {

    @Autowired
    public MockMvc mvc;

    private Long id;

    @Test
    public void testCase() throws Exception{
        createVacuumCleaner();
        updateVacuumCleaner();
        Integer sizeAfterCreate = getAll();
        deleteVacuumCleaner();
        Integer sizeAfterDelete = getAll();
        assertEquals(sizeAfterDelete, sizeAfterCreate - 1);
    }

    @Test
    public Integer getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                        .get("/rest/vacuum-cleaners/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<VacuumCleaner> vacuumCleaners = objectMapper.readValue(result, new TypeReference<List<VacuumCleaner>>() {});
        System.out.println(vacuumCleaners.size());
        return vacuumCleaners.size();
    }

    @Test
    public void createVacuumCleaner() throws Exception{
        VacuumCleanerDTO vacuumCleanerDTO = new VacuumCleanerDTO();
        vacuumCleanerDTO.setFirmName("Тестовая фирма");
        vacuumCleanerDTO.setModel("Тестовая модель");
        vacuumCleanerDTO.setColor("Тестовый цвет");
        vacuumCleanerDTO.setWeight(1.0);
        vacuumCleanerDTO.setAmount(1);
        vacuumCleanerDTO.setCost(100);
        vacuumCleanerDTO.setDescription("Тестовое описание");
        vacuumCleanerDTO.setPowerConsumption(1);
        vacuumCleanerDTO.setSuctionPower(1);
        vacuumCleanerDTO.setMaxNoiseLevel(1);
        vacuumCleanerDTO.setPowerCordLength(1.0);
        vacuumCleanerDTO.setHeight(1.0);
        vacuumCleanerDTO.setWidth(1.0);
        vacuumCleanerDTO.setDepth(1.0);


        String response = mvc.perform(
                        post("/rest/vacuum-cleaners/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(vacuumCleanerDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        VacuumCleaner vacuumCleaner =objectMapper.readValue(response, VacuumCleaner.class);
        this.id = vacuumCleaner.getId();
        System.out.println(id);
    }

    @Test
    public void updateVacuumCleaner() throws Exception {
        VacuumCleanerDTO vacuumCleanerDTO = new VacuumCleanerDTO();
        vacuumCleanerDTO.setFirmName("Тестовая фирма");
        vacuumCleanerDTO.setModel("Новая Тестовая модель");
        vacuumCleanerDTO.setColor("Новый Тестовый цвет");
        vacuumCleanerDTO.setWeight(1.0);
        vacuumCleanerDTO.setAmount(1);
        vacuumCleanerDTO.setCost(100);
        vacuumCleanerDTO.setDescription("Новое Тестовое описание");
        vacuumCleanerDTO.setPowerConsumption(1);
        vacuumCleanerDTO.setSuctionPower(1);
        vacuumCleanerDTO.setMaxNoiseLevel(1);
        vacuumCleanerDTO.setPowerCordLength(1.0);
        vacuumCleanerDTO.setHeight(1.0);
        vacuumCleanerDTO.setWeight(1.0);
        vacuumCleanerDTO.setDepth(1.0);

        String response = mvc.perform(
                        put("/rest/vacuum-cleaners/update")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(vacuumCleanerDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        VacuumCleaner vacuumCleaner =objectMapper.readValue(response, VacuumCleaner.class);
        System.out.println(vacuumCleaner);
    }

    @Test
    public void deleteVacuumCleaner() throws Exception{
        String response = mvc.perform(
                        delete("/rest/vacuum-cleaners/delete")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
