package com.onlinestoreequipment.restcontroller.product.home_appliance;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.home_appliances.WashingMachineDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveDTO;
import com.onlinestoreequipment.model.constant.MotorType;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestWashingMachineControllerTest {

    @Autowired
    public MockMvc mvc;

    private Long id;

    @Test
    public void testCase() throws Exception{
        createWashingMachine();
        updateWashingMachine();
        Integer sizeAfterCreate = getAll();
        deleteWashingMachine();
        Integer sizeAfterDelete = getAll();
        assertEquals(sizeAfterDelete, sizeAfterCreate - 1);
    }

    @Test
    public Integer getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                        .get("/rest/washing-machines/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<WashingMachine> washingMachines = objectMapper.readValue(result, new TypeReference<List<WashingMachine>>() {});
        System.out.println(washingMachines.size());
        return washingMachines.size();
    }

    @Test
    public void createWashingMachine() throws Exception{
        WashingMachineDTO washingMachineDTO = new WashingMachineDTO();
        washingMachineDTO.setFirmName("Тестовая фирма");
        washingMachineDTO.setModel("Тестовая модель");
        washingMachineDTO.setColor("Тестовый цвет");
        washingMachineDTO.setWeight(1.0);
        washingMachineDTO.setAmount(1);
        washingMachineDTO.setCost(100);
        washingMachineDTO.setDescription("Тестовое описание");
        washingMachineDTO.setPowerConsumption(1);
        washingMachineDTO.setMaxSpinSpeed(1);
        washingMachineDTO.setMotorType(MotorType.STANDARD);
        washingMachineDTO.setWaterConsumption(1.0);
        washingMachineDTO.setMaxLoad(1);
        washingMachineDTO.setHeight(1.0);
        washingMachineDTO.setWidth(1.0);
        washingMachineDTO.setDepth(1.0);


        String response = mvc.perform(
                        post("/rest/washing-machines/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(washingMachineDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        WashingMachine washingMachine =objectMapper.readValue(response, WashingMachine.class);
        this.id = washingMachine.getId();
        System.out.println(id);
    }

    @Test
    public void updateWashingMachine() throws Exception {
        WashingMachineDTO washingMachineDTO = new WashingMachineDTO();
        washingMachineDTO.setFirmName("Тестовая фирма");
        washingMachineDTO.setModel("Новая Тестовая модель");
        washingMachineDTO.setColor("Новый Тестовый цвет");
        washingMachineDTO.setWeight(1.0);
        washingMachineDTO.setAmount(1);
        washingMachineDTO.setCost(100);
        washingMachineDTO.setDescription("Новое Тестовое описание");
        washingMachineDTO.setPowerConsumption(1);
        washingMachineDTO.setMaxSpinSpeed(1);
        washingMachineDTO.setMotorType(MotorType.STANDARD);
        washingMachineDTO.setWaterConsumption(1.0);
        washingMachineDTO.setMaxLoad(1);
        washingMachineDTO.setHeight(1.0);
        washingMachineDTO.setWeight(1.0);
        washingMachineDTO.setDepth(1.0);

        String response = mvc.perform(
                        put("/rest/washing-machines/update")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(washingMachineDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        WashingMachine washingMachine =objectMapper.readValue(response, WashingMachine.class);
        System.out.println(washingMachine);
    }

    @Test
    public void deleteWashingMachine() throws Exception{
        String response = mvc.perform(
                        delete("/rest/washing-machines/delete")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
