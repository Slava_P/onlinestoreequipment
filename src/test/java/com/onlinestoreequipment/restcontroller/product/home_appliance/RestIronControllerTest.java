package com.onlinestoreequipment.restcontroller.product.home_appliance;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.home_appliances.IronDTO;
import com.onlinestoreequipment.dto.product.home_appliances.VacuumCleanerDTO;
import com.onlinestoreequipment.model.product.home_appliances.Iron;
import com.onlinestoreequipment.model.product.home_appliances.VacuumCleaner;
import com.onlinestoreequipment.model.product.home_appliances.WashingMachine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestIronControllerTest {

    @Autowired
    public MockMvc mvc;

    private Long id;

    @Test
    public void testCase() throws Exception{
        createIron();
        updateIron();
        Integer sizeAfterCreate = getAll();
        deleteIron();
        Integer sizeAfterDelete = getAll();
        assertEquals(sizeAfterDelete, sizeAfterCreate - 1);
    }

    @Test
    public Integer getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                        .get("/rest/irons/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<Iron> irons = objectMapper.readValue(result, new TypeReference<List<Iron>>() {});
        System.out.println(irons.size());
        return irons.size();
    }

    @Test
    public void createIron() throws Exception{
        IronDTO ironDTO = new IronDTO();
        ironDTO.setFirmName("Тестовая фирма");
        ironDTO.setModel("Тестовая модель");
        ironDTO.setColor("Тестовый цвет");
        ironDTO.setWeight(1.0);
        ironDTO.setAmount(1);
        ironDTO.setCost(100);
        ironDTO.setDescription("Тестовое описание");
        ironDTO.setPowerConsumption(1);
        ironDTO.setWaterTankVolume(1);
        ironDTO.setWaterSprayer(true);
        ironDTO.setTurboSteamSupply(true);
        ironDTO.setPowerCordLength(1.0);
        ironDTO.setHeight(1.0);
        ironDTO.setWidth(1.0);
        ironDTO.setDepth(1.0);


        String response = mvc.perform(
                        post("/rest/irons/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(ironDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Iron iron =objectMapper.readValue(response, Iron.class);
        this.id = iron.getId();
        System.out.println(id);
    }

    @Test
    public void updateIron() throws Exception {
        IronDTO ironDTO = new IronDTO();
        ironDTO.setFirmName("Тестовая фирма");
        ironDTO.setModel("Новая Тестовая модель");
        ironDTO.setColor("Новый Тестовый цвет");
        ironDTO.setWeight(1.0);
        ironDTO.setAmount(1);
        ironDTO.setCost(100);
        ironDTO.setDescription("Новое Тестовое описание");
        ironDTO.setPowerConsumption(1);
        ironDTO.setWaterTankVolume(1);
        ironDTO.setWaterSprayer(true);
        ironDTO.setTurboSteamSupply(true);
        ironDTO.setPowerCordLength(1.0);
        ironDTO.setHeight(1.0);
        ironDTO.setWeight(1.0);
        ironDTO.setDepth(1.0);

        String response = mvc.perform(
                        put("/rest/irons/update")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(ironDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Iron iron =objectMapper.readValue(response, Iron.class);
        System.out.println(iron);
    }

    @Test
    public void deleteIron() throws Exception{
        String response = mvc.perform(
                        delete("/rest/irons/delete")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
