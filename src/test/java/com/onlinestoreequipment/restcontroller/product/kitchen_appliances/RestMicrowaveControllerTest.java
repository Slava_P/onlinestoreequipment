package com.onlinestoreequipment.restcontroller.product.kitchen_appliances;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.kitchen_appliances.MicrowaveDTO;
import com.onlinestoreequipment.model.product.kitchen_appliances.Microwave;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestMicrowaveControllerTest {

    @Autowired
    public MockMvc mvc;

    private Long id;

    @Test
    public void testCase() throws Exception{
        createMicrowave();
        updateMicrowave();
        Integer sizeAfterCreate = getAll();
        deleteMicrowave();
        Integer sizeAfterDelete = getAll();
        assertEquals(sizeAfterDelete, sizeAfterCreate - 1);
    }

    @Test
    public Integer getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                        .get("/rest/microwaves/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<Microwave> microwaves = objectMapper.readValue(result, new TypeReference<List<Microwave>>() {});
        System.out.println(microwaves.size());
        return microwaves.size();
    }

    @Test
    public void createMicrowave() throws Exception{
        MicrowaveDTO microwaveDTO = new MicrowaveDTO();
        microwaveDTO.setFirmName("Тестовая фирма");
        microwaveDTO.setModel("Тестовая модель");
        microwaveDTO.setColor("Тестовый цвет");
        microwaveDTO.setWeight(1.0);
        microwaveDTO.setAmount(1);
        microwaveDTO.setCost(100);
        microwaveDTO.setDescription("Тестовое описание");
        microwaveDTO.setPowerConsumption(1);
        microwaveDTO.setMicrowavePower(1);
        microwaveDTO.setVolume(1);
        microwaveDTO.setHeight(1.0);
        microwaveDTO.setWidth(1.0);
        microwaveDTO.setDepth(1.0);


        String response = mvc.perform(
                        post("/rest/microwaves/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(microwaveDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Microwave microwave =objectMapper.readValue(response, Microwave.class);
        this.id = microwave.getId();
        System.out.println(id);
    }

    @Test
    public void updateMicrowave() throws Exception {
        MicrowaveDTO microwaveDTO = new MicrowaveDTO();
        microwaveDTO.setFirmName("Тестовая фирма");
        microwaveDTO.setModel("Новая Тестовая модель");
        microwaveDTO.setColor("Новый Тестовый цвет");
        microwaveDTO.setWeight(1.0);
        microwaveDTO.setAmount(1);
        microwaveDTO.setCost(100);
        microwaveDTO.setDescription("Новое Тестовое описание");
        microwaveDTO.setPowerConsumption(1);
        microwaveDTO.setMicrowavePower(1);
        microwaveDTO.setVolume(1);
        microwaveDTO.setHeight(1.0);
        microwaveDTO.setWidth(1.0);
        microwaveDTO.setDepth(1.0);

        String response = mvc.perform(
                        put("/rest/microwaves/update")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(microwaveDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Microwave microwave =objectMapper.readValue(response, Microwave.class);
        System.out.println(microwave);
    }

    @Test
    public void deleteMicrowave() throws Exception{
        String response = mvc.perform(
                        delete("/rest/microwaves/delete")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
