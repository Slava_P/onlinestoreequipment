package com.onlinestoreequipment.restcontroller.product.kitchen_appliances;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.onlinestoreequipment.dto.product.ManufacturerDTO;
import com.onlinestoreequipment.dto.product.kitchen_appliances.RefrigeratorDTO;
import com.onlinestoreequipment.model.product.Manufacturer;
import com.onlinestoreequipment.model.product.kitchen_appliances.Refrigerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RestRefrigeratorControllerTest {

    @Autowired
    public MockMvc mvc;

    private Long id;

    @Test
    public void testCase() throws Exception{
        createRefrigerator();
        updateRefrigerator();
        Integer sizeAfterCreate = getAll();
        deleteRefrigerator();
        Integer sizeAfterDelete = getAll();
        assertEquals(sizeAfterDelete, sizeAfterCreate - 1);
    }

    @Test
    public Integer getAll() throws Exception{

        String result = mvc.perform(MockMvcRequestBuilders
                        .get("/rest/refrigerators/getAll")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(result);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<Refrigerator> refrigerators = objectMapper.readValue(result, new TypeReference<List<Refrigerator>>() {});
        System.out.println(refrigerators.size());
        return refrigerators.size();
    }

    @Test
    public void createRefrigerator() throws Exception{
        RefrigeratorDTO refrigeratorDTO = new RefrigeratorDTO();
        refrigeratorDTO.setFirmName("Тестовая фирма");
        refrigeratorDTO.setModel("Тестовая модель");
        refrigeratorDTO.setColor("Тестовый цвет");
        refrigeratorDTO.setWeight(1.0);
        refrigeratorDTO.setAmount(1);
        refrigeratorDTO.setCost(100);
        refrigeratorDTO.setDescription("Тестовое описание");
        refrigeratorDTO.setPowerConsumption(1);
        refrigeratorDTO.setFreezingPower(1);
        refrigeratorDTO.setFreezingVolume(1);
        refrigeratorDTO.setRefrigeChamberVolume(1);
        refrigeratorDTO.setHeight(1.0);
        refrigeratorDTO.setWidth(1.0);
        refrigeratorDTO.setDepth(1.0);


        String response = mvc.perform(
                        post("/rest/refrigerators/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(refrigeratorDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Refrigerator refrigerator =objectMapper.readValue(response, Refrigerator.class);
        this.id = refrigerator.getId();
        System.out.println(id);
    }

    @Test
    public void updateRefrigerator() throws Exception {
        RefrigeratorDTO refrigeratorDTO = new RefrigeratorDTO();
        refrigeratorDTO.setFirmName("Тестовая фирма");
        refrigeratorDTO.setModel("Новая Тестовая модель");
        refrigeratorDTO.setColor("Новый Тестовый цвет");
        refrigeratorDTO.setWeight(1.0);
        refrigeratorDTO.setAmount(1);
        refrigeratorDTO.setCost(100);
        refrigeratorDTO.setDescription("Новое Тестовое описание");
        refrigeratorDTO.setPowerConsumption(1);
        refrigeratorDTO.setFreezingPower(1);
        refrigeratorDTO.setFreezingVolume(1);
        refrigeratorDTO.setRefrigeChamberVolume(1);
        refrigeratorDTO.setHeight(1.0);
        refrigeratorDTO.setWidth(1.0);
        refrigeratorDTO.setDepth(1.0);

        String response = mvc.perform(
                        put("/rest/refrigerators/update")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(refrigeratorDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        Refrigerator refrigerator =objectMapper.readValue(response, Refrigerator.class);
        System.out.println(refrigerator);
    }

    @Test
    public void deleteRefrigerator() throws Exception{
        String response = mvc.perform(
                        delete("/rest/refrigerators/delete")
                                .param("id", String.valueOf(id))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
